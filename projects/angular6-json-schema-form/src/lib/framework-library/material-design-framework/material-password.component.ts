import { AbstractControl, FormControl } from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';

import { JsonSchemaFormService } from '../../json-schema-form.service';

@Component({
  selector: 'material-password-widget',
  template: `
    <mat-form-field
      [floatLabel]="
        options?.floatLabel || (options?.notitle ? 'never' : 'auto')
      "
    >
      <input
        matInput
        [formControl]="passwordFC"
        [placeholder]="options?.notitle ? options?.placeholder : options?.title"
        [type]="show ? 'password' : 'text'"
      />
      <mat-icon matSuffix (click)="toggle()">{{
        show ? 'visibility' : 'visibility_off'
      }}</mat-icon>
    </mat-form-field>
  `,
  styles: [
    `
      .mat-form-field {
        width: 100%;
      }
    `
  ]
})
export class MaterialPasswordComponent implements OnInit {
  formControl: AbstractControl;
  passwordFC: AbstractControl = new FormControl();
  @Input() layoutNode: any;
  @Input() layoutIndex: number[];
  @Input() dataIndex: number[];
  options: any;
  show = true;

  constructor(private jsf: JsonSchemaFormService) {}

  ngOnInit() {
    this.options = this.layoutNode.options || {};
    this.jsf.initializeControl(this);
    this.passwordFC.valueChanges.subscribe(value => this.update(value));
  }

  update(value) {
    this.jsf.updateValue(this, value);
  }
  toggle() {
    this.show = !this.show;
  }
}
