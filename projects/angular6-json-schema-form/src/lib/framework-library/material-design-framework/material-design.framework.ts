import { FlexLayoutRootComponent } from './flex-layout-root.component';
import { FlexLayoutSectionComponent } from './flex-layout-section.component';
import { Framework } from '../framework';
import { Injectable } from '@angular/core';
import { MaterialAddReferenceComponent } from './material-add-reference.component';
import { MaterialAutoCompleteComponent } from './material-autocomplete.component';
import { MaterialButtonComponent } from './material-button.component';
import { MaterialButtonGroupComponent } from './material-button-group.component';
import { MaterialCheckboxComponent } from './material-checkbox.component';
import { MaterialCheckboxesComponent } from './material-checkboxes.component';
import { MaterialChiplistComponent } from './material-chiplist.component';
import { MaterialComputationComponent } from './material-computation.component';
import { MaterialDatepickerComponent } from './material-datepicker.component';
import { MaterialDesignFrameworkComponent } from './material-design-framework.component';
import { MaterialDynamicOptionsWidget } from './material-dynamic-options.component';
import { MaterialFileComponent } from './material-file.component';
import { MaterialInputComponent } from './material-input.component';
import { MaterialNumberComponent } from './material-number.component';
import { MaterialOneOfComponent } from './material-one-of.component';
import { MaterialRadiosComponent } from './material-radios.component';
import { MaterialSelectComponent } from './material-select.component';
import { MaterialSliderComponent } from './material-slider.component';
import { MaterialStepperComponent } from './material-stepper.component';
import { MaterialTabsComponent } from './material-tabs.component';
import { MaterialTextareaComponent } from './material-textarea.component';
import { MaterialMultiTextFieldComponent } from './material-multitextfield.component';
import { MaterialPasswordComponent } from './material-password.component';

// Material Design Framework
// https://github.com/angular/material2

@Injectable()
export class MaterialDesignFramework extends Framework {
  name = 'material-design';

  framework = MaterialDesignFrameworkComponent;

  stylesheets = [
    '//fonts.googleapis.com/icon?family=Material+Icons',
    '//fonts.googleapis.com/css?family=Roboto:300,400,500,700'
  ];

  widgets = {
    root: FlexLayoutRootComponent,
    section: FlexLayoutSectionComponent,
    $ref: MaterialAddReferenceComponent,
    autocomplete: MaterialAutoCompleteComponent,
    button: MaterialButtonComponent,
    'button-group': MaterialButtonGroupComponent,
    checkbox: MaterialCheckboxComponent,
    checkboxes: MaterialCheckboxesComponent,
    chiplist: MaterialChiplistComponent,
    computation: MaterialComputationComponent,
    multitextfield: MaterialMultiTextFieldComponent,
    date: MaterialDatepickerComponent,
    dynamicoption: MaterialDynamicOptionsWidget,
    file: MaterialFileComponent,
    number: MaterialNumberComponent,
    'one-of': MaterialOneOfComponent,
    password: MaterialPasswordComponent,
    radios: MaterialRadiosComponent,
    select: MaterialSelectComponent,
    slider: MaterialSliderComponent,
    stepper: MaterialStepperComponent,
    tabs: MaterialTabsComponent,
    text: MaterialInputComponent,
    textarea: MaterialTextareaComponent,
    'alt-date': 'date',
    'any-of': 'one-of',
    card: 'section',
    color: 'text',
    'expansion-panel': 'section',
    hidden: 'none',
    image: 'none',
    integer: 'number',
    radiobuttons: 'button-group',
    range: 'slider',
    submit: 'button',
    tagsinput: 'chiplist',
    wizard: 'stepper'
  };
}
