import { Component, Input, OnInit, ChangeDetectorRef } from '@angular/core';
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormGroup
} from '@angular/forms';
import { take } from 'rxjs/operators';

import { JsonSchemaFormService } from '../../json-schema-form.service';

@Component({
  selector: 'material-dynamic-options-widget',
  template: `
    <form [formGroup]="optionsForm">
      <div fxLayout="column" fxLayoutGap="32px">
        <div fxLayout="column" fxLayoutGap="16px">
          <h3>Default Options</h3>
          <div
            *ngIf="defaultOptions?.controls?.length; else noOptionsAvailable"
          >
            <mat-card>
              <div formArrayName="defaultOptions">
                <div
                  *ngFor="
                    let do of defaultOptions?.controls;
                    let defIndex = index;
                    trackBy: trackByFn
                  "
                >
                  <div
                    [formGroupName]="defIndex"
                    fxLayout="row wrap"
                    fxLayoutAlign="start center"
                    fxLayoutGap="16px"
                  >
                    <div fxFlex="5">
                      <mat-checkbox
                        formControlName="ticked"
                        (click)="tickDefaultOption(defIndex, do)"
                      ></mat-checkbox>
                    </div>
                    <div fxFlex="35">
                      <mat-form-field>
                        <mat-label>Display Value</mat-label>
                        <input matInput formControlName="displayValue" />
                      </mat-form-field>
                    </div>
                    <div fxFlex="35">
                      <mat-form-field>
                        <mat-label>Value</mat-label>
                        <input matInput formControlName="value" />
                      </mat-form-field>
                    </div>
                    <div fxFlex="5">
                      <mat-icon (click)="removeDefaultsOption(defIndex)"
                        >remove</mat-icon
                      >
                    </div>
                  </div>
                </div>
              </div>
            </mat-card>
          </div>
          <div fxLayout="row" fxLayoutAlign="start center">
            <button
              mat-raised-button
              (click)="addDefaultsOption()"
              color="accent"
            >
              Add Option
            </button>
          </div>
        </div>

        <div fxLayout="column" fxLayoutGap="16px">
          <h3>Dynamic Options</h3>
          <div
            *ngIf="
              dynamicOptions?.controls?.length;
              else noDynamicOptionsAvailable
            "
            fxLayout="column"
            fxLayoutGap="16px"
          >
            <div
              *ngFor="
                let do of dynamicOptions?.controls;
                let doIndex = index;
                trackBy: trackByFn
              "
            >
              <mat-card formArrayName="dynamicOptions">
                <div [formGroupName]="doIndex">
                  <div fxLayout="row" fxLayoutAlign="end center">
                    <mat-icon (click)="removeDynamicOption(defIndex)"
                      >close</mat-icon
                    >
                  </div>
                  <div
                    fxLayout="row wrap"
                    fxLayoutAlign="start center"
                    fxLayoutGap="16px"
                  >
                    <div fxFlex="45">
                      <mat-form-field>
                        <mat-label>Depends On</mat-label>
                        <mat-select formControlName="dependsOn">
                          <mat-option
                            *ngFor="let field of fields"
                            [value]="field.name"
                          >
                            {{ field.label }}
                          </mat-option>
                        </mat-select>
                      </mat-form-field>
                    </div>
                    <div fxFlex="45">
                      <mat-form-field>
                        <mat-label>Depends On Value</mat-label>
                        <input matInput formControlName="dependsOnValue" />
                      </mat-form-field>
                    </div>
                  </div>
                  <div
                    *ngIf="
                      do?.get('options')?.controls?.length;
                      else noOptionsAvailable
                    "
                  >
                    <div formArrayName="options">
                      <div
                        *ngFor="
                          let doo of do?.get('options')?.controls;
                          let dooIndex = index;
                          trackBy: trackByFn
                        "
                      >
                        <div
                          [formGroupName]="dooIndex"
                          fxLayout="row wrap"
                          fxLayoutAlign="start center"
                          fxLayoutGap="16px"
                        >
                          <div fxFlex="5">
                            <mat-checkbox
                              formControlName="ticked"
                              (click)="
                                tickDynamicOptionsOption(dooIndex, doo, do)
                              "
                            >
                            </mat-checkbox>
                          </div>
                          <div fxFlex="35">
                            <mat-form-field>
                              <mat-label>Display Value</mat-label>
                              <input matInput formControlName="displayValue" />
                            </mat-form-field>
                          </div>
                          <div fxFlex="35">
                            <mat-form-field>
                              <mat-label>Value</mat-label>
                              <input matInput formControlName="value" />
                            </mat-form-field>
                          </div>
                          <div fxFlex="5">
                            <mat-icon
                              (click)="
                                removeDynamicOptionsOption(
                                  do?.get('options'),
                                  dooIndex
                                )
                              "
                              >remove</mat-icon
                            >
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div
                    fxLayout="row"
                    fxLayoutAlign="start center"
                    fxLayoutGap="8px"
                  >
                    <button
                      mat-raised-button
                      (click)="addDynamicOptionsOption(do?.get('options'))"
                      color="accent"
                    >
                      Add Option
                    </button>
                    <!--button
                      mat-raised-button
                      (click)="duplicateDynamicOption(do, doIndex)"
                      color="accent"
                    >
                      Duplicate Dynamic Option
                    </button-->
                  </div>
                </div>
              </mat-card>
            </div>
          </div>
          <div fxLayout="row" fxLayoutAlign="start center">
            <button
              mat-raised-button
              (click)="addDynamicOptions()"
              color="accent"
            >
              Add Dynamic Option
            </button>
          </div>
        </div>
      </div>

      <ng-template #noOptionsAvailable>
        <p>No Options Added</p>
      </ng-template>

      <ng-template #noDynamicOptionsAvailable>
        <p>No Dynamic Options Added</p>
      </ng-template>
    </form>
  `,
  styles: [
    `
      .opt-close {
        font-size: 32px;
        font-weight: bold;
      }

      .do-card-close {
        font-size: 18px;
        font-weight: bold;
      }

      .mat-form-field {
        width: 100%;
      }
    `
  ]
})
export class MaterialDynamicOptionsWidget implements OnInit {
  optionsForm: FormGroup;
  defaultOptions: FormArray;
  dynamicOptions: FormArray;
  fields: any[];
  options: any;
  formValue: any;

  formControl: AbstractControl;
  @Input() layoutNode: any;
  @Input() layoutIndex: number[];
  @Input() dataIndex: number[];

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private formBuilder: FormBuilder,
    private jsf: JsonSchemaFormService
  ) {}

  ngOnInit() {
    this.jsf.initializeControl(this);
    this.options = this.layoutNode.options || {};
    this.fields = this.options.fields || [];
    this.defaultOptions = this.formBuilder.array([]);
    this.dynamicOptions = this.formBuilder.array([]);
    let _formValue = null;
    if (this.formControl.value) {
      try {
        _formValue = JSON.parse(this.formControl.value);
      } catch (err) {}
    }
    this.setDefaultOptions(
      _formValue
        ? _formValue.defaultOptions
        : null || this.options.defaultOptions || []
    );
    this.setDynamicOptions(
      _formValue
        ? _formValue.dynamicOptions
        : null || this.options.dynamicOptions || []
    );
    this.optionsForm = this.formBuilder.group({
      defaultOptions: this.defaultOptions,
      dynamicOptions: this.dynamicOptions
    });
    this.updateValue();
    this.optionsForm.valueChanges.subscribe(() => this.updateValue());
  }

  createOptFormGroup(opt) {
    return this.formBuilder.group({
      ticked: opt.ticked,
      value: opt.value,
      displayValue: opt.displayValue
    });
  }

  setDefaultOptions(_defaultOptions) {
    _defaultOptions.forEach(defOpt =>
      this.defaultOptions.push(this.createOptFormGroup(defOpt))
    );
  }

  setDynamicOptions(_dynamicOptions) {
    _dynamicOptions.forEach(doOpt => {
      const options = this.formBuilder.array([]);
      doOpt.options.forEach(doOptOpt => {
        options.push(this.createOptFormGroup(doOptOpt));
      });
      this.dynamicOptions.push(
        this.formBuilder.group({
          dependsOn: doOpt.dependsOn,
          dependsOnValue: doOpt.dependsOnValue,
          options: options
        })
      );
    });
  }

  tickDefaultOption(defIndex, defaultOption) {
    if (!this.options.multi) {
      const target = defaultOption.get('ticked');
      target.valueChanges.pipe(take(1)).subscribe(value => {
        if (value) {
          const doValue = this.defaultOptions.value;
          this.defaultOptions.patchValue(
            doValue.map((opt, index) => {
              opt.ticked = index === defIndex;
              return opt;
            })
          );
        }
      });
    }
  }

  tickDynamicOptionsOption(dooIndex, dooOption, dynamicOption) {
    if (!this.options.multi) {
      const target = dooOption.get('ticked');
      target.valueChanges.pipe(take(1)).subscribe(value => {
        if (value) {
          const doValue = dynamicOption.value;
          doValue.options = doValue.options.map((opt, index) => {
            opt.ticked = index === dooIndex;
            return opt;
          });
          dynamicOption.patchValue(doValue);
        }
      });
    }
  }

  defaultOptionObj() {
    return {
      ticked: false,
      value: '',
      displayValue: ''
    };
  }

  addDefaultsOption() {
    this.defaultOptions.push(this.formBuilder.group(this.defaultOptionObj()));
  }

  removeDefaultsOption(defIndex) {
    this.defaultOptions.removeAt(defIndex);
  }

  addDynamicOptions() {
    // const options = this.formBuilder.array([]);
    this.dynamicOptions.push(
      this.formBuilder.group({
        dependsOn: '',
        dependsOnValue: '',
        options: this.formBuilder.array([])
      })
    );
  }

  removeDynamicOption(defIndex) {
    this.dynamicOptions.removeAt(defIndex);
  }

  addDynamicOptionsOption(doOptions) {
    doOptions.push(this.formBuilder.group(this.defaultOptionObj()));
  }

  duplicateDynamicOption(doOpt, doIndex) {
    const doValue = this.dynamicOptions.value;
    doValue.splice(doIndex + 1, 0, { ...doOpt.value });
    this.clearDynanmicOptions();
    this.changeDetectorRef.detectChanges();
    this.setDynamicOptions(doValue);
  }

  clearDynanmicOptions() {
    while (this.dynamicOptions.value.length !== 0) {
      this.dynamicOptions.removeAt(0);
    }
  }

  removeDynamicOptionsOption(doOptions, dooIndex) {
    doOptions.removeAt(dooIndex);
  }

  trackByFn(index: any, item: any) {
    return index;
  }

  updateValue() {
    this.jsf.updateValue(this, JSON.stringify(this.optionsForm.value));
    if (
      this.options.onChanges &&
      typeof this.options.onChanges === 'function'
    ) {
      this.options.onChanges(this.optionsForm.value);
    }
  }
}
