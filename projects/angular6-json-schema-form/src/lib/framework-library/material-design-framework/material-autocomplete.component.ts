import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';

import { JsonSchemaFormService } from '../../json-schema-form.service';

@Component({
  selector: 'material-autocomplete-widget',
  template: `
    <mat-form-field
      [class]="options?.htmlClass || ''"
      [floatLabel]="
        options?.floatLabel || (options?.notitle ? 'never' : 'auto')
      "
      [style.width]="'30%'"
    >
      <input
        matInput
        [formControl]="dummyFormControl"
        [matAutocomplete]="leftAuto"
        [placeholder]="options?.notitle ? options?.placeholder : options?.title"
        [style.width]="'100%'"
      />
      <mat-autocomplete
        #leftAuto="matAutocomplete"
        (optionSelected)="selectOption($event.option.value)"
      >
        <mat-option *ngFor="let option of enum" [value]="option">
          {{ option.name }}
        </mat-option>
      </mat-autocomplete>
    </mat-form-field>
  `
})
export class MaterialAutoCompleteComponent implements OnInit {
  formControl: AbstractControl;
  dummyFormControl = new FormControl();
  controlName: string;
  controlValue: any;
  controlDisabled = true;
  boundControl = false;
  options: any;
  isArray = true;
  @Input() layoutNode: any;
  @Input() layoutIndex: number[];
  @Input() dataIndex: number[];

  enum: any[];

  constructor(private jsf: JsonSchemaFormService) {}

  ngOnInit() {
    this.options = this.layoutNode.options || {};
    this.enum = this.options.enum || [];
    this.jsf.initializeControl(this);
    this.setValue(this.formControl.value);
    this.dummyFormControl.valueChanges.subscribe(value =>
      this.updateValue(value)
    );
  }

  selectOption(option) {
    const foundEnum = this.enum.find(e => e.id === option.id);
    this.dummyFormControl.setValue(foundEnum ? foundEnum.name : foundEnum);
  }

  displayWithName(opt) {
    return !opt ? '' : typeof opt === 'string' ? opt : opt.name;
  }

  setValue(value) {
    const foundEnum = this.enum.find(e => e.id === value);
    this.dummyFormControl.setValue(foundEnum ? foundEnum.name : foundEnum);
  }

  updateValue(value) {
    if (!value) {
      this.enum = this.options.enum || [];
      this.jsf.updateValue(this, null);
    } else if (typeof value === 'string') {
      this.enum = this.options.enum.filter(
        e => e.name.toLowerCase().indexOf(value.toLowerCase()) !== -1
      );
    } else {
      const foundEnum = this.enum.find(e => e.id === value.id);
      this.jsf.updateValue(this, foundEnum ? foundEnum.id : foundEnum);
    }
  }
}
