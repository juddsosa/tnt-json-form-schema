import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import {
  MatAutocompleteSelectedEvent,
  MatChipInputEvent,
  MatAutocomplete
} from '@angular/material';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

import { JsonSchemaFormService } from '../../json-schema-form.service';
import _ from 'lodash';

@Component({
  selector: 'material-chiplist-widget',
  template: `
    <mat-form-field
      [class]="options?.htmlClass || ''"
      [floatLabel]="
        options?.floatLabel || (options?.notitle ? 'never' : 'auto')
      "
      [style.width]="'100%'"
    >
      <mat-chip-list #chipList [style.width]="'100%'">
        <div id="currentChips">
          <mat-chip
            *ngFor="let opt of currentOpt"
            [selectable]="selectable"
            [removable]="enableDelete"
            (removed)="removeCurrent(opt)"
            (click)="onChipsClick(opt)"
          >
            <div
              matChipAvatar
              class="complete-marker"
              *ngIf="opt.complete != null && opt.complete != ''"
              [ngClass]="{ complete: opt.complete }"
            ></div>

            <span class="chip-name">{{ opt.name }}</span>
            <mat-icon matChipRemove *ngIf="enableDelete">cancel</mat-icon>
          </mat-chip>
        </div>
        <input
          matInput
          #chipInput
          [placeholder]="
            options?.notitle ? options?.placeholder : options?.title
          "
          [matAutocomplete]="auto"
          [matChipInputFor]="chipList"
          [matChipInputSeparatorKeyCodes]="separatorKeysCodes"
          [matChipInputAddOnBlur]="addOnBlur"
          (matChipInputTokenEnd)="add($event)"
          [style.width]="'100%'"
          [formControl]="dummyFormControl"
          *ngIf="enableInput"
        />
      </mat-chip-list>
      <div>
        <mat-chip
          *ngFor="let opt of selectedOpt"
          [selectable]="selectable"
          [removable]="enableDelete"
          (removed)="removeSelected(opt)"
          (click)="onChipsClick(opt)"
        >
          <div
            matChipAvatar
            class="complete-marker"
            *ngIf="opt.complete != null && opt.complete != ''"
            [ngClass]="{ complete: opt.complete }"
          ></div>

          <span class="chip-name">{{ opt.name }}</span>

          <mat-icon matChipRemove *ngIf="enableDelete">cancel</mat-icon>
        </mat-chip>
      </div>
      <mat-autocomplete
        #auto="matAutocomplete"
        class="chiplist-autocomp"
        [style.width]="'100%'"
        (optionSelected)="selected($event)"
      >
        <mat-option *ngFor="let fe of filteredEnum | async" [value]="fe">
          {{ fe.name }}
        </mat-option>
      </mat-autocomplete>
    </mat-form-field>
  `,
  styles: [
    `
      .mat-standard-chip {
        margin: 4px 8px 4px 0;
      }

      .chip-name {
        margin-top: 2px;
      }

      .mat-chip-remove {
        font-size: 32px;
        opacity: 1 !important;
      }

      .complete-marker {
        background-color: red;
      }

      .complete {
        background-color: green;
      }
    `
  ]
})
export class MaterialChiplistComponent implements OnInit {
  formControl: AbstractControl;
  dummyFormControl = new FormControl();
  controlName: string;
  controlValue: any;
  controlDisabled = true;
  boundControl = false;
  options: any;
  isArray = true;
  @Input() layoutNode: any;
  @Input() layoutIndex: number[];
  @Input() dataIndex: number[];

  selectable = true;
  addOnBlur = true;
  enableInput = true;
  enableDelete = true;
  enableClick = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  filteredEnum: Observable<string[]>;
  selectedOpt: string[] = [];
  currentOpt: string[] = [];

  @ViewChild('chipInput') chipInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;

  constructor(private jsf: JsonSchemaFormService) {}

  ngOnInit() {
    this.options = this.layoutNode.options || {};
    this.options.enum = this.options.enum || [];
    this.selectedOpt = this.options.selectedOpt || [];
    this.currentOpt = [];
    this.enableInput = this.options.enableInput;
    this.enableDelete = this.options.enableDelete;
    this.enableClick = this.options.enableClick;
    this.jsf.initializeControl(this);
    const _fcValue = JSON.parse(this.formControl.value);
    if (_fcValue && _fcValue.length) {
      for (const value of _fcValue) {
        value.complete != null
          ? this.selectedOpt.push(value)
          : this.currentOpt.push(value);
      }
    }
    this.selectedOpt = _.uniqBy(this.selectedOpt, 'name');
    this.updateValue();
    this.filteredEnum = this.dummyFormControl.valueChanges.pipe(
      startWith(null),
      map((fe: string | null) =>
        fe ? this.findOpts(fe, true) : this.options.enum.slice()
      )
    );
  }

  updateValue() {
    this.jsf.updateValue(
      this,
      JSON.stringify([...this.currentOpt, ...this.selectedOpt])
    );
    if (
      this.options.onChanges &&
      typeof this.options.onChanges === 'function'
    ) {
      this.options.onChanges(this.currentOpt);
    }
  }

  add(event: MatChipInputEvent): void {
    if (!this.matAutocomplete.isOpen) {
      const input = event.input;
      const value = event.value;

      if ((value || '').trim()) {
        this.addSelected(value);
      }

      // Reset the input value
      if (input) {
        input.value = '';
      }
      this.dummyFormControl.setValue(null);
      this.updateValue();
    }
  }

  removeSelected(opt: string): void {
    const index = this.selectedOpt.indexOf(opt);

    if (index >= 0) {
      this.selectedOpt.splice(index, 1);
    }
    this.updateValue();
  }

  removeCurrent(opt: string): void {
    const index = this.currentOpt.indexOf(opt);

    if (index >= 0) {
      this.currentOpt.splice(index, 1);
    }
    this.updateValue();
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    if (
      this.currentOpt.length < this.options.chipsLimit ||
      this.options.chipsLimit === 0
    ) {
      this.addSelected(event.option.viewValue);
      this.chipInput.nativeElement.value = '';
      this.dummyFormControl.setValue(null);
      this.updateValue();
    }
  }

  onChipsClick(value) {
    if (
      this.enableClick &&
      this.options.onChipsClick &&
      typeof this.options.onChipsClick === 'function'
    ) {
      this.options.onChipsClick(value);
    }
  }

  // private addSelected(value) {
  //   const _opt = this.findOpts(value);
  //   if (_opt) {
  //     const _selectedOpt = this.selectedOpt.filter(
  //       (so: any) => so.name.toLowerCase() === _opt.name.toLowerCase()
  //     );
  //     if (!_selectedOpt.length) {
  //       this.selectedOpt.push(_opt);
  //     }
  //   }
  // }

  private addSelected(value) {
    const _opt = this.findOpts(value);
    if (_opt) {
      const _currentOpt = this.currentOpt.filter(
        (so: any) => so.name.toLowerCase() === _opt.name.toLowerCase()
      );
      const _selectedOpt = this.selectedOpt.filter(
        (so: any) => so.name.toLowerCase() === _opt.name.toLowerCase()
      );
      if (!_currentOpt.length && !_selectedOpt.length) {
        this.currentOpt.push(_opt);
      }
    }
  }

  private findOpts(value, like: boolean = false) {
    if (value) {
      const _value = (value.name || value).trim().split(' | ');
      const _foundOpt = like
        ? this.options.enum.filter(
            opt => opt.name.toLowerCase().indexOf(_value[0].toLowerCase()) === 0
          )
        : this.options.enum.filter(
            opt => opt.name.toLowerCase() === _value[0].toLowerCase()
          );
      return _foundOpt.length ? (like ? _foundOpt : _foundOpt[0]) : null;
    }
    return null;
  }

  private _filter(value: any): string[] {
    const _opt = this.findOpts(value, true);
    return _opt
      ? this.options.enum.filter(
          opt => opt.name.toLowerCase().indexOf(_opt.name.toLowerCase()) === 0
        )
      : [];
  }
}
