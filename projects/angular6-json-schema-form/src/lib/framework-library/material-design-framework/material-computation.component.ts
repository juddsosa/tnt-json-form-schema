import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import {
  MatAutocompleteSelectedEvent,
  MatChipInputEvent,
  MatAutocomplete
} from '@angular/material';
import { Observable } from 'rxjs';
import { map, startWith, skipWhile } from 'rxjs/operators';

import { JsonSchemaFormService } from '../../json-schema-form.service';

@Component({
  selector: 'material-computation-widget',
  template: `
    <div fxLayout="column" fxLayoutAlign="start center">
      <div
        fxLayout="row"
        fxLayoutAlign="space-between center"
        class="function-container"
      >
        <mat-form-field
          [class]="options?.htmlClass || ''"
          [floatLabel]="
            options?.floatLabel || (options?.notitle ? 'never' : 'auto')
          "
          [style.width]="'30%'"
        >
          <mat-select
            [formControl]="outputTypeControl"
            placeholder="Output Type"
          >
            <mat-option
              *ngFor="let ot of outputType"
              [value]="ot"
              [attr.selected]="ot === outputTypeControl.value"
            >
              {{ ot }}
            </mat-option>
          </mat-select>
        </mat-form-field>

        <mat-form-field
          [class]="options?.htmlClass || ''"
          [floatLabel]="
            options?.floatLabel || (options?.notitle ? 'never' : 'auto')
          "
          [style.width]="'30%'"
        >
          <mat-select
            [formControl]="functionTypeControl"
            placeholder="Function Type"
          >
            <mat-option (click)="functionTypeControl.reset()"></mat-option>
            <mat-option
              *ngFor="let ft of functionType"
              [value]="ft"
              [attr.selected]="ft === functionTypeControl.value"
            >
              {{ ft }}
            </mat-option>
          </mat-select>
        </mat-form-field>

        <mat-form-field
          [class]="options?.htmlClass || ''"
          [floatLabel]="
            options?.floatLabel || (options?.notitle ? 'never' : 'auto')
          "
          [style.width]="'30%'"
        >
          <input
            matInput
            [formControl]="functionValueControl"
            placeholder="Function Value"
          />
        </mat-form-field>
      </div>
      <div
        fxLayout="row"
        fxLayoutAlign="start start"
        class="computations-container"
      >
        <mat-form-field
          [class]="options?.htmlClass || ''"
          [floatLabel]="
            options?.floatLabel || (options?.notitle ? 'never' : 'auto')
          "
          [style.width]="'30%'"
        >
          <input
            matInput
            [formControl]="copyToControl"
            [matAutocomplete]="leftAuto"
            placeholder="Copy To"
            [style.width]="'100%'"
          />
          <mat-autocomplete #leftAuto="matAutocomplete">
            <mat-option
              *ngFor="let option of filteredLeftEnum | async"
              [value]="option.name"
            >
              {{ option.name }}
            </mat-option>
          </mat-autocomplete>
        </mat-form-field>

        <p
          [ngStyle]="{
            'text-align': 'center',
            width: '5%',
            margin: '28px 0 0 0'
          }"
        >
          =
        </p>

        <mat-form-field
          [class]="options?.htmlClass || ''"
          [floatLabel]="
            options?.floatLabel || (options?.notitle ? 'never' : 'auto')
          "
          [style.width]="'65%'"
        >
          <mat-chip-list #chipList [style.width]="'100%'">
            <div class="right-chiplist-content">
              <input
                matInput
                #chipInput
                [formControl]="joinTheseControl"
                [matAutocomplete]="rightAuto"
                [matChipInputFor]="chipList"
                [matChipInputAddOnBlur]="addOnBlur"
                [matChipInputSeparatorKeyCodes]="separatorKeysCodes"
                (matChipInputTokenEnd)="add($event)"
                placeholder="Join these"
                [style.width]="'100%'"
              />
              <mat-chip
                *ngFor="let opt of selectedRightOpt"
                [selectable]="selectable"
                [removable]="removable"
                (removed)="remove(opt)"
              >
                <span>{{ opt.name || opt.value }}</span>
                <mat-icon matChipRemove>cancel</mat-icon>
              </mat-chip>
            </div>
          </mat-chip-list>
          <mat-autocomplete
            #rightAuto="matAutocomplete"
            (optionSelected)="selected($event)"
            [style.width]="'100%'"
          >
            <mat-option
              *ngFor="let option of filteredRightEnum | async"
              [value]="option.name"
            >
              {{ option.name }}
            </mat-option>
          </mat-autocomplete>
        </mat-form-field>
      </div>
    </div>
  `,
  styles: [
    `
      .function-container {
        width: 100%;
      }

      .computations-container {
        width: 100%;
      }

      .right-chiplist-content {
        width: 100%;
      }

      .mat-chip-remove {
        background: grey;
        border-radius: 50%;
        color: white !important;
        font-size: 18px;
        font-weight: bold;
      }
    `
  ]
})
export class MaterialComputationComponent implements OnInit {
  formControl: AbstractControl;
  outputTypeControl = new FormControl('Alphanumeric');
  functionTypeControl = new FormControl();
  functionValueControl = new FormControl();
  copyToControl = new FormControl();
  joinTheseControl = new FormControl();
  controlName: string;
  controlValue: any;
  controlDisabled = true;
  boundControl = false;
  options: any;
  isArray = true;
  @Input() layoutNode: any;
  @Input() layoutIndex: number[];
  @Input() dataIndex: number[];

  selectable = true;
  addOnBlur = true;
  removable = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  leftEnum: any[];
  rightEnum: any[];
  filteredLeftEnum: Observable<any[]>;
  filteredRightEnum: Observable<any[]>;
  selectedLeftOpt = '';
  selectedRightOpt: any[] = [];
  outputType: string[] = ['Alphanumeric', 'Numeric'];
  functionType: string[] = ['First', 'Last'];

  @ViewChild('chipInput') chipInput: ElementRef<HTMLInputElement>;
  @ViewChild('rightAuto') matAutocomplete: MatAutocomplete;

  constructor(private jsf: JsonSchemaFormService) {}

  ngOnInit() {
    this.options = this.layoutNode.options || {};
    this.leftEnum = this.options.leftEnum || [];
    this.rightEnum = this.options.rightEnum || [];
    this.selectedLeftOpt = this.options.selectedLeftOpt || '';
    this.selectedRightOpt = this.options.selectedRightOpt || [];
    this.jsf.initializeControl(this);
    if (this.formControl.value) {
      const _fcValue = JSON.parse(this.formControl.value);

      Object.keys(_fcValue).forEach(key => {
        if (key === 'formula') {
          this.outputTypeControl.setValue(
            _fcValue[key].outputType || 'Alphanumeric'
          );
          this.functionTypeControl.setValue(_fcValue[key].functionType);
          this.functionValueControl.setValue(_fcValue[key].functionValue);
        } else {
          const foundLeftEnum = this.leftEnum.find(le => le.field === key);
          this.selectedLeftOpt = foundLeftEnum ? foundLeftEnum.name : '';
          this.selectedRightOpt = _fcValue[key] || [];
        }
      });
    }
    this.copyToControl.setValue(this.selectedLeftOpt);
    this.updateValue();
    if (
      !this.options.notitle &&
      !this.options.description &&
      this.options.placeholder
    ) {
      this.options.description = this.options.placeholder;
    }

    this.filteredLeftEnum = this.copyToControl.valueChanges.pipe(
      startWith(null),
      map((fre: string | null) =>
        fre ? this.filterLeftEnum(fre) : this.rightEnum.slice()
      )
    );

    this.filteredRightEnum = this.joinTheseControl.valueChanges.pipe(
      startWith(null),
      map((fre: string | null) =>
        fre ? this.filterRightEnum(fre) : this.rightEnum.slice()
      )
    );

    this.copyToControl.valueChanges
      .pipe(
        skipWhile(value => {
          if (this.options.enableCustomInput) {
            return false;
          }
          return this.leftEnum.findIndex(
            le => le.name.toLowerCase() === value.split('.')[0].toLowerCase()
          ) !== -1
            ? false
            : true;
        })
      )
      .subscribe(value => this.updateValue());

    this.outputTypeControl.valueChanges.subscribe(value => this.updateValue());
    this.functionTypeControl.valueChanges.subscribe(value => {
      this.updateValue();
    });
    this.functionValueControl.valueChanges.subscribe(value =>
      this.updateValue()
    );
  }

  updateValue() {
    const foundLeftEnum = this.leftEnum.find(
      le => le.name === this.copyToControl.value
    );
    const outputKey = foundLeftEnum ? foundLeftEnum.field : this.options.name;
    const _output = {
      formula: {
        outputType: this.outputTypeControl.value,
        functionType: this.functionTypeControl.value,
        functionValue: this.functionValueControl.value
      },
      [outputKey]: this.selectedRightOpt
    };
    this.jsf.updateValue(this, JSON.stringify(_output));
    if (
      this.options.onChanges &&
      typeof this.options.onChanges === 'function'
    ) {
      this.options.onChanges(_output);
    }
  }

  private filterLeftEnum(fle) {
    return this.leftEnum.filter(
      opt => opt.name.toLowerCase().indexOf(fle.toLowerCase()) > -1
    );
  }

  private filterRightEnum(fre) {
    return this.rightEnum.filter(
      opt => opt.name.toLowerCase().indexOf(fre.toLowerCase()) > -1
    );
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if (value) {
      this.addSelected(value);
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
    this.joinTheseControl.setValue(null);
    this.updateValue();
  }

  private addSelected(value) {
    const foundRightEnum = this.rightEnum.find(re => re.name === value);
    this.selectedRightOpt.push(
      foundRightEnum
        ? foundRightEnum
        : { id: null, name: '', field: '', value: value }
    );
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.addSelected(event.option.viewValue);
    this.chipInput.nativeElement.value = '';
    this.joinTheseControl.setValue(null);
    this.updateValue();
  }

  remove(opt: string): void {
    const index = this.selectedRightOpt.indexOf(opt);

    if (index >= 0) {
      this.selectedRightOpt.splice(index, 1);
    }
    this.updateValue();
  }
}
