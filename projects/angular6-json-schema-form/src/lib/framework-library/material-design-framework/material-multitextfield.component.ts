import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  FormArray
} from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';

import { JsonSchemaFormService } from '../../json-schema-form.service';

@Component({
  selector: 'material-multitextfield-widget',
  template: `
    <mat-label>{{ options.title }}</mat-label>
    <form [formGroup]="multiTextFieldForm">
      <div formArrayName="textfield">
        <div
          fxLayout="row"
          fxLayoutAlign="center center"
          fxLayoutGap="10px"
          *ngFor="let item of textfield.controls; let pointIndex = index"
          [formGroupName]="pointIndex"
        >
          <div fxFlex="90">
            <mat-form-field>
              <input matInput formControlName='value'/>
            </mat-form-field>
          </div>
          <div fxFlex="10" fxLayoutGap="10px">
            <mat-icon (click)="add()">add</mat-icon>
            <mat-icon (click)="delete(pointIndex)">remove</mat-icon>
          </div>
        </div>
      </div>
    </form>
  `,
  styles: [
    `
      .mat-form-field {
        width: 100%;
      }
    `
  ]
})
export class MaterialMultiTextFieldComponent implements OnInit {
  formControl: AbstractControl;
  @Input() layoutNode: any;
  @Input() layoutIndex: number[];
  @Input() dataIndex: number[];
  options: any;
  multiTextFieldForm: FormGroup;

  constructor(private fb: FormBuilder, private jsf: JsonSchemaFormService) {}

  ngOnInit() {
    this.jsf.initializeControl(this);
    this.multiTextFieldForm = this.fb.group({
      textfield: this.fb.array([this.fb.group({ value: '' })])
    });
    this.multiTextFieldForm.valueChanges.subscribe(() => {
      this.update();
    });
    this.insertInitial();
  }

  get textfield() {
    return this.multiTextFieldForm.get('textfield') as FormArray;
  }

  add(item = '') {
    this.textfield.push(this.fb.group({ value: item }));
  }

  delete(index) {
    this.textfield.removeAt(index);
  }

  update() {
    const answers = this.multiTextFieldForm.value.textfield.map(
      obj => obj.value
    );
    for (let i = 0; i < answers.length; i++) {
      if (answers[i] === '') {
        answers.splice(i, 1);
      }
    }
    this.jsf.updateValue(this, JSON.stringify(answers));
  }

  insertInitial() {
    const parsedValue = JSON.parse(this.formControl.value);
    if (parsedValue && parsedValue.length) {
      this.delete(0);
      for (const item of parsedValue) {
        if (item) {
          this.add(item);
        }
      }
    }
  }
}
