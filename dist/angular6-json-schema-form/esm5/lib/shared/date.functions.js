/**
 * 'dateToString' function
 *
 * //  { Date | string } date
 * //   options
 * // { string }
 */
export function dateToString(date, options) {
    if (options === void 0) { options = {}; }
    var dateFormat = options.dateFormat || 'YYYY-MM-DD';
    // TODO: Use options.locale to change default format and names
    // const locale = options.locale || 'en-US';
    if (typeof date === 'string') {
        date = stringToDate(date);
    }
    if (Object.prototype.toString.call(date) !== '[object Date]') {
        return null;
    }
    var longMonths = ['January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December'];
    var shortMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var longDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    var shortDays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    return dateFormat
        .replace(/YYYY/ig, date.getFullYear() + '')
        .replace(/YY/ig, (date.getFullYear() + '').slice(-2))
        .replace(/MMMM/ig, longMonths[date.getMonth()])
        .replace(/MMM/ig, shortMonths[date.getMonth()])
        .replace(/MM/ig, ('0' + (date.getMonth() + 1)).slice(-2))
        .replace(/M/ig, (date.getMonth() + 1) + '')
        .replace(/DDDD/ig, longDays[date.getDay()])
        .replace(/DDD/ig, shortDays[date.getDay()])
        .replace(/DD/ig, ('0' + date.getDate()).slice(-2))
        .replace(/D/ig, date.getDate() + '')
        .replace(/S/ig, ordinal(date.getDate()));
}
export function ordinal(number) {
    if (typeof number === 'number') {
        number = number + '';
    }
    var last = number.slice(-1);
    var nextToLast = number.slice(-2, 1);
    return (nextToLast !== '1' && { '1': 'st', '2': 'nd', '3': 'rd' }[last]) || 'th';
}
/**
 * 'stringToDate' function
 *
 * //  { string } dateString
 * // { Date }
 */
export function stringToDate(dateString) {
    var getDate = findDate(dateString);
    if (!getDate) {
        return null;
    }
    var dateParts = [];
    // Split x-y-z to [x, y, z]
    if (/^\d+[^\d]\d+[^\d]\d+$/.test(getDate)) {
        dateParts = getDate.split(/[^\d]/).map(function (part) { return +part; });
        // Split xxxxyyzz to [xxxx, yy, zz]
    }
    else if (/^\d{8}$/.test(getDate)) {
        dateParts = [+getDate.slice(0, 4), +getDate.slice(4, 6), +getDate.slice(6)];
    }
    var thisYear = +(new Date().getFullYear() + '').slice(-2);
    // Check for [YYYY, MM, DD]
    if (dateParts[0] > 1000 && dateParts[0] < 2100 && dateParts[1] <= 12 && dateParts[2] <= 31) {
        return new Date(dateParts[0], dateParts[1] - 1, dateParts[2]);
        // Check for [MM, DD, YYYY]
    }
    else if (dateParts[0] <= 12 && dateParts[1] <= 31 && dateParts[2] > 1000 && dateParts[2] < 2100) {
        return new Date(dateParts[2], dateParts[0] - 1, dateParts[1]);
        // Check for [MM, DD, YY]
    }
    else if (dateParts[0] <= 12 && dateParts[1] <= 31 && dateParts[2] < 100) {
        var year = (dateParts[2] <= thisYear ? 2000 : 1900) + dateParts[2];
        return new Date(year, dateParts[0] - 1, dateParts[1]);
        // Check for [YY, MM, DD]
    }
    else if (dateParts[0] < 100 && dateParts[1] <= 12 && dateParts[2] <= 31) {
        var year = (dateParts[0] <= thisYear ? 2000 : 1900) + dateParts[0];
        return new Date(year, dateParts[1] - 1, dateParts[2]);
    }
    return null;
}
/**
 * 'findDate' function
 *
 * //  { string } text
 * // { string }
 */
export function findDate(text) {
    if (!text) {
        return null;
    }
    var foundDate;
    // Match ...YYYY-MM-DD...
    foundDate = text.match(/(?:19|20)\d\d[-_\\\/\. ](?:0?\d|1[012])[-_\\\/\. ](?:[012]?\d|3[01])(?!\d)/);
    if (foundDate) {
        return foundDate[0];
    }
    // Match ...MM-DD-YYYY...
    foundDate = text.match(/(?:[012]?\d|3[01])[-_\\\/\. ](?:0?\d|1[012])[-_\\\/\. ](?:19|20)\d\d(?!\d)/);
    if (foundDate) {
        return foundDate[0];
    }
    // Match MM-DD-YY...
    foundDate = text.match(/^(?:[012]?\d|3[01])[-_\\\/\. ](?:0?\d|1[012])[-_\\\/\. ]\d\d(?!\d)/);
    if (foundDate) {
        return foundDate[0];
    }
    // Match YY-MM-DD...
    foundDate = text.match(/^\d\d[-_\\\/\. ](?:[012]?\d|3[01])[-_\\\/\. ](?:0?\d|1[012])(?!\d)/);
    if (foundDate) {
        return foundDate[0];
    }
    // Match YYYYMMDD...
    foundDate = text.match(/^(?:19|20)\d\d(?:0\d|1[012])(?:[012]\d|3[01])/);
    if (foundDate) {
        return foundDate[0];
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZS5mdW5jdGlvbnMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hbmd1bGFyNi1qc29uLXNjaGVtYS1mb3JtLyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9kYXRlLmZ1bmN0aW9ucy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7O0dBTUc7QUFDSCxNQUFNLFVBQVUsWUFBWSxDQUFDLElBQUksRUFBRSxPQUFpQjtJQUFqQix3QkFBQSxFQUFBLFlBQWlCO0lBQ2xELElBQU0sVUFBVSxHQUFHLE9BQU8sQ0FBQyxVQUFVLElBQUksWUFBWSxDQUFDO0lBQ3RELDhEQUE4RDtJQUM5RCw0Q0FBNEM7SUFDNUMsSUFBSSxPQUFPLElBQUksS0FBSyxRQUFRLEVBQUU7UUFBRSxJQUFJLEdBQUcsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO0tBQUU7SUFDNUQsSUFBSSxNQUFNLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssZUFBZSxFQUFFO1FBQUUsT0FBTyxJQUFJLENBQUM7S0FBRTtJQUM5RSxJQUFNLFVBQVUsR0FBRyxDQUFDLFNBQVMsRUFBRSxVQUFVLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTTtRQUN4RSxNQUFNLEVBQUUsUUFBUSxFQUFFLFdBQVcsRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQ3BFLElBQU0sV0FBVyxHQUFHLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztJQUN6RyxJQUFNLFFBQVEsR0FBRyxDQUFDLFFBQVEsRUFBRSxRQUFRLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQ2hHLElBQU0sU0FBUyxHQUFHLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDcEUsT0FBTyxVQUFVO1NBQ2QsT0FBTyxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLEdBQUcsRUFBRSxDQUFDO1NBQzFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDcEQsT0FBTyxDQUFDLFFBQVEsRUFBRSxVQUFVLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7U0FDOUMsT0FBTyxDQUFDLE9BQU8sRUFBRSxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7U0FDOUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ3hELE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLEdBQUcsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDO1NBQzFDLE9BQU8sQ0FBQyxRQUFRLEVBQUUsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO1NBQzFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO1NBQzFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDakQsT0FBTyxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFFLEdBQUcsRUFBRSxDQUFDO1NBQ25DLE9BQU8sQ0FBQyxLQUFLLEVBQUUsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLENBQUM7QUFDN0MsQ0FBQztBQUVELE1BQU0sVUFBVSxPQUFPLENBQUMsTUFBcUI7SUFDM0MsSUFBSSxPQUFPLE1BQU0sS0FBSyxRQUFRLEVBQUU7UUFBRSxNQUFNLEdBQUcsTUFBTSxHQUFHLEVBQUUsQ0FBQztLQUFFO0lBQ3pELElBQU0sSUFBSSxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUM5QixJQUFNLFVBQVUsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQ3ZDLE9BQU8sQ0FBQyxVQUFVLEtBQUssR0FBRyxJQUFJLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQztBQUNuRixDQUFDO0FBRUQ7Ozs7O0dBS0c7QUFDSCxNQUFNLFVBQVUsWUFBWSxDQUFDLFVBQVU7SUFDckMsSUFBTSxPQUFPLEdBQVcsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQzdDLElBQUksQ0FBQyxPQUFPLEVBQUU7UUFBRSxPQUFPLElBQUksQ0FBQztLQUFFO0lBQzlCLElBQUksU0FBUyxHQUFhLEVBQUUsQ0FBQztJQUM3QiwyQkFBMkI7SUFDM0IsSUFBSSx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUU7UUFDekMsU0FBUyxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsQ0FBQyxJQUFJLEVBQUwsQ0FBSyxDQUFDLENBQUM7UUFDeEQsbUNBQW1DO0tBQ2xDO1NBQU0sSUFBSSxTQUFTLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFO1FBQ2xDLFNBQVMsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztLQUM3RTtJQUNELElBQU0sUUFBUSxHQUFHLENBQUMsQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFDLFdBQVcsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzVELDJCQUEyQjtJQUMzQixJQUFJLFNBQVMsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLElBQUksU0FBUyxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksSUFBSSxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxJQUFJLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQUU7UUFDMUYsT0FBTyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEVBQUUsU0FBUyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNoRSwyQkFBMkI7S0FDMUI7U0FBTSxJQUFJLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLElBQUksU0FBUyxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxTQUFTLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxJQUFJLFNBQVMsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLEVBQUU7UUFDakcsT0FBTyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEVBQUUsU0FBUyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNoRSx5QkFBeUI7S0FDeEI7U0FBTSxJQUFJLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLElBQUksU0FBUyxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxTQUFTLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxFQUFFO1FBQ3pFLElBQU0sSUFBSSxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxJQUFJLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDckUsT0FBTyxJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN4RCx5QkFBeUI7S0FDeEI7U0FBTSxJQUFJLFNBQVMsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLElBQUksU0FBUyxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFO1FBQ3pFLElBQU0sSUFBSSxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxJQUFJLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDckUsT0FBTyxJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztLQUN2RDtJQUNELE9BQU8sSUFBSSxDQUFDO0FBQ2QsQ0FBQztBQUVEOzs7OztHQUtHO0FBQ0gsTUFBTSxVQUFVLFFBQVEsQ0FBQyxJQUFJO0lBQzNCLElBQUksQ0FBQyxJQUFJLEVBQUU7UUFBRSxPQUFPLElBQUksQ0FBQztLQUFFO0lBQzNCLElBQUksU0FBZ0IsQ0FBQztJQUNyQix5QkFBeUI7SUFDekIsU0FBUyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsNEVBQTRFLENBQUMsQ0FBQztJQUNyRyxJQUFJLFNBQVMsRUFBRTtRQUFFLE9BQU8sU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO0tBQUU7SUFDdkMseUJBQXlCO0lBQ3pCLFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLDRFQUE0RSxDQUFDLENBQUM7SUFDckcsSUFBSSxTQUFTLEVBQUU7UUFBRSxPQUFPLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztLQUFFO0lBQ3ZDLG9CQUFvQjtJQUNwQixTQUFTLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxvRUFBb0UsQ0FBQyxDQUFDO0lBQzdGLElBQUksU0FBUyxFQUFFO1FBQUUsT0FBTyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7S0FBRTtJQUN2QyxvQkFBb0I7SUFDcEIsU0FBUyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsb0VBQW9FLENBQUMsQ0FBQztJQUM3RixJQUFJLFNBQVMsRUFBRTtRQUFFLE9BQU8sU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO0tBQUU7SUFDdkMsb0JBQW9CO0lBQ3BCLFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLCtDQUErQyxDQUFDLENBQUM7SUFDeEUsSUFBSSxTQUFTLEVBQUU7UUFBRSxPQUFPLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztLQUFFO0FBQ3pDLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcclxuICogJ2RhdGVUb1N0cmluZycgZnVuY3Rpb25cclxuICpcclxuICogLy8gIHsgRGF0ZSB8IHN0cmluZyB9IGRhdGVcclxuICogLy8gICBvcHRpb25zXHJcbiAqIC8vIHsgc3RyaW5nIH1cclxuICovXHJcbmV4cG9ydCBmdW5jdGlvbiBkYXRlVG9TdHJpbmcoZGF0ZSwgb3B0aW9uczogYW55ID0ge30pIHtcclxuICBjb25zdCBkYXRlRm9ybWF0ID0gb3B0aW9ucy5kYXRlRm9ybWF0IHx8ICdZWVlZLU1NLUREJztcclxuICAvLyBUT0RPOiBVc2Ugb3B0aW9ucy5sb2NhbGUgdG8gY2hhbmdlIGRlZmF1bHQgZm9ybWF0IGFuZCBuYW1lc1xyXG4gIC8vIGNvbnN0IGxvY2FsZSA9IG9wdGlvbnMubG9jYWxlIHx8ICdlbi1VUyc7XHJcbiAgaWYgKHR5cGVvZiBkYXRlID09PSAnc3RyaW5nJykgeyBkYXRlID0gc3RyaW5nVG9EYXRlKGRhdGUpOyB9XHJcbiAgaWYgKE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbChkYXRlKSAhPT0gJ1tvYmplY3QgRGF0ZV0nKSB7IHJldHVybiBudWxsOyB9XHJcbiAgY29uc3QgbG9uZ01vbnRocyA9IFsnSmFudWFyeScsICdGZWJydWFyeScsICdNYXJjaCcsICdBcHJpbCcsICdNYXknLCAnSnVuZScsXHJcbiAgICAnSnVseScsICdBdWd1c3QnLCAnU2VwdGVtYmVyJywgJ09jdG9iZXInLCAnTm92ZW1iZXInLCAnRGVjZW1iZXInXTtcclxuICBjb25zdCBzaG9ydE1vbnRocyA9IFsnSmFuJywgJ0ZlYicsICdNYXInLCAnQXByJywgJ01heScsICdKdW4nLCAnSnVsJywgJ0F1ZycsICdTZXAnLCAnT2N0JywgJ05vdicsICdEZWMnXTtcclxuICBjb25zdCBsb25nRGF5cyA9IFsnU3VuZGF5JywgJ01vbmRheScsICdUdWVzZGF5JywgJ1dlZG5lc2RheScsICdUaHVyc2RheScsICdGcmlkYXknLCAnU2F0dXJkYXknXTtcclxuICBjb25zdCBzaG9ydERheXMgPSBbJ1N1bicsICdNb24nLCAnVHVlJywgJ1dlZCcsICdUaHUnLCAnRnJpJywgJ1NhdCddO1xyXG4gIHJldHVybiBkYXRlRm9ybWF0XHJcbiAgICAucmVwbGFjZSgvWVlZWS9pZywgZGF0ZS5nZXRGdWxsWWVhcigpICsgJycpXHJcbiAgICAucmVwbGFjZSgvWVkvaWcsIChkYXRlLmdldEZ1bGxZZWFyKCkgKyAnJykuc2xpY2UoLTIpKVxyXG4gICAgLnJlcGxhY2UoL01NTU0vaWcsIGxvbmdNb250aHNbZGF0ZS5nZXRNb250aCgpXSlcclxuICAgIC5yZXBsYWNlKC9NTU0vaWcsIHNob3J0TW9udGhzW2RhdGUuZ2V0TW9udGgoKV0pXHJcbiAgICAucmVwbGFjZSgvTU0vaWcsICgnMCcgKyAoZGF0ZS5nZXRNb250aCgpICsgMSkpLnNsaWNlKC0yKSlcclxuICAgIC5yZXBsYWNlKC9NL2lnLCAoZGF0ZS5nZXRNb250aCgpICsgMSkgKyAnJylcclxuICAgIC5yZXBsYWNlKC9EREREL2lnLCBsb25nRGF5c1tkYXRlLmdldERheSgpXSlcclxuICAgIC5yZXBsYWNlKC9EREQvaWcsIHNob3J0RGF5c1tkYXRlLmdldERheSgpXSlcclxuICAgIC5yZXBsYWNlKC9ERC9pZywgKCcwJyArIGRhdGUuZ2V0RGF0ZSgpKS5zbGljZSgtMikpXHJcbiAgICAucmVwbGFjZSgvRC9pZywgZGF0ZS5nZXREYXRlKCkgKyAnJylcclxuICAgIC5yZXBsYWNlKC9TL2lnLCBvcmRpbmFsKGRhdGUuZ2V0RGF0ZSgpKSk7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBvcmRpbmFsKG51bWJlcjogbnVtYmVyfHN0cmluZyk6IHN0cmluZyB7XHJcbiAgaWYgKHR5cGVvZiBudW1iZXIgPT09ICdudW1iZXInKSB7IG51bWJlciA9IG51bWJlciArICcnOyB9XHJcbiAgY29uc3QgbGFzdCA9IG51bWJlci5zbGljZSgtMSk7XHJcbiAgY29uc3QgbmV4dFRvTGFzdCA9IG51bWJlci5zbGljZSgtMiwgMSk7XHJcbiAgcmV0dXJuIChuZXh0VG9MYXN0ICE9PSAnMScgJiYgeyAnMSc6ICdzdCcsICcyJzogJ25kJywgJzMnOiAncmQnIH1bbGFzdF0pIHx8ICd0aCc7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiAnc3RyaW5nVG9EYXRlJyBmdW5jdGlvblxyXG4gKlxyXG4gKiAvLyAgeyBzdHJpbmcgfSBkYXRlU3RyaW5nXHJcbiAqIC8vIHsgRGF0ZSB9XHJcbiAqL1xyXG5leHBvcnQgZnVuY3Rpb24gc3RyaW5nVG9EYXRlKGRhdGVTdHJpbmcpIHtcclxuICBjb25zdCBnZXREYXRlOiBzdHJpbmcgPSBmaW5kRGF0ZShkYXRlU3RyaW5nKTtcclxuICBpZiAoIWdldERhdGUpIHsgcmV0dXJuIG51bGw7IH1cclxuICBsZXQgZGF0ZVBhcnRzOiBudW1iZXJbXSA9IFtdO1xyXG4gIC8vIFNwbGl0IHgteS16IHRvIFt4LCB5LCB6XVxyXG4gIGlmICgvXlxcZCtbXlxcZF1cXGQrW15cXGRdXFxkKyQvLnRlc3QoZ2V0RGF0ZSkpIHtcclxuICAgIGRhdGVQYXJ0cyA9IGdldERhdGUuc3BsaXQoL1teXFxkXS8pLm1hcChwYXJ0ID0+ICtwYXJ0KTtcclxuICAvLyBTcGxpdCB4eHh4eXl6eiB0byBbeHh4eCwgeXksIHp6XVxyXG4gIH0gZWxzZSBpZiAoL15cXGR7OH0kLy50ZXN0KGdldERhdGUpKSB7XHJcbiAgICBkYXRlUGFydHMgPSBbK2dldERhdGUuc2xpY2UoMCwgNCksICtnZXREYXRlLnNsaWNlKDQsIDYpLCArZ2V0RGF0ZS5zbGljZSg2KV07XHJcbiAgfVxyXG4gIGNvbnN0IHRoaXNZZWFyID0gKyhuZXcgRGF0ZSgpLmdldEZ1bGxZZWFyKCkgKyAnJykuc2xpY2UoLTIpO1xyXG4gIC8vIENoZWNrIGZvciBbWVlZWSwgTU0sIEREXVxyXG4gIGlmIChkYXRlUGFydHNbMF0gPiAxMDAwICYmIGRhdGVQYXJ0c1swXSA8IDIxMDAgJiYgZGF0ZVBhcnRzWzFdIDw9IDEyICYmIGRhdGVQYXJ0c1syXSA8PSAzMSkge1xyXG4gICAgcmV0dXJuIG5ldyBEYXRlKGRhdGVQYXJ0c1swXSwgZGF0ZVBhcnRzWzFdIC0gMSwgZGF0ZVBhcnRzWzJdKTtcclxuICAvLyBDaGVjayBmb3IgW01NLCBERCwgWVlZWV1cclxuICB9IGVsc2UgaWYgKGRhdGVQYXJ0c1swXSA8PSAxMiAmJiBkYXRlUGFydHNbMV0gPD0gMzEgJiYgZGF0ZVBhcnRzWzJdID4gMTAwMCAmJiBkYXRlUGFydHNbMl0gPCAyMTAwKSB7XHJcbiAgICByZXR1cm4gbmV3IERhdGUoZGF0ZVBhcnRzWzJdLCBkYXRlUGFydHNbMF0gLSAxLCBkYXRlUGFydHNbMV0pO1xyXG4gIC8vIENoZWNrIGZvciBbTU0sIERELCBZWV1cclxuICB9IGVsc2UgaWYgKGRhdGVQYXJ0c1swXSA8PSAxMiAmJiBkYXRlUGFydHNbMV0gPD0gMzEgJiYgZGF0ZVBhcnRzWzJdIDwgMTAwKSB7XHJcbiAgICBjb25zdCB5ZWFyID0gKGRhdGVQYXJ0c1syXSA8PSB0aGlzWWVhciA/IDIwMDAgOiAxOTAwKSArIGRhdGVQYXJ0c1syXTtcclxuICAgIHJldHVybiBuZXcgRGF0ZSh5ZWFyLCBkYXRlUGFydHNbMF0gLSAxLCBkYXRlUGFydHNbMV0pO1xyXG4gIC8vIENoZWNrIGZvciBbWVksIE1NLCBERF1cclxuICB9IGVsc2UgaWYgKGRhdGVQYXJ0c1swXSA8IDEwMCAmJiBkYXRlUGFydHNbMV0gPD0gMTIgJiYgZGF0ZVBhcnRzWzJdIDw9IDMxKSB7XHJcbiAgICBjb25zdCB5ZWFyID0gKGRhdGVQYXJ0c1swXSA8PSB0aGlzWWVhciA/IDIwMDAgOiAxOTAwKSArIGRhdGVQYXJ0c1swXTtcclxuICAgIHJldHVybiBuZXcgRGF0ZSh5ZWFyLCBkYXRlUGFydHNbMV0gLSAxLCBkYXRlUGFydHNbMl0pO1xyXG4gIH1cclxuICByZXR1cm4gbnVsbDtcclxufVxyXG5cclxuLyoqXHJcbiAqICdmaW5kRGF0ZScgZnVuY3Rpb25cclxuICpcclxuICogLy8gIHsgc3RyaW5nIH0gdGV4dFxyXG4gKiAvLyB7IHN0cmluZyB9XHJcbiAqL1xyXG5leHBvcnQgZnVuY3Rpb24gZmluZERhdGUodGV4dCkge1xyXG4gIGlmICghdGV4dCkgeyByZXR1cm4gbnVsbDsgfVxyXG4gIGxldCBmb3VuZERhdGU6IGFueVtdO1xyXG4gIC8vIE1hdGNoIC4uLllZWVktTU0tREQuLi5cclxuICBmb3VuZERhdGUgPSB0ZXh0Lm1hdGNoKC8oPzoxOXwyMClcXGRcXGRbLV9cXFxcXFwvXFwuIF0oPzowP1xcZHwxWzAxMl0pWy1fXFxcXFxcL1xcLiBdKD86WzAxMl0/XFxkfDNbMDFdKSg/IVxcZCkvKTtcclxuICBpZiAoZm91bmREYXRlKSB7IHJldHVybiBmb3VuZERhdGVbMF07IH1cclxuICAvLyBNYXRjaCAuLi5NTS1ERC1ZWVlZLi4uXHJcbiAgZm91bmREYXRlID0gdGV4dC5tYXRjaCgvKD86WzAxMl0/XFxkfDNbMDFdKVstX1xcXFxcXC9cXC4gXSg/OjA/XFxkfDFbMDEyXSlbLV9cXFxcXFwvXFwuIF0oPzoxOXwyMClcXGRcXGQoPyFcXGQpLyk7XHJcbiAgaWYgKGZvdW5kRGF0ZSkgeyByZXR1cm4gZm91bmREYXRlWzBdOyB9XHJcbiAgLy8gTWF0Y2ggTU0tREQtWVkuLi5cclxuICBmb3VuZERhdGUgPSB0ZXh0Lm1hdGNoKC9eKD86WzAxMl0/XFxkfDNbMDFdKVstX1xcXFxcXC9cXC4gXSg/OjA/XFxkfDFbMDEyXSlbLV9cXFxcXFwvXFwuIF1cXGRcXGQoPyFcXGQpLyk7XHJcbiAgaWYgKGZvdW5kRGF0ZSkgeyByZXR1cm4gZm91bmREYXRlWzBdOyB9XHJcbiAgLy8gTWF0Y2ggWVktTU0tREQuLi5cclxuICBmb3VuZERhdGUgPSB0ZXh0Lm1hdGNoKC9eXFxkXFxkWy1fXFxcXFxcL1xcLiBdKD86WzAxMl0/XFxkfDNbMDFdKVstX1xcXFxcXC9cXC4gXSg/OjA/XFxkfDFbMDEyXSkoPyFcXGQpLyk7XHJcbiAgaWYgKGZvdW5kRGF0ZSkgeyByZXR1cm4gZm91bmREYXRlWzBdOyB9XHJcbiAgLy8gTWF0Y2ggWVlZWU1NREQuLi5cclxuICBmb3VuZERhdGUgPSB0ZXh0Lm1hdGNoKC9eKD86MTl8MjApXFxkXFxkKD86MFxcZHwxWzAxMl0pKD86WzAxMl1cXGR8M1swMV0pLyk7XHJcbiAgaWYgKGZvdW5kRGF0ZSkgeyByZXR1cm4gZm91bmREYXRlWzBdOyB9XHJcbn1cclxuIl19