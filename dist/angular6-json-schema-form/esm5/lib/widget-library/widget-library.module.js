import * as tslib_1 from "tslib";
import { BASIC_WIDGETS } from './index';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JsonSchemaFormService } from '../json-schema-form.service';
import { NgModule } from '@angular/core';
import { OrderableDirective } from './orderable.directive';
var WidgetLibraryModule = /** @class */ (function () {
    function WidgetLibraryModule() {
    }
    WidgetLibraryModule_1 = WidgetLibraryModule;
    WidgetLibraryModule.forRoot = function () {
        return {
            ngModule: WidgetLibraryModule_1,
            providers: [JsonSchemaFormService]
        };
    };
    var WidgetLibraryModule_1;
    WidgetLibraryModule = WidgetLibraryModule_1 = tslib_1.__decorate([
        NgModule({
            imports: [CommonModule, FormsModule, ReactiveFormsModule],
            declarations: tslib_1.__spread(BASIC_WIDGETS, [OrderableDirective]),
            exports: tslib_1.__spread(BASIC_WIDGETS, [OrderableDirective]),
            entryComponents: tslib_1.__spread(BASIC_WIDGETS),
            providers: [JsonSchemaFormService]
        })
    ], WidgetLibraryModule);
    return WidgetLibraryModule;
}());
export { WidgetLibraryModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2lkZ2V0LWxpYnJhcnkubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYW5ndWxhcjYtanNvbi1zY2hlbWEtZm9ybS8iLCJzb3VyY2VzIjpbImxpYi93aWRnZXQtbGlicmFyeS93aWRnZXQtbGlicmFyeS5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFDeEMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxXQUFXLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNsRSxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUNwRSxPQUFPLEVBQXVCLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM5RCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQVMzRDtJQUFBO0lBT0EsQ0FBQzs0QkFQWSxtQkFBbUI7SUFDdkIsMkJBQU8sR0FBZDtRQUNFLE9BQU87WUFDTCxRQUFRLEVBQUUscUJBQW1CO1lBQzdCLFNBQVMsRUFBRSxDQUFFLHFCQUFxQixDQUFFO1NBQ3JDLENBQUM7SUFDSixDQUFDOztJQU5VLG1CQUFtQjtRQVAvQixRQUFRLENBQUM7WUFDUixPQUFPLEVBQVUsQ0FBRSxZQUFZLEVBQUUsV0FBVyxFQUFFLG1CQUFtQixDQUFFO1lBQ25FLFlBQVksbUJBQVUsYUFBYSxHQUFFLGtCQUFrQixFQUFFO1lBQ3pELE9BQU8sbUJBQWUsYUFBYSxHQUFFLGtCQUFrQixFQUFFO1lBQ3pELGVBQWUsbUJBQU8sYUFBYSxDQUFFO1lBQ3JDLFNBQVMsRUFBUSxDQUFFLHFCQUFxQixDQUFFO1NBQzNDLENBQUM7T0FDVyxtQkFBbUIsQ0FPL0I7SUFBRCwwQkFBQztDQUFBLEFBUEQsSUFPQztTQVBZLG1CQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEJBU0lDX1dJREdFVFMgfSBmcm9tICcuL2luZGV4JztcclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgRm9ybXNNb2R1bGUsIFJlYWN0aXZlRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IEpzb25TY2hlbWFGb3JtU2VydmljZSB9IGZyb20gJy4uL2pzb24tc2NoZW1hLWZvcm0uc2VydmljZSc7XHJcbmltcG9ydCB7IE1vZHVsZVdpdGhQcm92aWRlcnMsIE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE9yZGVyYWJsZURpcmVjdGl2ZSB9IGZyb20gJy4vb3JkZXJhYmxlLmRpcmVjdGl2ZSc7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGltcG9ydHM6ICAgICAgICAgWyBDb21tb25Nb2R1bGUsIEZvcm1zTW9kdWxlLCBSZWFjdGl2ZUZvcm1zTW9kdWxlIF0sXHJcbiAgZGVjbGFyYXRpb25zOiAgICBbIC4uLkJBU0lDX1dJREdFVFMsIE9yZGVyYWJsZURpcmVjdGl2ZSBdLFxyXG4gIGV4cG9ydHM6ICAgICAgICAgWyAuLi5CQVNJQ19XSURHRVRTLCBPcmRlcmFibGVEaXJlY3RpdmUgXSxcclxuICBlbnRyeUNvbXBvbmVudHM6IFsgLi4uQkFTSUNfV0lER0VUUyBdLFxyXG4gIHByb3ZpZGVyczogICAgICAgWyBKc29uU2NoZW1hRm9ybVNlcnZpY2UgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgV2lkZ2V0TGlicmFyeU1vZHVsZSB7XHJcbiAgc3RhdGljIGZvclJvb3QoKTogTW9kdWxlV2l0aFByb3ZpZGVycyB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICBuZ01vZHVsZTogV2lkZ2V0TGlicmFyeU1vZHVsZSxcclxuICAgICAgcHJvdmlkZXJzOiBbIEpzb25TY2hlbWFGb3JtU2VydmljZSBdXHJcbiAgICB9O1xyXG4gIH1cclxufVxyXG4iXX0=