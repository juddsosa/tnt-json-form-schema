export var zhValidationMessages = {
    required: '必填字段.',
    minLength: '字符长度必须大于或者等于 {{minimumLength}} (当前长度: {{currentLength}})',
    maxLength: '字符长度必须小于或者等于 {{maximumLength}} (当前长度: {{currentLength}})',
    pattern: '必须匹配正则表达式: {{requiredPattern}}',
    format: function (error) {
        switch (error.requiredFormat) {
            case 'date':
                return '必须为日期格式, 比如 "2000-12-31"';
            case 'time':
                return '必须为时间格式, 比如 "16:20" 或者 "03:14:15.9265"';
            case 'date-time':
                return '必须为日期时间格式, 比如 "2000-03-14T01:59" 或者 "2000-03-14T01:59:26.535Z"';
            case 'email':
                return '必须为邮箱地址, 比如 "name@example.com"';
            case 'hostname':
                return '必须为主机名, 比如 "example.com"';
            case 'ipv4':
                return '必须为 IPv4 地址, 比如 "127.0.0.1"';
            case 'ipv6':
                return '必须为 IPv6 地址, 比如 "1234:5678:9ABC:DEF0:1234:5678:9ABC:DEF0"';
            // TODO: add examples for 'uri', 'uri-reference', and 'uri-template'
            // case 'uri': case 'uri-reference': case 'uri-template':
            case 'url':
                return '必须为 url, 比如 "http://www.example.com/page.html"';
            case 'uuid':
                return '必须为 uuid, 比如 "12345678-9ABC-DEF0-1234-56789ABCDEF0"';
            case 'color':
                return '必须为颜色值, 比如 "#FFFFFF" 或者 "rgb(255, 255, 255)"';
            case 'json-pointer':
                return '必须为 JSON Pointer, 比如 "/pointer/to/something"';
            case 'relative-json-pointer':
                return '必须为相对的 JSON Pointer, 比如 "2/pointer/to/something"';
            case 'regex':
                return '必须为正则表达式, 比如 "(1-)?\\d{3}-\\d{3}-\\d{4}"';
            default:
                return '必须为格式正确的 ' + error.requiredFormat;
        }
    },
    minimum: '必须大于或者等于最小值: {{minimumValue}}',
    exclusiveMinimum: '必须大于最小值: {{exclusiveMinimumValue}}',
    maximum: '必须小于或者等于最大值: {{maximumValue}}',
    exclusiveMaximum: '必须小于最大值: {{exclusiveMaximumValue}}',
    multipleOf: function (error) {
        if ((1 / error.multipleOfValue) % 10 === 0) {
            var decimals = Math.log10(1 / error.multipleOfValue);
            return "\u5FC5\u987B\u6709 " + decimals + " \u4F4D\u6216\u66F4\u5C11\u7684\u5C0F\u6570\u4F4D";
        }
        else {
            return "\u5FC5\u987B\u4E3A " + error.multipleOfValue + " \u7684\u500D\u6570";
        }
    },
    minProperties: '项目数必须大于或者等于 {{minimumProperties}} (当前项目数: {{currentProperties}})',
    maxProperties: '项目数必须小于或者等于 {{maximumProperties}} (当前项目数: {{currentProperties}})',
    minItems: '项目数必须大于或者等于 {{minimumItems}} (当前项目数: {{currentItems}})',
    maxItems: '项目数必须小于或者等于 {{maximumItems}} (当前项目数: {{currentItems}})',
    uniqueItems: '所有项目必须是唯一的',
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiemgtdmFsaWRhdGlvbi1tZXNzYWdlcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FuZ3VsYXI2LWpzb24tc2NoZW1hLWZvcm0vIiwic291cmNlcyI6WyJsaWIvbG9jYWxlL3poLXZhbGlkYXRpb24tbWVzc2FnZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsTUFBTSxDQUFDLElBQU0sb0JBQW9CLEdBQVE7SUFDdkMsUUFBUSxFQUFFLE9BQU87SUFDakIsU0FBUyxFQUFFLDBEQUEwRDtJQUNyRSxTQUFTLEVBQUUsMERBQTBEO0lBQ3JFLE9BQU8sRUFBRSxnQ0FBZ0M7SUFDekMsTUFBTSxFQUFFLFVBQVUsS0FBSztRQUNyQixRQUFRLEtBQUssQ0FBQyxjQUFjLEVBQUU7WUFDNUIsS0FBSyxNQUFNO2dCQUNULE9BQU8sMEJBQTBCLENBQUM7WUFDcEMsS0FBSyxNQUFNO2dCQUNULE9BQU8sd0NBQXdDLENBQUM7WUFDbEQsS0FBSyxXQUFXO2dCQUNkLE9BQU8sZ0VBQWdFLENBQUM7WUFDMUUsS0FBSyxPQUFPO2dCQUNWLE9BQU8sZ0NBQWdDLENBQUM7WUFDMUMsS0FBSyxVQUFVO2dCQUNiLE9BQU8sMEJBQTBCLENBQUM7WUFDcEMsS0FBSyxNQUFNO2dCQUNULE9BQU8sNkJBQTZCLENBQUM7WUFDdkMsS0FBSyxNQUFNO2dCQUNULE9BQU8sMkRBQTJELENBQUM7WUFDckUsb0VBQW9FO1lBQ3BFLHlEQUF5RDtZQUN6RCxLQUFLLEtBQUs7Z0JBQ1IsT0FBTyxnREFBZ0QsQ0FBQztZQUMxRCxLQUFLLE1BQU07Z0JBQ1QsT0FBTyxxREFBcUQsQ0FBQztZQUMvRCxLQUFLLE9BQU87Z0JBQ1YsT0FBTyw4Q0FBOEMsQ0FBQztZQUN4RCxLQUFLLGNBQWM7Z0JBQ2pCLE9BQU8sOENBQThDLENBQUM7WUFDeEQsS0FBSyx1QkFBdUI7Z0JBQzFCLE9BQU8sa0RBQWtELENBQUM7WUFDNUQsS0FBSyxPQUFPO2dCQUNWLE9BQU8sMENBQTBDLENBQUM7WUFDcEQ7Z0JBQ0UsT0FBTyxXQUFXLEdBQUcsS0FBSyxDQUFDLGNBQWMsQ0FBQztTQUM3QztJQUNILENBQUM7SUFDRCxPQUFPLEVBQUUsK0JBQStCO0lBQ3hDLGdCQUFnQixFQUFFLG9DQUFvQztJQUN0RCxPQUFPLEVBQUUsK0JBQStCO0lBQ3hDLGdCQUFnQixFQUFFLG9DQUFvQztJQUN0RCxVQUFVLEVBQUUsVUFBVSxLQUFLO1FBQ3pCLElBQUksQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDLGVBQWUsQ0FBQyxHQUFHLEVBQUUsS0FBSyxDQUFDLEVBQUU7WUFDMUMsSUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQ3ZELE9BQU8sd0JBQU8sUUFBUSxzREFBVyxDQUFDO1NBQ25DO2FBQU07WUFDTCxPQUFPLHdCQUFPLEtBQUssQ0FBQyxlQUFlLHdCQUFNLENBQUM7U0FDM0M7SUFDSCxDQUFDO0lBQ0QsYUFBYSxFQUFFLGtFQUFrRTtJQUNqRixhQUFhLEVBQUUsa0VBQWtFO0lBQ2pGLFFBQVEsRUFBRSx3REFBd0Q7SUFDbEUsUUFBUSxFQUFFLHdEQUF3RDtJQUNsRSxXQUFXLEVBQUUsWUFBWTtDQUUxQixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNvbnN0IHpoVmFsaWRhdGlvbk1lc3NhZ2VzOiBhbnkgPSB7IC8vIENoaW5lc2UgZXJyb3IgbWVzc2FnZXNcclxuICByZXF1aXJlZDogJ+W/heWhq+Wtl+autS4nLFxyXG4gIG1pbkxlbmd0aDogJ+Wtl+espumVv+W6puW/hemhu+Wkp+S6juaIluiAheetieS6jiB7e21pbmltdW1MZW5ndGh9fSAo5b2T5YmN6ZW/5bqmOiB7e2N1cnJlbnRMZW5ndGh9fSknLFxyXG4gIG1heExlbmd0aDogJ+Wtl+espumVv+W6puW/hemhu+Wwj+S6juaIluiAheetieS6jiB7e21heGltdW1MZW5ndGh9fSAo5b2T5YmN6ZW/5bqmOiB7e2N1cnJlbnRMZW5ndGh9fSknLFxyXG4gIHBhdHRlcm46ICflv4XpobvljLnphY3mraPliJnooajovr7lvI86IHt7cmVxdWlyZWRQYXR0ZXJufX0nLFxyXG4gIGZvcm1hdDogZnVuY3Rpb24gKGVycm9yKSB7XHJcbiAgICBzd2l0Y2ggKGVycm9yLnJlcXVpcmVkRm9ybWF0KSB7XHJcbiAgICAgIGNhc2UgJ2RhdGUnOlxyXG4gICAgICAgIHJldHVybiAn5b+F6aG75Li65pel5pyf5qC85byPLCDmr5TlpoIgXCIyMDAwLTEyLTMxXCInO1xyXG4gICAgICBjYXNlICd0aW1lJzpcclxuICAgICAgICByZXR1cm4gJ+W/hemhu+S4uuaXtumXtOagvOW8jywg5q+U5aaCIFwiMTY6MjBcIiDmiJbogIUgXCIwMzoxNDoxNS45MjY1XCInO1xyXG4gICAgICBjYXNlICdkYXRlLXRpbWUnOlxyXG4gICAgICAgIHJldHVybiAn5b+F6aG75Li65pel5pyf5pe26Ze05qC85byPLCDmr5TlpoIgXCIyMDAwLTAzLTE0VDAxOjU5XCIg5oiW6ICFIFwiMjAwMC0wMy0xNFQwMTo1OToyNi41MzVaXCInO1xyXG4gICAgICBjYXNlICdlbWFpbCc6XHJcbiAgICAgICAgcmV0dXJuICflv4XpobvkuLrpgq7nrrHlnLDlnYAsIOavlOWmgiBcIm5hbWVAZXhhbXBsZS5jb21cIic7XHJcbiAgICAgIGNhc2UgJ2hvc3RuYW1lJzpcclxuICAgICAgICByZXR1cm4gJ+W/hemhu+S4uuS4u+acuuWQjSwg5q+U5aaCIFwiZXhhbXBsZS5jb21cIic7XHJcbiAgICAgIGNhc2UgJ2lwdjQnOlxyXG4gICAgICAgIHJldHVybiAn5b+F6aG75Li6IElQdjQg5Zyw5Z2ALCDmr5TlpoIgXCIxMjcuMC4wLjFcIic7XHJcbiAgICAgIGNhc2UgJ2lwdjYnOlxyXG4gICAgICAgIHJldHVybiAn5b+F6aG75Li6IElQdjYg5Zyw5Z2ALCDmr5TlpoIgXCIxMjM0OjU2Nzg6OUFCQzpERUYwOjEyMzQ6NTY3ODo5QUJDOkRFRjBcIic7XHJcbiAgICAgIC8vIFRPRE86IGFkZCBleGFtcGxlcyBmb3IgJ3VyaScsICd1cmktcmVmZXJlbmNlJywgYW5kICd1cmktdGVtcGxhdGUnXHJcbiAgICAgIC8vIGNhc2UgJ3VyaSc6IGNhc2UgJ3VyaS1yZWZlcmVuY2UnOiBjYXNlICd1cmktdGVtcGxhdGUnOlxyXG4gICAgICBjYXNlICd1cmwnOlxyXG4gICAgICAgIHJldHVybiAn5b+F6aG75Li6IHVybCwg5q+U5aaCIFwiaHR0cDovL3d3dy5leGFtcGxlLmNvbS9wYWdlLmh0bWxcIic7XHJcbiAgICAgIGNhc2UgJ3V1aWQnOlxyXG4gICAgICAgIHJldHVybiAn5b+F6aG75Li6IHV1aWQsIOavlOWmgiBcIjEyMzQ1Njc4LTlBQkMtREVGMC0xMjM0LTU2Nzg5QUJDREVGMFwiJztcclxuICAgICAgY2FzZSAnY29sb3InOlxyXG4gICAgICAgIHJldHVybiAn5b+F6aG75Li66aKc6Imy5YC8LCDmr5TlpoIgXCIjRkZGRkZGXCIg5oiW6ICFIFwicmdiKDI1NSwgMjU1LCAyNTUpXCInO1xyXG4gICAgICBjYXNlICdqc29uLXBvaW50ZXInOlxyXG4gICAgICAgIHJldHVybiAn5b+F6aG75Li6IEpTT04gUG9pbnRlciwg5q+U5aaCIFwiL3BvaW50ZXIvdG8vc29tZXRoaW5nXCInO1xyXG4gICAgICBjYXNlICdyZWxhdGl2ZS1qc29uLXBvaW50ZXInOlxyXG4gICAgICAgIHJldHVybiAn5b+F6aG75Li655u45a+555qEIEpTT04gUG9pbnRlciwg5q+U5aaCIFwiMi9wb2ludGVyL3RvL3NvbWV0aGluZ1wiJztcclxuICAgICAgY2FzZSAncmVnZXgnOlxyXG4gICAgICAgIHJldHVybiAn5b+F6aG75Li65q2j5YiZ6KGo6L6+5byPLCDmr5TlpoIgXCIoMS0pP1xcXFxkezN9LVxcXFxkezN9LVxcXFxkezR9XCInO1xyXG4gICAgICBkZWZhdWx0OlxyXG4gICAgICAgIHJldHVybiAn5b+F6aG75Li65qC85byP5q2j56Gu55qEICcgKyBlcnJvci5yZXF1aXJlZEZvcm1hdDtcclxuICAgIH1cclxuICB9LFxyXG4gIG1pbmltdW06ICflv4XpobvlpKfkuo7miJbogIXnrYnkuo7mnIDlsI/lgLw6IHt7bWluaW11bVZhbHVlfX0nLFxyXG4gIGV4Y2x1c2l2ZU1pbmltdW06ICflv4XpobvlpKfkuo7mnIDlsI/lgLw6IHt7ZXhjbHVzaXZlTWluaW11bVZhbHVlfX0nLFxyXG4gIG1heGltdW06ICflv4XpobvlsI/kuo7miJbogIXnrYnkuo7mnIDlpKflgLw6IHt7bWF4aW11bVZhbHVlfX0nLFxyXG4gIGV4Y2x1c2l2ZU1heGltdW06ICflv4XpobvlsI/kuo7mnIDlpKflgLw6IHt7ZXhjbHVzaXZlTWF4aW11bVZhbHVlfX0nLFxyXG4gIG11bHRpcGxlT2Y6IGZ1bmN0aW9uIChlcnJvcikge1xyXG4gICAgaWYgKCgxIC8gZXJyb3IubXVsdGlwbGVPZlZhbHVlKSAlIDEwID09PSAwKSB7XHJcbiAgICAgIGNvbnN0IGRlY2ltYWxzID0gTWF0aC5sb2cxMCgxIC8gZXJyb3IubXVsdGlwbGVPZlZhbHVlKTtcclxuICAgICAgcmV0dXJuIGDlv4XpobvmnIkgJHtkZWNpbWFsc30g5L2N5oiW5pu05bCR55qE5bCP5pWw5L2NYDtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiBg5b+F6aG75Li6ICR7ZXJyb3IubXVsdGlwbGVPZlZhbHVlfSDnmoTlgI3mlbBgO1xyXG4gICAgfVxyXG4gIH0sXHJcbiAgbWluUHJvcGVydGllczogJ+mhueebruaVsOW/hemhu+Wkp+S6juaIluiAheetieS6jiB7e21pbmltdW1Qcm9wZXJ0aWVzfX0gKOW9k+WJjemhueebruaVsDoge3tjdXJyZW50UHJvcGVydGllc319KScsXHJcbiAgbWF4UHJvcGVydGllczogJ+mhueebruaVsOW/hemhu+Wwj+S6juaIluiAheetieS6jiB7e21heGltdW1Qcm9wZXJ0aWVzfX0gKOW9k+WJjemhueebruaVsDoge3tjdXJyZW50UHJvcGVydGllc319KScsXHJcbiAgbWluSXRlbXM6ICfpobnnm67mlbDlv4XpobvlpKfkuo7miJbogIXnrYnkuo4ge3ttaW5pbXVtSXRlbXN9fSAo5b2T5YmN6aG555uu5pWwOiB7e2N1cnJlbnRJdGVtc319KScsXHJcbiAgbWF4SXRlbXM6ICfpobnnm67mlbDlv4XpobvlsI/kuo7miJbogIXnrYnkuo4ge3ttYXhpbXVtSXRlbXN9fSAo5b2T5YmN6aG555uu5pWwOiB7e2N1cnJlbnRJdGVtc319KScsXHJcbiAgdW5pcXVlSXRlbXM6ICfmiYDmnInpobnnm67lv4XpobvmmK/llK/kuIDnmoQnLFxyXG4gIC8vIE5vdGU6IE5vIGRlZmF1bHQgZXJyb3IgbWVzc2FnZXMgZm9yICd0eXBlJywgJ2NvbnN0JywgJ2VudW0nLCBvciAnZGVwZW5kZW5jaWVzJ1xyXG59O1xyXG4iXX0=