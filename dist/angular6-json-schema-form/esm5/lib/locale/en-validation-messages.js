export var enValidationMessages = {
    required: 'This field is required.',
    minLength: 'Must be {{minimumLength}} characters or longer (current length: {{currentLength}})',
    maxLength: 'Must be {{maximumLength}} characters or shorter (current length: {{currentLength}})',
    pattern: 'Must match pattern: {{requiredPattern}}',
    format: function (error) {
        switch (error.requiredFormat) {
            case 'date':
                return 'Must be a date, like "2000-12-31"';
            case 'time':
                return 'Must be a time, like "16:20" or "03:14:15.9265"';
            case 'date-time':
                return 'Must be a date-time, like "2000-03-14T01:59" or "2000-03-14T01:59:26.535Z"';
            case 'email':
                return 'Must be an email address, like "name@example.com"';
            case 'hostname':
                return 'Must be a hostname, like "example.com"';
            case 'ipv4':
                return 'Must be an IPv4 address, like "127.0.0.1"';
            case 'ipv6':
                return 'Must be an IPv6 address, like "1234:5678:9ABC:DEF0:1234:5678:9ABC:DEF0"';
            // TODO: add examples for 'uri', 'uri-reference', and 'uri-template'
            // case 'uri': case 'uri-reference': case 'uri-template':
            case 'url':
                return 'Must be a url, like "http://www.example.com/page.html"';
            case 'uuid':
                return 'Must be a uuid, like "12345678-9ABC-DEF0-1234-56789ABCDEF0"';
            case 'color':
                return 'Must be a color, like "#FFFFFF" or "rgb(255, 255, 255)"';
            case 'json-pointer':
                return 'Must be a JSON Pointer, like "/pointer/to/something"';
            case 'relative-json-pointer':
                return 'Must be a relative JSON Pointer, like "2/pointer/to/something"';
            case 'regex':
                return 'Must be a regular expression, like "(1-)?\\d{3}-\\d{3}-\\d{4}"';
            default:
                return 'Must be a correctly formatted ' + error.requiredFormat;
        }
    },
    minimum: 'Must be {{minimumValue}} or more',
    exclusiveMinimum: 'Must be more than {{exclusiveMinimumValue}}',
    maximum: 'Must be {{maximumValue}} or less',
    exclusiveMaximum: 'Must be less than {{exclusiveMaximumValue}}',
    multipleOf: function (error) {
        if ((1 / error.multipleOfValue) % 10 === 0) {
            var decimals = Math.log10(1 / error.multipleOfValue);
            return "Must have " + decimals + " or fewer decimal places.";
        }
        else {
            return "Must be a multiple of " + error.multipleOfValue + ".";
        }
    },
    minProperties: 'Must have {{minimumProperties}} or more items (current items: {{currentProperties}})',
    maxProperties: 'Must have {{maximumProperties}} or fewer items (current items: {{currentProperties}})',
    minItems: 'Must have {{minimumItems}} or more items (current items: {{currentItems}})',
    maxItems: 'Must have {{maximumItems}} or fewer items (current items: {{currentItems}})',
    uniqueItems: 'All items must be unique',
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW4tdmFsaWRhdGlvbi1tZXNzYWdlcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FuZ3VsYXI2LWpzb24tc2NoZW1hLWZvcm0vIiwic291cmNlcyI6WyJsaWIvbG9jYWxlL2VuLXZhbGlkYXRpb24tbWVzc2FnZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsTUFBTSxDQUFDLElBQU0sb0JBQW9CLEdBQVE7SUFDdkMsUUFBUSxFQUFFLHlCQUF5QjtJQUNuQyxTQUFTLEVBQUUsb0ZBQW9GO0lBQy9GLFNBQVMsRUFBRSxxRkFBcUY7SUFDaEcsT0FBTyxFQUFFLHlDQUF5QztJQUNsRCxNQUFNLEVBQUUsVUFBVSxLQUFLO1FBQ3JCLFFBQVEsS0FBSyxDQUFDLGNBQWMsRUFBRTtZQUM1QixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxtQ0FBbUMsQ0FBQztZQUM3QyxLQUFLLE1BQU07Z0JBQ1QsT0FBTyxpREFBaUQsQ0FBQztZQUMzRCxLQUFLLFdBQVc7Z0JBQ2QsT0FBTyw0RUFBNEUsQ0FBQztZQUN0RixLQUFLLE9BQU87Z0JBQ1YsT0FBTyxtREFBbUQsQ0FBQztZQUM3RCxLQUFLLFVBQVU7Z0JBQ2IsT0FBTyx3Q0FBd0MsQ0FBQztZQUNsRCxLQUFLLE1BQU07Z0JBQ1QsT0FBTywyQ0FBMkMsQ0FBQztZQUNyRCxLQUFLLE1BQU07Z0JBQ1QsT0FBTyx5RUFBeUUsQ0FBQztZQUNuRixvRUFBb0U7WUFDcEUseURBQXlEO1lBQ3pELEtBQUssS0FBSztnQkFDUixPQUFPLHdEQUF3RCxDQUFDO1lBQ2xFLEtBQUssTUFBTTtnQkFDVCxPQUFPLDZEQUE2RCxDQUFDO1lBQ3ZFLEtBQUssT0FBTztnQkFDVixPQUFPLHlEQUF5RCxDQUFDO1lBQ25FLEtBQUssY0FBYztnQkFDakIsT0FBTyxzREFBc0QsQ0FBQztZQUNoRSxLQUFLLHVCQUF1QjtnQkFDMUIsT0FBTyxnRUFBZ0UsQ0FBQztZQUMxRSxLQUFLLE9BQU87Z0JBQ1YsT0FBTyxnRUFBZ0UsQ0FBQztZQUMxRTtnQkFDRSxPQUFPLGdDQUFnQyxHQUFHLEtBQUssQ0FBQyxjQUFjLENBQUM7U0FDbEU7SUFDSCxDQUFDO0lBQ0QsT0FBTyxFQUFFLGtDQUFrQztJQUMzQyxnQkFBZ0IsRUFBRSw2Q0FBNkM7SUFDL0QsT0FBTyxFQUFFLGtDQUFrQztJQUMzQyxnQkFBZ0IsRUFBRSw2Q0FBNkM7SUFDL0QsVUFBVSxFQUFFLFVBQVUsS0FBSztRQUN6QixJQUFJLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQyxlQUFlLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxFQUFFO1lBQzFDLElBQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUN2RCxPQUFPLGVBQWEsUUFBUSw4QkFBMkIsQ0FBQztTQUN6RDthQUFNO1lBQ0wsT0FBTywyQkFBeUIsS0FBSyxDQUFDLGVBQWUsTUFBRyxDQUFDO1NBQzFEO0lBQ0gsQ0FBQztJQUNELGFBQWEsRUFBRSxzRkFBc0Y7SUFDckcsYUFBYSxFQUFFLHVGQUF1RjtJQUN0RyxRQUFRLEVBQUUsNEVBQTRFO0lBQ3RGLFFBQVEsRUFBRSw2RUFBNkU7SUFDdkYsV0FBVyxFQUFFLDBCQUEwQjtDQUV4QyxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNvbnN0IGVuVmFsaWRhdGlvbk1lc3NhZ2VzOiBhbnkgPSB7IC8vIERlZmF1bHQgRW5nbGlzaCBlcnJvciBtZXNzYWdlc1xyXG4gIHJlcXVpcmVkOiAnVGhpcyBmaWVsZCBpcyByZXF1aXJlZC4nLFxyXG4gIG1pbkxlbmd0aDogJ011c3QgYmUge3ttaW5pbXVtTGVuZ3RofX0gY2hhcmFjdGVycyBvciBsb25nZXIgKGN1cnJlbnQgbGVuZ3RoOiB7e2N1cnJlbnRMZW5ndGh9fSknLFxyXG4gIG1heExlbmd0aDogJ011c3QgYmUge3ttYXhpbXVtTGVuZ3RofX0gY2hhcmFjdGVycyBvciBzaG9ydGVyIChjdXJyZW50IGxlbmd0aDoge3tjdXJyZW50TGVuZ3RofX0pJyxcclxuICBwYXR0ZXJuOiAnTXVzdCBtYXRjaCBwYXR0ZXJuOiB7e3JlcXVpcmVkUGF0dGVybn19JyxcclxuICBmb3JtYXQ6IGZ1bmN0aW9uIChlcnJvcikge1xyXG4gICAgc3dpdGNoIChlcnJvci5yZXF1aXJlZEZvcm1hdCkge1xyXG4gICAgICBjYXNlICdkYXRlJzpcclxuICAgICAgICByZXR1cm4gJ011c3QgYmUgYSBkYXRlLCBsaWtlIFwiMjAwMC0xMi0zMVwiJztcclxuICAgICAgY2FzZSAndGltZSc6XHJcbiAgICAgICAgcmV0dXJuICdNdXN0IGJlIGEgdGltZSwgbGlrZSBcIjE2OjIwXCIgb3IgXCIwMzoxNDoxNS45MjY1XCInO1xyXG4gICAgICBjYXNlICdkYXRlLXRpbWUnOlxyXG4gICAgICAgIHJldHVybiAnTXVzdCBiZSBhIGRhdGUtdGltZSwgbGlrZSBcIjIwMDAtMDMtMTRUMDE6NTlcIiBvciBcIjIwMDAtMDMtMTRUMDE6NTk6MjYuNTM1WlwiJztcclxuICAgICAgY2FzZSAnZW1haWwnOlxyXG4gICAgICAgIHJldHVybiAnTXVzdCBiZSBhbiBlbWFpbCBhZGRyZXNzLCBsaWtlIFwibmFtZUBleGFtcGxlLmNvbVwiJztcclxuICAgICAgY2FzZSAnaG9zdG5hbWUnOlxyXG4gICAgICAgIHJldHVybiAnTXVzdCBiZSBhIGhvc3RuYW1lLCBsaWtlIFwiZXhhbXBsZS5jb21cIic7XHJcbiAgICAgIGNhc2UgJ2lwdjQnOlxyXG4gICAgICAgIHJldHVybiAnTXVzdCBiZSBhbiBJUHY0IGFkZHJlc3MsIGxpa2UgXCIxMjcuMC4wLjFcIic7XHJcbiAgICAgIGNhc2UgJ2lwdjYnOlxyXG4gICAgICAgIHJldHVybiAnTXVzdCBiZSBhbiBJUHY2IGFkZHJlc3MsIGxpa2UgXCIxMjM0OjU2Nzg6OUFCQzpERUYwOjEyMzQ6NTY3ODo5QUJDOkRFRjBcIic7XHJcbiAgICAgIC8vIFRPRE86IGFkZCBleGFtcGxlcyBmb3IgJ3VyaScsICd1cmktcmVmZXJlbmNlJywgYW5kICd1cmktdGVtcGxhdGUnXHJcbiAgICAgIC8vIGNhc2UgJ3VyaSc6IGNhc2UgJ3VyaS1yZWZlcmVuY2UnOiBjYXNlICd1cmktdGVtcGxhdGUnOlxyXG4gICAgICBjYXNlICd1cmwnOlxyXG4gICAgICAgIHJldHVybiAnTXVzdCBiZSBhIHVybCwgbGlrZSBcImh0dHA6Ly93d3cuZXhhbXBsZS5jb20vcGFnZS5odG1sXCInO1xyXG4gICAgICBjYXNlICd1dWlkJzpcclxuICAgICAgICByZXR1cm4gJ011c3QgYmUgYSB1dWlkLCBsaWtlIFwiMTIzNDU2NzgtOUFCQy1ERUYwLTEyMzQtNTY3ODlBQkNERUYwXCInO1xyXG4gICAgICBjYXNlICdjb2xvcic6XHJcbiAgICAgICAgcmV0dXJuICdNdXN0IGJlIGEgY29sb3IsIGxpa2UgXCIjRkZGRkZGXCIgb3IgXCJyZ2IoMjU1LCAyNTUsIDI1NSlcIic7XHJcbiAgICAgIGNhc2UgJ2pzb24tcG9pbnRlcic6XHJcbiAgICAgICAgcmV0dXJuICdNdXN0IGJlIGEgSlNPTiBQb2ludGVyLCBsaWtlIFwiL3BvaW50ZXIvdG8vc29tZXRoaW5nXCInO1xyXG4gICAgICBjYXNlICdyZWxhdGl2ZS1qc29uLXBvaW50ZXInOlxyXG4gICAgICAgIHJldHVybiAnTXVzdCBiZSBhIHJlbGF0aXZlIEpTT04gUG9pbnRlciwgbGlrZSBcIjIvcG9pbnRlci90by9zb21ldGhpbmdcIic7XHJcbiAgICAgIGNhc2UgJ3JlZ2V4JzpcclxuICAgICAgICByZXR1cm4gJ011c3QgYmUgYSByZWd1bGFyIGV4cHJlc3Npb24sIGxpa2UgXCIoMS0pP1xcXFxkezN9LVxcXFxkezN9LVxcXFxkezR9XCInO1xyXG4gICAgICBkZWZhdWx0OlxyXG4gICAgICAgIHJldHVybiAnTXVzdCBiZSBhIGNvcnJlY3RseSBmb3JtYXR0ZWQgJyArIGVycm9yLnJlcXVpcmVkRm9ybWF0O1xyXG4gICAgfVxyXG4gIH0sXHJcbiAgbWluaW11bTogJ011c3QgYmUge3ttaW5pbXVtVmFsdWV9fSBvciBtb3JlJyxcclxuICBleGNsdXNpdmVNaW5pbXVtOiAnTXVzdCBiZSBtb3JlIHRoYW4ge3tleGNsdXNpdmVNaW5pbXVtVmFsdWV9fScsXHJcbiAgbWF4aW11bTogJ011c3QgYmUge3ttYXhpbXVtVmFsdWV9fSBvciBsZXNzJyxcclxuICBleGNsdXNpdmVNYXhpbXVtOiAnTXVzdCBiZSBsZXNzIHRoYW4ge3tleGNsdXNpdmVNYXhpbXVtVmFsdWV9fScsXHJcbiAgbXVsdGlwbGVPZjogZnVuY3Rpb24gKGVycm9yKSB7XHJcbiAgICBpZiAoKDEgLyBlcnJvci5tdWx0aXBsZU9mVmFsdWUpICUgMTAgPT09IDApIHtcclxuICAgICAgY29uc3QgZGVjaW1hbHMgPSBNYXRoLmxvZzEwKDEgLyBlcnJvci5tdWx0aXBsZU9mVmFsdWUpO1xyXG4gICAgICByZXR1cm4gYE11c3QgaGF2ZSAke2RlY2ltYWxzfSBvciBmZXdlciBkZWNpbWFsIHBsYWNlcy5gO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIGBNdXN0IGJlIGEgbXVsdGlwbGUgb2YgJHtlcnJvci5tdWx0aXBsZU9mVmFsdWV9LmA7XHJcbiAgICB9XHJcbiAgfSxcclxuICBtaW5Qcm9wZXJ0aWVzOiAnTXVzdCBoYXZlIHt7bWluaW11bVByb3BlcnRpZXN9fSBvciBtb3JlIGl0ZW1zIChjdXJyZW50IGl0ZW1zOiB7e2N1cnJlbnRQcm9wZXJ0aWVzfX0pJyxcclxuICBtYXhQcm9wZXJ0aWVzOiAnTXVzdCBoYXZlIHt7bWF4aW11bVByb3BlcnRpZXN9fSBvciBmZXdlciBpdGVtcyAoY3VycmVudCBpdGVtczoge3tjdXJyZW50UHJvcGVydGllc319KScsXHJcbiAgbWluSXRlbXM6ICdNdXN0IGhhdmUge3ttaW5pbXVtSXRlbXN9fSBvciBtb3JlIGl0ZW1zIChjdXJyZW50IGl0ZW1zOiB7e2N1cnJlbnRJdGVtc319KScsXHJcbiAgbWF4SXRlbXM6ICdNdXN0IGhhdmUge3ttYXhpbXVtSXRlbXN9fSBvciBmZXdlciBpdGVtcyAoY3VycmVudCBpdGVtczoge3tjdXJyZW50SXRlbXN9fSknLFxyXG4gIHVuaXF1ZUl0ZW1zOiAnQWxsIGl0ZW1zIG11c3QgYmUgdW5pcXVlJyxcclxuICAvLyBOb3RlOiBObyBkZWZhdWx0IGVycm9yIG1lc3NhZ2VzIGZvciAndHlwZScsICdjb25zdCcsICdlbnVtJywgb3IgJ2RlcGVuZGVuY2llcydcclxufTtcclxuIl19