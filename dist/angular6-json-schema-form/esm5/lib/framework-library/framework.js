import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
var Framework = /** @class */ (function () {
    function Framework() {
        this.widgets = {};
        this.stylesheets = [];
        this.scripts = [];
    }
    Framework = tslib_1.__decorate([
        Injectable()
    ], Framework);
    return Framework;
}());
export { Framework };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZnJhbWV3b3JrLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYW5ndWxhcjYtanNvbi1zY2hlbWEtZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9mcmFtZXdvcmstbGlicmFyeS9mcmFtZXdvcmsudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFHM0M7SUFEQTtRQUlFLFlBQU8sR0FBNEIsRUFBRSxDQUFDO1FBQ3RDLGdCQUFXLEdBQWMsRUFBRSxDQUFDO1FBQzVCLFlBQU8sR0FBYyxFQUFFLENBQUM7SUFDMUIsQ0FBQztJQU5ZLFNBQVM7UUFEckIsVUFBVSxFQUFFO09BQ0EsU0FBUyxDQU1yQjtJQUFELGdCQUFDO0NBQUEsQUFORCxJQU1DO1NBTlksU0FBUyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIEZyYW1ld29yayB7XHJcbiAgbmFtZTogc3RyaW5nO1xyXG4gIGZyYW1ld29yazogYW55O1xyXG4gIHdpZGdldHM/OiB7IFtrZXk6IHN0cmluZ106IGFueSB9ID0ge307XHJcbiAgc3R5bGVzaGVldHM/OiBzdHJpbmdbXSA9IFtdO1xyXG4gIHNjcmlwdHM/OiBzdHJpbmdbXSA9IFtdO1xyXG59XHJcbiJdfQ==