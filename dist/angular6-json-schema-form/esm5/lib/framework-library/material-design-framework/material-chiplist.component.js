import * as tslib_1 from "tslib";
import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatAutocomplete } from '@angular/material';
import { map, startWith } from 'rxjs/operators';
import { JsonSchemaFormService } from '../../json-schema-form.service';
import _ from 'lodash';
var MaterialChiplistComponent = /** @class */ (function () {
    function MaterialChiplistComponent(jsf) {
        this.jsf = jsf;
        this.dummyFormControl = new FormControl();
        this.controlDisabled = true;
        this.boundControl = false;
        this.isArray = true;
        this.selectable = true;
        this.addOnBlur = true;
        this.enableInput = true;
        this.enableDelete = true;
        this.enableClick = true;
        this.separatorKeysCodes = [ENTER, COMMA];
        this.selectedOpt = [];
        this.currentOpt = [];
    }
    MaterialChiplistComponent.prototype.ngOnInit = function () {
        var _this = this;
        var e_1, _a;
        this.options = this.layoutNode.options || {};
        this.options.enum = this.options.enum || [];
        this.selectedOpt = this.options.selectedOpt || [];
        this.currentOpt = [];
        this.enableInput = this.options.enableInput;
        this.enableDelete = this.options.enableDelete;
        this.enableClick = this.options.enableClick;
        this.jsf.initializeControl(this);
        var _fcValue = JSON.parse(this.formControl.value);
        if (_fcValue && _fcValue.length) {
            try {
                for (var _fcValue_1 = tslib_1.__values(_fcValue), _fcValue_1_1 = _fcValue_1.next(); !_fcValue_1_1.done; _fcValue_1_1 = _fcValue_1.next()) {
                    var value = _fcValue_1_1.value;
                    value.complete != null
                        ? this.selectedOpt.push(value)
                        : this.currentOpt.push(value);
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_fcValue_1_1 && !_fcValue_1_1.done && (_a = _fcValue_1.return)) _a.call(_fcValue_1);
                }
                finally { if (e_1) throw e_1.error; }
            }
        }
        this.selectedOpt = _.uniqBy(this.selectedOpt, 'name');
        this.updateValue();
        this.filteredEnum = this.dummyFormControl.valueChanges.pipe(startWith(null), map(function (fe) {
            return fe ? _this.findOpts(fe, true) : _this.options.enum.slice();
        }));
    };
    MaterialChiplistComponent.prototype.updateValue = function () {
        this.jsf.updateValue(this, JSON.stringify(tslib_1.__spread(this.currentOpt, this.selectedOpt)));
        if (this.options.onChanges &&
            typeof this.options.onChanges === 'function') {
            this.options.onChanges(this.currentOpt);
        }
    };
    MaterialChiplistComponent.prototype.add = function (event) {
        if (!this.matAutocomplete.isOpen) {
            var input = event.input;
            var value = event.value;
            if ((value || '').trim()) {
                this.addSelected(value);
            }
            // Reset the input value
            if (input) {
                input.value = '';
            }
            this.dummyFormControl.setValue(null);
            this.updateValue();
        }
    };
    MaterialChiplistComponent.prototype.removeSelected = function (opt) {
        var index = this.selectedOpt.indexOf(opt);
        if (index >= 0) {
            this.selectedOpt.splice(index, 1);
        }
        this.updateValue();
    };
    MaterialChiplistComponent.prototype.removeCurrent = function (opt) {
        var index = this.currentOpt.indexOf(opt);
        if (index >= 0) {
            this.currentOpt.splice(index, 1);
        }
        this.updateValue();
    };
    MaterialChiplistComponent.prototype.selected = function (event) {
        if (this.currentOpt.length < this.options.chipsLimit ||
            this.options.chipsLimit === 0) {
            this.addSelected(event.option.viewValue);
            this.chipInput.nativeElement.value = '';
            this.dummyFormControl.setValue(null);
            this.updateValue();
        }
    };
    MaterialChiplistComponent.prototype.onChipsClick = function (value) {
        if (this.enableClick &&
            this.options.onChipsClick &&
            typeof this.options.onChipsClick === 'function') {
            this.options.onChipsClick(value);
        }
    };
    // private addSelected(value) {
    //   const _opt = this.findOpts(value);
    //   if (_opt) {
    //     const _selectedOpt = this.selectedOpt.filter(
    //       (so: any) => so.name.toLowerCase() === _opt.name.toLowerCase()
    //     );
    //     if (!_selectedOpt.length) {
    //       this.selectedOpt.push(_opt);
    //     }
    //   }
    // }
    MaterialChiplistComponent.prototype.addSelected = function (value) {
        var _opt = this.findOpts(value);
        if (_opt) {
            var _currentOpt = this.currentOpt.filter(function (so) { return so.name.toLowerCase() === _opt.name.toLowerCase(); });
            var _selectedOpt = this.selectedOpt.filter(function (so) { return so.name.toLowerCase() === _opt.name.toLowerCase(); });
            if (!_currentOpt.length && !_selectedOpt.length) {
                this.currentOpt.push(_opt);
            }
        }
    };
    MaterialChiplistComponent.prototype.findOpts = function (value, like) {
        if (like === void 0) { like = false; }
        if (value) {
            var _value_1 = (value.name || value).trim().split(' | ');
            var _foundOpt = like
                ? this.options.enum.filter(function (opt) { return opt.name.toLowerCase().indexOf(_value_1[0].toLowerCase()) === 0; })
                : this.options.enum.filter(function (opt) { return opt.name.toLowerCase() === _value_1[0].toLowerCase(); });
            return _foundOpt.length ? (like ? _foundOpt : _foundOpt[0]) : null;
        }
        return null;
    };
    MaterialChiplistComponent.prototype._filter = function (value) {
        var _opt = this.findOpts(value, true);
        return _opt
            ? this.options.enum.filter(function (opt) { return opt.name.toLowerCase().indexOf(_opt.name.toLowerCase()) === 0; })
            : [];
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], MaterialChiplistComponent.prototype, "layoutNode", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Array)
    ], MaterialChiplistComponent.prototype, "layoutIndex", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Array)
    ], MaterialChiplistComponent.prototype, "dataIndex", void 0);
    tslib_1.__decorate([
        ViewChild('chipInput'),
        tslib_1.__metadata("design:type", ElementRef)
    ], MaterialChiplistComponent.prototype, "chipInput", void 0);
    tslib_1.__decorate([
        ViewChild('auto'),
        tslib_1.__metadata("design:type", MatAutocomplete)
    ], MaterialChiplistComponent.prototype, "matAutocomplete", void 0);
    MaterialChiplistComponent = tslib_1.__decorate([
        Component({
            selector: 'material-chiplist-widget',
            template: "\n    <mat-form-field\n      [class]=\"options?.htmlClass || ''\"\n      [floatLabel]=\"\n        options?.floatLabel || (options?.notitle ? 'never' : 'auto')\n      \"\n      [style.width]=\"'100%'\"\n    >\n      <mat-chip-list #chipList [style.width]=\"'100%'\">\n        <div id=\"currentChips\">\n          <mat-chip\n            *ngFor=\"let opt of currentOpt\"\n            [selectable]=\"selectable\"\n            [removable]=\"enableDelete\"\n            (removed)=\"removeCurrent(opt)\"\n            (click)=\"onChipsClick(opt)\"\n          >\n            <div\n              matChipAvatar\n              class=\"complete-marker\"\n              *ngIf=\"opt.complete != null && opt.complete != ''\"\n              [ngClass]=\"{ complete: opt.complete }\"\n            ></div>\n\n            <span class=\"chip-name\">{{ opt.name }}</span>\n            <mat-icon matChipRemove *ngIf=\"enableDelete\">cancel</mat-icon>\n          </mat-chip>\n        </div>\n        <input\n          matInput\n          #chipInput\n          [placeholder]=\"\n            options?.notitle ? options?.placeholder : options?.title\n          \"\n          [matAutocomplete]=\"auto\"\n          [matChipInputFor]=\"chipList\"\n          [matChipInputSeparatorKeyCodes]=\"separatorKeysCodes\"\n          [matChipInputAddOnBlur]=\"addOnBlur\"\n          (matChipInputTokenEnd)=\"add($event)\"\n          [style.width]=\"'100%'\"\n          [formControl]=\"dummyFormControl\"\n          *ngIf=\"enableInput\"\n        />\n      </mat-chip-list>\n      <div>\n        <mat-chip\n          *ngFor=\"let opt of selectedOpt\"\n          [selectable]=\"selectable\"\n          [removable]=\"enableDelete\"\n          (removed)=\"removeSelected(opt)\"\n          (click)=\"onChipsClick(opt)\"\n        >\n          <div\n            matChipAvatar\n            class=\"complete-marker\"\n            *ngIf=\"opt.complete != null && opt.complete != ''\"\n            [ngClass]=\"{ complete: opt.complete }\"\n          ></div>\n\n          <span class=\"chip-name\">{{ opt.name }}</span>\n\n          <mat-icon matChipRemove *ngIf=\"enableDelete\">cancel</mat-icon>\n        </mat-chip>\n      </div>\n      <mat-autocomplete\n        #auto=\"matAutocomplete\"\n        class=\"chiplist-autocomp\"\n        [style.width]=\"'100%'\"\n        (optionSelected)=\"selected($event)\"\n      >\n        <mat-option *ngFor=\"let fe of filteredEnum | async\" [value]=\"fe\">\n          {{ fe.name }}\n        </mat-option>\n      </mat-autocomplete>\n    </mat-form-field>\n  ",
            styles: ["\n      .mat-standard-chip {\n        margin: 4px 8px 4px 0;\n      }\n\n      .chip-name {\n        margin-top: 2px;\n      }\n\n      .mat-chip-remove {\n        font-size: 32px;\n        opacity: 1 !important;\n      }\n\n      .complete-marker {\n        background-color: red;\n      }\n\n      .complete {\n        background-color: green;\n      }\n    "]
        }),
        tslib_1.__metadata("design:paramtypes", [JsonSchemaFormService])
    ], MaterialChiplistComponent);
    return MaterialChiplistComponent;
}());
export { MaterialChiplistComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWF0ZXJpYWwtY2hpcGxpc3QuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYW5ndWxhcjYtanNvbi1zY2hlbWEtZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9mcmFtZXdvcmstbGlicmFyeS9tYXRlcmlhbC1kZXNpZ24tZnJhbWV3b3JrL21hdGVyaWFsLWNoaXBsaXN0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFVLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNoRixPQUFPLEVBQW1CLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzlELE9BQU8sRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDckQsT0FBTyxFQUdMLGVBQWUsRUFDaEIsTUFBTSxtQkFBbUIsQ0FBQztBQUUzQixPQUFPLEVBQUUsR0FBRyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRWhELE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQ3ZFLE9BQU8sQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQXlHdkI7SUEwQkUsbUNBQW9CLEdBQTBCO1FBQTFCLFFBQUcsR0FBSCxHQUFHLENBQXVCO1FBeEI5QyxxQkFBZ0IsR0FBRyxJQUFJLFdBQVcsRUFBRSxDQUFDO1FBR3JDLG9CQUFlLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLGlCQUFZLEdBQUcsS0FBSyxDQUFDO1FBRXJCLFlBQU8sR0FBRyxJQUFJLENBQUM7UUFLZixlQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLGNBQVMsR0FBRyxJQUFJLENBQUM7UUFDakIsZ0JBQVcsR0FBRyxJQUFJLENBQUM7UUFDbkIsaUJBQVksR0FBRyxJQUFJLENBQUM7UUFDcEIsZ0JBQVcsR0FBRyxJQUFJLENBQUM7UUFDbkIsdUJBQWtCLEdBQWEsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFFOUMsZ0JBQVcsR0FBYSxFQUFFLENBQUM7UUFDM0IsZUFBVSxHQUFhLEVBQUUsQ0FBQztJQUt1QixDQUFDO0lBRWxELDRDQUFRLEdBQVI7UUFBQSxpQkF5QkM7O1FBeEJDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLElBQUksRUFBRSxDQUFDO1FBQzdDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQztRQUM1QyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxJQUFJLEVBQUUsQ0FBQztRQUNsRCxJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDO1FBQzVDLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUM7UUFDOUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQztRQUM1QyxJQUFJLENBQUMsR0FBRyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2pDLElBQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNwRCxJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsTUFBTSxFQUFFOztnQkFDL0IsS0FBb0IsSUFBQSxhQUFBLGlCQUFBLFFBQVEsQ0FBQSxrQ0FBQSx3REFBRTtvQkFBekIsSUFBTSxLQUFLLHFCQUFBO29CQUNkLEtBQUssQ0FBQyxRQUFRLElBQUksSUFBSTt3QkFDcEIsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQzt3QkFDOUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUNqQzs7Ozs7Ozs7O1NBQ0Y7UUFDRCxJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxNQUFNLENBQUMsQ0FBQztRQUN0RCxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDbkIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxDQUFDLElBQUksQ0FDekQsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUNmLEdBQUcsQ0FBQyxVQUFDLEVBQWlCO1lBQ3BCLE9BQUEsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFO1FBQXhELENBQXdELENBQ3pELENBQ0YsQ0FBQztJQUNKLENBQUM7SUFFRCwrQ0FBVyxHQUFYO1FBQ0UsSUFBSSxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQ2xCLElBQUksRUFDSixJQUFJLENBQUMsU0FBUyxrQkFBSyxJQUFJLENBQUMsVUFBVSxFQUFLLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FDMUQsQ0FBQztRQUNGLElBQ0UsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTO1lBQ3RCLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEtBQUssVUFBVSxFQUM1QztZQUNBLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztTQUN6QztJQUNILENBQUM7SUFFRCx1Q0FBRyxHQUFILFVBQUksS0FBd0I7UUFDMUIsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFO1lBQ2hDLElBQU0sS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUM7WUFDMUIsSUFBTSxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQztZQUUxQixJQUFJLENBQUMsS0FBSyxJQUFJLEVBQUUsQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFO2dCQUN4QixJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ3pCO1lBRUQsd0JBQXdCO1lBQ3hCLElBQUksS0FBSyxFQUFFO2dCQUNULEtBQUssQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO2FBQ2xCO1lBQ0QsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNyQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDcEI7SUFDSCxDQUFDO0lBRUQsa0RBQWMsR0FBZCxVQUFlLEdBQVc7UUFDeEIsSUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7UUFFNUMsSUFBSSxLQUFLLElBQUksQ0FBQyxFQUFFO1lBQ2QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ25DO1FBQ0QsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ3JCLENBQUM7SUFFRCxpREFBYSxHQUFiLFVBQWMsR0FBVztRQUN2QixJQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUUzQyxJQUFJLEtBQUssSUFBSSxDQUFDLEVBQUU7WUFDZCxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDbEM7UUFDRCxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDckIsQ0FBQztJQUVELDRDQUFRLEdBQVIsVUFBUyxLQUFtQztRQUMxQyxJQUNFLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVTtZQUNoRCxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsS0FBSyxDQUFDLEVBQzdCO1lBQ0EsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3pDLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7WUFDeEMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNyQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDcEI7SUFDSCxDQUFDO0lBRUQsZ0RBQVksR0FBWixVQUFhLEtBQUs7UUFDaEIsSUFDRSxJQUFJLENBQUMsV0FBVztZQUNoQixJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVk7WUFDekIsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksS0FBSyxVQUFVLEVBQy9DO1lBQ0EsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDbEM7SUFDSCxDQUFDO0lBRUQsK0JBQStCO0lBQy9CLHVDQUF1QztJQUN2QyxnQkFBZ0I7SUFDaEIsb0RBQW9EO0lBQ3BELHVFQUF1RTtJQUN2RSxTQUFTO0lBQ1Qsa0NBQWtDO0lBQ2xDLHFDQUFxQztJQUNyQyxRQUFRO0lBQ1IsTUFBTTtJQUNOLElBQUk7SUFFSSwrQ0FBVyxHQUFuQixVQUFvQixLQUFLO1FBQ3ZCLElBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDbEMsSUFBSSxJQUFJLEVBQUU7WUFDUixJQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FDeEMsVUFBQyxFQUFPLElBQUssT0FBQSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxLQUFLLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLEVBQWpELENBQWlELENBQy9ELENBQUM7WUFDRixJQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FDMUMsVUFBQyxFQUFPLElBQUssT0FBQSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxLQUFLLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLEVBQWpELENBQWlELENBQy9ELENBQUM7WUFDRixJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUU7Z0JBQy9DLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQzVCO1NBQ0Y7SUFDSCxDQUFDO0lBRU8sNENBQVEsR0FBaEIsVUFBaUIsS0FBSyxFQUFFLElBQXFCO1FBQXJCLHFCQUFBLEVBQUEsWUFBcUI7UUFDM0MsSUFBSSxLQUFLLEVBQUU7WUFDVCxJQUFNLFFBQU0sR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLElBQUksS0FBSyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3pELElBQU0sU0FBUyxHQUFHLElBQUk7Z0JBQ3BCLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQ3RCLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxPQUFPLENBQUMsUUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDLEtBQUssQ0FBQyxFQUE3RCxDQUE2RCxDQUNyRTtnQkFDSCxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUN0QixVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLEtBQUssUUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxFQUFsRCxDQUFrRCxDQUMxRCxDQUFDO1lBQ04sT0FBTyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1NBQ3BFO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRU8sMkNBQU8sR0FBZixVQUFnQixLQUFVO1FBQ3hCLElBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3hDLE9BQU8sSUFBSTtZQUNULENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQ3RCLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxLQUFLLENBQUMsRUFBN0QsQ0FBNkQsQ0FDckU7WUFDSCxDQUFDLENBQUMsRUFBRSxDQUFDO0lBQ1QsQ0FBQztJQXRLUTtRQUFSLEtBQUssRUFBRTs7aUVBQWlCO0lBQ2hCO1FBQVIsS0FBSyxFQUFFOztrRUFBdUI7SUFDdEI7UUFBUixLQUFLLEVBQUU7O2dFQUFxQjtJQVlMO1FBQXZCLFNBQVMsQ0FBQyxXQUFXLENBQUM7MENBQVksVUFBVTtnRUFBbUI7SUFDN0M7UUFBbEIsU0FBUyxDQUFDLE1BQU0sQ0FBQzswQ0FBa0IsZUFBZTtzRUFBQztJQXhCekMseUJBQXlCO1FBdkdyQyxTQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsMEJBQTBCO1lBQ3BDLFFBQVEsRUFBRSxxL0VBMkVUO3FCQUVDLDBXQXFCQztTQUVKLENBQUM7aURBMkJ5QixxQkFBcUI7T0ExQm5DLHlCQUF5QixDQWdMckM7SUFBRCxnQ0FBQztDQUFBLEFBaExELElBZ0xDO1NBaExZLHlCQUF5QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgRWxlbWVudFJlZiwgSW5wdXQsIE9uSW5pdCwgVmlld0NoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEFic3RyYWN0Q29udHJvbCwgRm9ybUNvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IENPTU1BLCBFTlRFUiB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9rZXljb2Rlcyc7XHJcbmltcG9ydCB7XHJcbiAgTWF0QXV0b2NvbXBsZXRlU2VsZWN0ZWRFdmVudCxcclxuICBNYXRDaGlwSW5wdXRFdmVudCxcclxuICBNYXRBdXRvY29tcGxldGVcclxufSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgbWFwLCBzdGFydFdpdGggfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5pbXBvcnQgeyBKc29uU2NoZW1hRm9ybVNlcnZpY2UgfSBmcm9tICcuLi8uLi9qc29uLXNjaGVtYS1mb3JtLnNlcnZpY2UnO1xyXG5pbXBvcnQgXyBmcm9tICdsb2Rhc2gnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdtYXRlcmlhbC1jaGlwbGlzdC13aWRnZXQnLFxyXG4gIHRlbXBsYXRlOiBgXHJcbiAgICA8bWF0LWZvcm0tZmllbGRcclxuICAgICAgW2NsYXNzXT1cIm9wdGlvbnM/Lmh0bWxDbGFzcyB8fCAnJ1wiXHJcbiAgICAgIFtmbG9hdExhYmVsXT1cIlxyXG4gICAgICAgIG9wdGlvbnM/LmZsb2F0TGFiZWwgfHwgKG9wdGlvbnM/Lm5vdGl0bGUgPyAnbmV2ZXInIDogJ2F1dG8nKVxyXG4gICAgICBcIlxyXG4gICAgICBbc3R5bGUud2lkdGhdPVwiJzEwMCUnXCJcclxuICAgID5cclxuICAgICAgPG1hdC1jaGlwLWxpc3QgI2NoaXBMaXN0IFtzdHlsZS53aWR0aF09XCInMTAwJSdcIj5cclxuICAgICAgICA8ZGl2IGlkPVwiY3VycmVudENoaXBzXCI+XHJcbiAgICAgICAgICA8bWF0LWNoaXBcclxuICAgICAgICAgICAgKm5nRm9yPVwibGV0IG9wdCBvZiBjdXJyZW50T3B0XCJcclxuICAgICAgICAgICAgW3NlbGVjdGFibGVdPVwic2VsZWN0YWJsZVwiXHJcbiAgICAgICAgICAgIFtyZW1vdmFibGVdPVwiZW5hYmxlRGVsZXRlXCJcclxuICAgICAgICAgICAgKHJlbW92ZWQpPVwicmVtb3ZlQ3VycmVudChvcHQpXCJcclxuICAgICAgICAgICAgKGNsaWNrKT1cIm9uQ2hpcHNDbGljayhvcHQpXCJcclxuICAgICAgICAgID5cclxuICAgICAgICAgICAgPGRpdlxyXG4gICAgICAgICAgICAgIG1hdENoaXBBdmF0YXJcclxuICAgICAgICAgICAgICBjbGFzcz1cImNvbXBsZXRlLW1hcmtlclwiXHJcbiAgICAgICAgICAgICAgKm5nSWY9XCJvcHQuY29tcGxldGUgIT0gbnVsbCAmJiBvcHQuY29tcGxldGUgIT0gJydcIlxyXG4gICAgICAgICAgICAgIFtuZ0NsYXNzXT1cInsgY29tcGxldGU6IG9wdC5jb21wbGV0ZSB9XCJcclxuICAgICAgICAgICAgPjwvZGl2PlxyXG5cclxuICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJjaGlwLW5hbWVcIj57eyBvcHQubmFtZSB9fTwvc3Bhbj5cclxuICAgICAgICAgICAgPG1hdC1pY29uIG1hdENoaXBSZW1vdmUgKm5nSWY9XCJlbmFibGVEZWxldGVcIj5jYW5jZWw8L21hdC1pY29uPlxyXG4gICAgICAgICAgPC9tYXQtY2hpcD5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8aW5wdXRcclxuICAgICAgICAgIG1hdElucHV0XHJcbiAgICAgICAgICAjY2hpcElucHV0XHJcbiAgICAgICAgICBbcGxhY2Vob2xkZXJdPVwiXHJcbiAgICAgICAgICAgIG9wdGlvbnM/Lm5vdGl0bGUgPyBvcHRpb25zPy5wbGFjZWhvbGRlciA6IG9wdGlvbnM/LnRpdGxlXHJcbiAgICAgICAgICBcIlxyXG4gICAgICAgICAgW21hdEF1dG9jb21wbGV0ZV09XCJhdXRvXCJcclxuICAgICAgICAgIFttYXRDaGlwSW5wdXRGb3JdPVwiY2hpcExpc3RcIlxyXG4gICAgICAgICAgW21hdENoaXBJbnB1dFNlcGFyYXRvcktleUNvZGVzXT1cInNlcGFyYXRvcktleXNDb2Rlc1wiXHJcbiAgICAgICAgICBbbWF0Q2hpcElucHV0QWRkT25CbHVyXT1cImFkZE9uQmx1clwiXHJcbiAgICAgICAgICAobWF0Q2hpcElucHV0VG9rZW5FbmQpPVwiYWRkKCRldmVudClcIlxyXG4gICAgICAgICAgW3N0eWxlLndpZHRoXT1cIicxMDAlJ1wiXHJcbiAgICAgICAgICBbZm9ybUNvbnRyb2xdPVwiZHVtbXlGb3JtQ29udHJvbFwiXHJcbiAgICAgICAgICAqbmdJZj1cImVuYWJsZUlucHV0XCJcclxuICAgICAgICAvPlxyXG4gICAgICA8L21hdC1jaGlwLWxpc3Q+XHJcbiAgICAgIDxkaXY+XHJcbiAgICAgICAgPG1hdC1jaGlwXHJcbiAgICAgICAgICAqbmdGb3I9XCJsZXQgb3B0IG9mIHNlbGVjdGVkT3B0XCJcclxuICAgICAgICAgIFtzZWxlY3RhYmxlXT1cInNlbGVjdGFibGVcIlxyXG4gICAgICAgICAgW3JlbW92YWJsZV09XCJlbmFibGVEZWxldGVcIlxyXG4gICAgICAgICAgKHJlbW92ZWQpPVwicmVtb3ZlU2VsZWN0ZWQob3B0KVwiXHJcbiAgICAgICAgICAoY2xpY2spPVwib25DaGlwc0NsaWNrKG9wdClcIlxyXG4gICAgICAgID5cclxuICAgICAgICAgIDxkaXZcclxuICAgICAgICAgICAgbWF0Q2hpcEF2YXRhclxyXG4gICAgICAgICAgICBjbGFzcz1cImNvbXBsZXRlLW1hcmtlclwiXHJcbiAgICAgICAgICAgICpuZ0lmPVwib3B0LmNvbXBsZXRlICE9IG51bGwgJiYgb3B0LmNvbXBsZXRlICE9ICcnXCJcclxuICAgICAgICAgICAgW25nQ2xhc3NdPVwieyBjb21wbGV0ZTogb3B0LmNvbXBsZXRlIH1cIlxyXG4gICAgICAgICAgPjwvZGl2PlxyXG5cclxuICAgICAgICAgIDxzcGFuIGNsYXNzPVwiY2hpcC1uYW1lXCI+e3sgb3B0Lm5hbWUgfX08L3NwYW4+XHJcblxyXG4gICAgICAgICAgPG1hdC1pY29uIG1hdENoaXBSZW1vdmUgKm5nSWY9XCJlbmFibGVEZWxldGVcIj5jYW5jZWw8L21hdC1pY29uPlxyXG4gICAgICAgIDwvbWF0LWNoaXA+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgICA8bWF0LWF1dG9jb21wbGV0ZVxyXG4gICAgICAgICNhdXRvPVwibWF0QXV0b2NvbXBsZXRlXCJcclxuICAgICAgICBjbGFzcz1cImNoaXBsaXN0LWF1dG9jb21wXCJcclxuICAgICAgICBbc3R5bGUud2lkdGhdPVwiJzEwMCUnXCJcclxuICAgICAgICAob3B0aW9uU2VsZWN0ZWQpPVwic2VsZWN0ZWQoJGV2ZW50KVwiXHJcbiAgICAgID5cclxuICAgICAgICA8bWF0LW9wdGlvbiAqbmdGb3I9XCJsZXQgZmUgb2YgZmlsdGVyZWRFbnVtIHwgYXN5bmNcIiBbdmFsdWVdPVwiZmVcIj5cclxuICAgICAgICAgIHt7IGZlLm5hbWUgfX1cclxuICAgICAgICA8L21hdC1vcHRpb24+XHJcbiAgICAgIDwvbWF0LWF1dG9jb21wbGV0ZT5cclxuICAgIDwvbWF0LWZvcm0tZmllbGQ+XHJcbiAgYCxcclxuICBzdHlsZXM6IFtcclxuICAgIGBcclxuICAgICAgLm1hdC1zdGFuZGFyZC1jaGlwIHtcclxuICAgICAgICBtYXJnaW46IDRweCA4cHggNHB4IDA7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC5jaGlwLW5hbWUge1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDJweDtcclxuICAgICAgfVxyXG5cclxuICAgICAgLm1hdC1jaGlwLXJlbW92ZSB7XHJcbiAgICAgICAgZm9udC1zaXplOiAzMnB4O1xyXG4gICAgICAgIG9wYWNpdHk6IDEgIWltcG9ydGFudDtcclxuICAgICAgfVxyXG5cclxuICAgICAgLmNvbXBsZXRlLW1hcmtlciB7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogcmVkO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAuY29tcGxldGUge1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IGdyZWVuO1xyXG4gICAgICB9XHJcbiAgICBgXHJcbiAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTWF0ZXJpYWxDaGlwbGlzdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgZm9ybUNvbnRyb2w6IEFic3RyYWN0Q29udHJvbDtcclxuICBkdW1teUZvcm1Db250cm9sID0gbmV3IEZvcm1Db250cm9sKCk7XHJcbiAgY29udHJvbE5hbWU6IHN0cmluZztcclxuICBjb250cm9sVmFsdWU6IGFueTtcclxuICBjb250cm9sRGlzYWJsZWQgPSB0cnVlO1xyXG4gIGJvdW5kQ29udHJvbCA9IGZhbHNlO1xyXG4gIG9wdGlvbnM6IGFueTtcclxuICBpc0FycmF5ID0gdHJ1ZTtcclxuICBASW5wdXQoKSBsYXlvdXROb2RlOiBhbnk7XHJcbiAgQElucHV0KCkgbGF5b3V0SW5kZXg6IG51bWJlcltdO1xyXG4gIEBJbnB1dCgpIGRhdGFJbmRleDogbnVtYmVyW107XHJcblxyXG4gIHNlbGVjdGFibGUgPSB0cnVlO1xyXG4gIGFkZE9uQmx1ciA9IHRydWU7XHJcbiAgZW5hYmxlSW5wdXQgPSB0cnVlO1xyXG4gIGVuYWJsZURlbGV0ZSA9IHRydWU7XHJcbiAgZW5hYmxlQ2xpY2sgPSB0cnVlO1xyXG4gIHNlcGFyYXRvcktleXNDb2RlczogbnVtYmVyW10gPSBbRU5URVIsIENPTU1BXTtcclxuICBmaWx0ZXJlZEVudW06IE9ic2VydmFibGU8c3RyaW5nW10+O1xyXG4gIHNlbGVjdGVkT3B0OiBzdHJpbmdbXSA9IFtdO1xyXG4gIGN1cnJlbnRPcHQ6IHN0cmluZ1tdID0gW107XHJcblxyXG4gIEBWaWV3Q2hpbGQoJ2NoaXBJbnB1dCcpIGNoaXBJbnB1dDogRWxlbWVudFJlZjxIVE1MSW5wdXRFbGVtZW50PjtcclxuICBAVmlld0NoaWxkKCdhdXRvJykgbWF0QXV0b2NvbXBsZXRlOiBNYXRBdXRvY29tcGxldGU7XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUganNmOiBKc29uU2NoZW1hRm9ybVNlcnZpY2UpIHt9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgdGhpcy5vcHRpb25zID0gdGhpcy5sYXlvdXROb2RlLm9wdGlvbnMgfHwge307XHJcbiAgICB0aGlzLm9wdGlvbnMuZW51bSA9IHRoaXMub3B0aW9ucy5lbnVtIHx8IFtdO1xyXG4gICAgdGhpcy5zZWxlY3RlZE9wdCA9IHRoaXMub3B0aW9ucy5zZWxlY3RlZE9wdCB8fCBbXTtcclxuICAgIHRoaXMuY3VycmVudE9wdCA9IFtdO1xyXG4gICAgdGhpcy5lbmFibGVJbnB1dCA9IHRoaXMub3B0aW9ucy5lbmFibGVJbnB1dDtcclxuICAgIHRoaXMuZW5hYmxlRGVsZXRlID0gdGhpcy5vcHRpb25zLmVuYWJsZURlbGV0ZTtcclxuICAgIHRoaXMuZW5hYmxlQ2xpY2sgPSB0aGlzLm9wdGlvbnMuZW5hYmxlQ2xpY2s7XHJcbiAgICB0aGlzLmpzZi5pbml0aWFsaXplQ29udHJvbCh0aGlzKTtcclxuICAgIGNvbnN0IF9mY1ZhbHVlID0gSlNPTi5wYXJzZSh0aGlzLmZvcm1Db250cm9sLnZhbHVlKTtcclxuICAgIGlmIChfZmNWYWx1ZSAmJiBfZmNWYWx1ZS5sZW5ndGgpIHtcclxuICAgICAgZm9yIChjb25zdCB2YWx1ZSBvZiBfZmNWYWx1ZSkge1xyXG4gICAgICAgIHZhbHVlLmNvbXBsZXRlICE9IG51bGxcclxuICAgICAgICAgID8gdGhpcy5zZWxlY3RlZE9wdC5wdXNoKHZhbHVlKVxyXG4gICAgICAgICAgOiB0aGlzLmN1cnJlbnRPcHQucHVzaCh2YWx1ZSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIHRoaXMuc2VsZWN0ZWRPcHQgPSBfLnVuaXFCeSh0aGlzLnNlbGVjdGVkT3B0LCAnbmFtZScpO1xyXG4gICAgdGhpcy51cGRhdGVWYWx1ZSgpO1xyXG4gICAgdGhpcy5maWx0ZXJlZEVudW0gPSB0aGlzLmR1bW15Rm9ybUNvbnRyb2wudmFsdWVDaGFuZ2VzLnBpcGUoXHJcbiAgICAgIHN0YXJ0V2l0aChudWxsKSxcclxuICAgICAgbWFwKChmZTogc3RyaW5nIHwgbnVsbCkgPT5cclxuICAgICAgICBmZSA/IHRoaXMuZmluZE9wdHMoZmUsIHRydWUpIDogdGhpcy5vcHRpb25zLmVudW0uc2xpY2UoKVxyXG4gICAgICApXHJcbiAgICApO1xyXG4gIH1cclxuXHJcbiAgdXBkYXRlVmFsdWUoKSB7XHJcbiAgICB0aGlzLmpzZi51cGRhdGVWYWx1ZShcclxuICAgICAgdGhpcyxcclxuICAgICAgSlNPTi5zdHJpbmdpZnkoWy4uLnRoaXMuY3VycmVudE9wdCwgLi4udGhpcy5zZWxlY3RlZE9wdF0pXHJcbiAgICApO1xyXG4gICAgaWYgKFxyXG4gICAgICB0aGlzLm9wdGlvbnMub25DaGFuZ2VzICYmXHJcbiAgICAgIHR5cGVvZiB0aGlzLm9wdGlvbnMub25DaGFuZ2VzID09PSAnZnVuY3Rpb24nXHJcbiAgICApIHtcclxuICAgICAgdGhpcy5vcHRpb25zLm9uQ2hhbmdlcyh0aGlzLmN1cnJlbnRPcHQpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgYWRkKGV2ZW50OiBNYXRDaGlwSW5wdXRFdmVudCk6IHZvaWQge1xyXG4gICAgaWYgKCF0aGlzLm1hdEF1dG9jb21wbGV0ZS5pc09wZW4pIHtcclxuICAgICAgY29uc3QgaW5wdXQgPSBldmVudC5pbnB1dDtcclxuICAgICAgY29uc3QgdmFsdWUgPSBldmVudC52YWx1ZTtcclxuXHJcbiAgICAgIGlmICgodmFsdWUgfHwgJycpLnRyaW0oKSkge1xyXG4gICAgICAgIHRoaXMuYWRkU2VsZWN0ZWQodmFsdWUpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAvLyBSZXNldCB0aGUgaW5wdXQgdmFsdWVcclxuICAgICAgaWYgKGlucHV0KSB7XHJcbiAgICAgICAgaW5wdXQudmFsdWUgPSAnJztcclxuICAgICAgfVxyXG4gICAgICB0aGlzLmR1bW15Rm9ybUNvbnRyb2wuc2V0VmFsdWUobnVsbCk7XHJcbiAgICAgIHRoaXMudXBkYXRlVmFsdWUoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHJlbW92ZVNlbGVjdGVkKG9wdDogc3RyaW5nKTogdm9pZCB7XHJcbiAgICBjb25zdCBpbmRleCA9IHRoaXMuc2VsZWN0ZWRPcHQuaW5kZXhPZihvcHQpO1xyXG5cclxuICAgIGlmIChpbmRleCA+PSAwKSB7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRPcHQuc3BsaWNlKGluZGV4LCAxKTtcclxuICAgIH1cclxuICAgIHRoaXMudXBkYXRlVmFsdWUoKTtcclxuICB9XHJcblxyXG4gIHJlbW92ZUN1cnJlbnQob3B0OiBzdHJpbmcpOiB2b2lkIHtcclxuICAgIGNvbnN0IGluZGV4ID0gdGhpcy5jdXJyZW50T3B0LmluZGV4T2Yob3B0KTtcclxuXHJcbiAgICBpZiAoaW5kZXggPj0gMCkge1xyXG4gICAgICB0aGlzLmN1cnJlbnRPcHQuc3BsaWNlKGluZGV4LCAxKTtcclxuICAgIH1cclxuICAgIHRoaXMudXBkYXRlVmFsdWUoKTtcclxuICB9XHJcblxyXG4gIHNlbGVjdGVkKGV2ZW50OiBNYXRBdXRvY29tcGxldGVTZWxlY3RlZEV2ZW50KTogdm9pZCB7XHJcbiAgICBpZiAoXHJcbiAgICAgIHRoaXMuY3VycmVudE9wdC5sZW5ndGggPCB0aGlzLm9wdGlvbnMuY2hpcHNMaW1pdCB8fFxyXG4gICAgICB0aGlzLm9wdGlvbnMuY2hpcHNMaW1pdCA9PT0gMFxyXG4gICAgKSB7XHJcbiAgICAgIHRoaXMuYWRkU2VsZWN0ZWQoZXZlbnQub3B0aW9uLnZpZXdWYWx1ZSk7XHJcbiAgICAgIHRoaXMuY2hpcElucHV0Lm5hdGl2ZUVsZW1lbnQudmFsdWUgPSAnJztcclxuICAgICAgdGhpcy5kdW1teUZvcm1Db250cm9sLnNldFZhbHVlKG51bGwpO1xyXG4gICAgICB0aGlzLnVwZGF0ZVZhbHVlKCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBvbkNoaXBzQ2xpY2sodmFsdWUpIHtcclxuICAgIGlmIChcclxuICAgICAgdGhpcy5lbmFibGVDbGljayAmJlxyXG4gICAgICB0aGlzLm9wdGlvbnMub25DaGlwc0NsaWNrICYmXHJcbiAgICAgIHR5cGVvZiB0aGlzLm9wdGlvbnMub25DaGlwc0NsaWNrID09PSAnZnVuY3Rpb24nXHJcbiAgICApIHtcclxuICAgICAgdGhpcy5vcHRpb25zLm9uQ2hpcHNDbGljayh2YWx1ZSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvLyBwcml2YXRlIGFkZFNlbGVjdGVkKHZhbHVlKSB7XHJcbiAgLy8gICBjb25zdCBfb3B0ID0gdGhpcy5maW5kT3B0cyh2YWx1ZSk7XHJcbiAgLy8gICBpZiAoX29wdCkge1xyXG4gIC8vICAgICBjb25zdCBfc2VsZWN0ZWRPcHQgPSB0aGlzLnNlbGVjdGVkT3B0LmZpbHRlcihcclxuICAvLyAgICAgICAoc286IGFueSkgPT4gc28ubmFtZS50b0xvd2VyQ2FzZSgpID09PSBfb3B0Lm5hbWUudG9Mb3dlckNhc2UoKVxyXG4gIC8vICAgICApO1xyXG4gIC8vICAgICBpZiAoIV9zZWxlY3RlZE9wdC5sZW5ndGgpIHtcclxuICAvLyAgICAgICB0aGlzLnNlbGVjdGVkT3B0LnB1c2goX29wdCk7XHJcbiAgLy8gICAgIH1cclxuICAvLyAgIH1cclxuICAvLyB9XHJcblxyXG4gIHByaXZhdGUgYWRkU2VsZWN0ZWQodmFsdWUpIHtcclxuICAgIGNvbnN0IF9vcHQgPSB0aGlzLmZpbmRPcHRzKHZhbHVlKTtcclxuICAgIGlmIChfb3B0KSB7XHJcbiAgICAgIGNvbnN0IF9jdXJyZW50T3B0ID0gdGhpcy5jdXJyZW50T3B0LmZpbHRlcihcclxuICAgICAgICAoc286IGFueSkgPT4gc28ubmFtZS50b0xvd2VyQ2FzZSgpID09PSBfb3B0Lm5hbWUudG9Mb3dlckNhc2UoKVxyXG4gICAgICApO1xyXG4gICAgICBjb25zdCBfc2VsZWN0ZWRPcHQgPSB0aGlzLnNlbGVjdGVkT3B0LmZpbHRlcihcclxuICAgICAgICAoc286IGFueSkgPT4gc28ubmFtZS50b0xvd2VyQ2FzZSgpID09PSBfb3B0Lm5hbWUudG9Mb3dlckNhc2UoKVxyXG4gICAgICApO1xyXG4gICAgICBpZiAoIV9jdXJyZW50T3B0Lmxlbmd0aCAmJiAhX3NlbGVjdGVkT3B0Lmxlbmd0aCkge1xyXG4gICAgICAgIHRoaXMuY3VycmVudE9wdC5wdXNoKF9vcHQpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGZpbmRPcHRzKHZhbHVlLCBsaWtlOiBib29sZWFuID0gZmFsc2UpIHtcclxuICAgIGlmICh2YWx1ZSkge1xyXG4gICAgICBjb25zdCBfdmFsdWUgPSAodmFsdWUubmFtZSB8fCB2YWx1ZSkudHJpbSgpLnNwbGl0KCcgfCAnKTtcclxuICAgICAgY29uc3QgX2ZvdW5kT3B0ID0gbGlrZVxyXG4gICAgICAgID8gdGhpcy5vcHRpb25zLmVudW0uZmlsdGVyKFxyXG4gICAgICAgICAgICBvcHQgPT4gb3B0Lm5hbWUudG9Mb3dlckNhc2UoKS5pbmRleE9mKF92YWx1ZVswXS50b0xvd2VyQ2FzZSgpKSA9PT0gMFxyXG4gICAgICAgICAgKVxyXG4gICAgICAgIDogdGhpcy5vcHRpb25zLmVudW0uZmlsdGVyKFxyXG4gICAgICAgICAgICBvcHQgPT4gb3B0Lm5hbWUudG9Mb3dlckNhc2UoKSA9PT0gX3ZhbHVlWzBdLnRvTG93ZXJDYXNlKClcclxuICAgICAgICAgICk7XHJcbiAgICAgIHJldHVybiBfZm91bmRPcHQubGVuZ3RoID8gKGxpa2UgPyBfZm91bmRPcHQgOiBfZm91bmRPcHRbMF0pIDogbnVsbDtcclxuICAgIH1cclxuICAgIHJldHVybiBudWxsO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBfZmlsdGVyKHZhbHVlOiBhbnkpOiBzdHJpbmdbXSB7XHJcbiAgICBjb25zdCBfb3B0ID0gdGhpcy5maW5kT3B0cyh2YWx1ZSwgdHJ1ZSk7XHJcbiAgICByZXR1cm4gX29wdFxyXG4gICAgICA/IHRoaXMub3B0aW9ucy5lbnVtLmZpbHRlcihcclxuICAgICAgICAgIG9wdCA9PiBvcHQubmFtZS50b0xvd2VyQ2FzZSgpLmluZGV4T2YoX29wdC5uYW1lLnRvTG93ZXJDYXNlKCkpID09PSAwXHJcbiAgICAgICAgKVxyXG4gICAgICA6IFtdO1xyXG4gIH1cclxufVxyXG4iXX0=