import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { JsonSchemaFormService } from '../../json-schema-form.service';
var MaterialAutoCompleteComponent = /** @class */ (function () {
    function MaterialAutoCompleteComponent(jsf) {
        this.jsf = jsf;
        this.dummyFormControl = new FormControl();
        this.controlDisabled = true;
        this.boundControl = false;
        this.isArray = true;
    }
    MaterialAutoCompleteComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.options = this.layoutNode.options || {};
        this.enum = this.options.enum || [];
        this.jsf.initializeControl(this);
        this.setValue(this.formControl.value);
        this.dummyFormControl.valueChanges.subscribe(function (value) {
            return _this.updateValue(value);
        });
    };
    MaterialAutoCompleteComponent.prototype.selectOption = function (option) {
        var foundEnum = this.enum.find(function (e) { return e.id === option.id; });
        this.dummyFormControl.setValue(foundEnum ? foundEnum.name : foundEnum);
    };
    MaterialAutoCompleteComponent.prototype.displayWithName = function (opt) {
        return !opt ? '' : typeof opt === 'string' ? opt : opt.name;
    };
    MaterialAutoCompleteComponent.prototype.setValue = function (value) {
        var foundEnum = this.enum.find(function (e) { return e.id === value; });
        this.dummyFormControl.setValue(foundEnum ? foundEnum.name : foundEnum);
    };
    MaterialAutoCompleteComponent.prototype.updateValue = function (value) {
        if (!value) {
            this.enum = this.options.enum || [];
            this.jsf.updateValue(this, null);
        }
        else if (typeof value === 'string') {
            this.enum = this.options.enum.filter(function (e) { return e.name.toLowerCase().indexOf(value.toLowerCase()) !== -1; });
        }
        else {
            var foundEnum = this.enum.find(function (e) { return e.id === value.id; });
            this.jsf.updateValue(this, foundEnum ? foundEnum.id : foundEnum);
        }
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], MaterialAutoCompleteComponent.prototype, "layoutNode", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Array)
    ], MaterialAutoCompleteComponent.prototype, "layoutIndex", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Array)
    ], MaterialAutoCompleteComponent.prototype, "dataIndex", void 0);
    MaterialAutoCompleteComponent = tslib_1.__decorate([
        Component({
            selector: 'material-autocomplete-widget',
            template: "\n    <mat-form-field\n      [class]=\"options?.htmlClass || ''\"\n      [floatLabel]=\"\n        options?.floatLabel || (options?.notitle ? 'never' : 'auto')\n      \"\n      [style.width]=\"'30%'\"\n    >\n      <input\n        matInput\n        [formControl]=\"dummyFormControl\"\n        [matAutocomplete]=\"leftAuto\"\n        [placeholder]=\"options?.notitle ? options?.placeholder : options?.title\"\n        [style.width]=\"'100%'\"\n      />\n      <mat-autocomplete\n        #leftAuto=\"matAutocomplete\"\n        (optionSelected)=\"selectOption($event.option.value)\"\n      >\n        <mat-option *ngFor=\"let option of enum\" [value]=\"option\">\n          {{ option.name }}\n        </mat-option>\n      </mat-autocomplete>\n    </mat-form-field>\n  "
        }),
        tslib_1.__metadata("design:paramtypes", [JsonSchemaFormService])
    ], MaterialAutoCompleteComponent);
    return MaterialAutoCompleteComponent;
}());
export { MaterialAutoCompleteComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWF0ZXJpYWwtYXV0b2NvbXBsZXRlLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FuZ3VsYXI2LWpzb24tc2NoZW1hLWZvcm0vIiwic291cmNlcyI6WyJsaWIvZnJhbWV3b3JrLWxpYnJhcnkvbWF0ZXJpYWwtZGVzaWduLWZyYW1ld29yay9tYXRlcmlhbC1hdXRvY29tcGxldGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBVSxNQUFNLGVBQWUsQ0FBQztBQUN6RCxPQUFPLEVBQW1CLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRTlELE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBOEJ2RTtJQWVFLHVDQUFvQixHQUEwQjtRQUExQixRQUFHLEdBQUgsR0FBRyxDQUF1QjtRQWI5QyxxQkFBZ0IsR0FBRyxJQUFJLFdBQVcsRUFBRSxDQUFDO1FBR3JDLG9CQUFlLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLGlCQUFZLEdBQUcsS0FBSyxDQUFDO1FBRXJCLFlBQU8sR0FBRyxJQUFJLENBQUM7SUFPa0MsQ0FBQztJQUVsRCxnREFBUSxHQUFSO1FBQUEsaUJBUUM7UUFQQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxJQUFJLEVBQUUsQ0FBQztRQUM3QyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQztRQUNwQyxJQUFJLENBQUMsR0FBRyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2pDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN0QyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxVQUFBLEtBQUs7WUFDaEQsT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQztRQUF2QixDQUF1QixDQUN4QixDQUFDO0lBQ0osQ0FBQztJQUVELG9EQUFZLEdBQVosVUFBYSxNQUFNO1FBQ2pCLElBQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLEVBQUUsS0FBSyxNQUFNLENBQUMsRUFBRSxFQUFsQixDQUFrQixDQUFDLENBQUM7UUFDMUQsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ3pFLENBQUM7SUFFRCx1REFBZSxHQUFmLFVBQWdCLEdBQUc7UUFDakIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLEdBQUcsS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQztJQUM5RCxDQUFDO0lBRUQsZ0RBQVEsR0FBUixVQUFTLEtBQUs7UUFDWixJQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxFQUFFLEtBQUssS0FBSyxFQUFkLENBQWMsQ0FBQyxDQUFDO1FBQ3RELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUN6RSxDQUFDO0lBRUQsbURBQVcsR0FBWCxVQUFZLEtBQUs7UUFDZixJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ1YsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksSUFBSSxFQUFFLENBQUM7WUFDcEMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO1NBQ2xDO2FBQU0sSUFBSSxPQUFPLEtBQUssS0FBSyxRQUFRLEVBQUU7WUFDcEMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQ2xDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQXhELENBQXdELENBQzlELENBQUM7U0FDSDthQUFNO1lBQ0wsSUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsRUFBRSxLQUFLLEtBQUssQ0FBQyxFQUFFLEVBQWpCLENBQWlCLENBQUMsQ0FBQztZQUN6RCxJQUFJLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUNsRTtJQUNILENBQUM7SUE1Q1E7UUFBUixLQUFLLEVBQUU7O3FFQUFpQjtJQUNoQjtRQUFSLEtBQUssRUFBRTs7c0VBQXVCO0lBQ3RCO1FBQVIsS0FBSyxFQUFFOztvRUFBcUI7SUFYbEIsNkJBQTZCO1FBNUJ6QyxTQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsOEJBQThCO1lBQ3hDLFFBQVEsRUFBRSw4dkJBd0JUO1NBQ0YsQ0FBQztpREFnQnlCLHFCQUFxQjtPQWZuQyw2QkFBNkIsQ0FzRHpDO0lBQUQsb0NBQUM7Q0FBQSxBQXRERCxJQXNEQztTQXREWSw2QkFBNkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQWJzdHJhY3RDb250cm9sLCBGb3JtQ29udHJvbCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuXHJcbmltcG9ydCB7IEpzb25TY2hlbWFGb3JtU2VydmljZSB9IGZyb20gJy4uLy4uL2pzb24tc2NoZW1hLWZvcm0uc2VydmljZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ21hdGVyaWFsLWF1dG9jb21wbGV0ZS13aWRnZXQnLFxyXG4gIHRlbXBsYXRlOiBgXHJcbiAgICA8bWF0LWZvcm0tZmllbGRcclxuICAgICAgW2NsYXNzXT1cIm9wdGlvbnM/Lmh0bWxDbGFzcyB8fCAnJ1wiXHJcbiAgICAgIFtmbG9hdExhYmVsXT1cIlxyXG4gICAgICAgIG9wdGlvbnM/LmZsb2F0TGFiZWwgfHwgKG9wdGlvbnM/Lm5vdGl0bGUgPyAnbmV2ZXInIDogJ2F1dG8nKVxyXG4gICAgICBcIlxyXG4gICAgICBbc3R5bGUud2lkdGhdPVwiJzMwJSdcIlxyXG4gICAgPlxyXG4gICAgICA8aW5wdXRcclxuICAgICAgICBtYXRJbnB1dFxyXG4gICAgICAgIFtmb3JtQ29udHJvbF09XCJkdW1teUZvcm1Db250cm9sXCJcclxuICAgICAgICBbbWF0QXV0b2NvbXBsZXRlXT1cImxlZnRBdXRvXCJcclxuICAgICAgICBbcGxhY2Vob2xkZXJdPVwib3B0aW9ucz8ubm90aXRsZSA/IG9wdGlvbnM/LnBsYWNlaG9sZGVyIDogb3B0aW9ucz8udGl0bGVcIlxyXG4gICAgICAgIFtzdHlsZS53aWR0aF09XCInMTAwJSdcIlxyXG4gICAgICAvPlxyXG4gICAgICA8bWF0LWF1dG9jb21wbGV0ZVxyXG4gICAgICAgICNsZWZ0QXV0bz1cIm1hdEF1dG9jb21wbGV0ZVwiXHJcbiAgICAgICAgKG9wdGlvblNlbGVjdGVkKT1cInNlbGVjdE9wdGlvbigkZXZlbnQub3B0aW9uLnZhbHVlKVwiXHJcbiAgICAgID5cclxuICAgICAgICA8bWF0LW9wdGlvbiAqbmdGb3I9XCJsZXQgb3B0aW9uIG9mIGVudW1cIiBbdmFsdWVdPVwib3B0aW9uXCI+XHJcbiAgICAgICAgICB7eyBvcHRpb24ubmFtZSB9fVxyXG4gICAgICAgIDwvbWF0LW9wdGlvbj5cclxuICAgICAgPC9tYXQtYXV0b2NvbXBsZXRlPlxyXG4gICAgPC9tYXQtZm9ybS1maWVsZD5cclxuICBgXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBNYXRlcmlhbEF1dG9Db21wbGV0ZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgZm9ybUNvbnRyb2w6IEFic3RyYWN0Q29udHJvbDtcclxuICBkdW1teUZvcm1Db250cm9sID0gbmV3IEZvcm1Db250cm9sKCk7XHJcbiAgY29udHJvbE5hbWU6IHN0cmluZztcclxuICBjb250cm9sVmFsdWU6IGFueTtcclxuICBjb250cm9sRGlzYWJsZWQgPSB0cnVlO1xyXG4gIGJvdW5kQ29udHJvbCA9IGZhbHNlO1xyXG4gIG9wdGlvbnM6IGFueTtcclxuICBpc0FycmF5ID0gdHJ1ZTtcclxuICBASW5wdXQoKSBsYXlvdXROb2RlOiBhbnk7XHJcbiAgQElucHV0KCkgbGF5b3V0SW5kZXg6IG51bWJlcltdO1xyXG4gIEBJbnB1dCgpIGRhdGFJbmRleDogbnVtYmVyW107XHJcblxyXG4gIGVudW06IGFueVtdO1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGpzZjogSnNvblNjaGVtYUZvcm1TZXJ2aWNlKSB7fVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICAgIHRoaXMub3B0aW9ucyA9IHRoaXMubGF5b3V0Tm9kZS5vcHRpb25zIHx8IHt9O1xyXG4gICAgdGhpcy5lbnVtID0gdGhpcy5vcHRpb25zLmVudW0gfHwgW107XHJcbiAgICB0aGlzLmpzZi5pbml0aWFsaXplQ29udHJvbCh0aGlzKTtcclxuICAgIHRoaXMuc2V0VmFsdWUodGhpcy5mb3JtQ29udHJvbC52YWx1ZSk7XHJcbiAgICB0aGlzLmR1bW15Rm9ybUNvbnRyb2wudmFsdWVDaGFuZ2VzLnN1YnNjcmliZSh2YWx1ZSA9PlxyXG4gICAgICB0aGlzLnVwZGF0ZVZhbHVlKHZhbHVlKVxyXG4gICAgKTtcclxuICB9XHJcblxyXG4gIHNlbGVjdE9wdGlvbihvcHRpb24pIHtcclxuICAgIGNvbnN0IGZvdW5kRW51bSA9IHRoaXMuZW51bS5maW5kKGUgPT4gZS5pZCA9PT0gb3B0aW9uLmlkKTtcclxuICAgIHRoaXMuZHVtbXlGb3JtQ29udHJvbC5zZXRWYWx1ZShmb3VuZEVudW0gPyBmb3VuZEVudW0ubmFtZSA6IGZvdW5kRW51bSk7XHJcbiAgfVxyXG5cclxuICBkaXNwbGF5V2l0aE5hbWUob3B0KSB7XHJcbiAgICByZXR1cm4gIW9wdCA/ICcnIDogdHlwZW9mIG9wdCA9PT0gJ3N0cmluZycgPyBvcHQgOiBvcHQubmFtZTtcclxuICB9XHJcblxyXG4gIHNldFZhbHVlKHZhbHVlKSB7XHJcbiAgICBjb25zdCBmb3VuZEVudW0gPSB0aGlzLmVudW0uZmluZChlID0+IGUuaWQgPT09IHZhbHVlKTtcclxuICAgIHRoaXMuZHVtbXlGb3JtQ29udHJvbC5zZXRWYWx1ZShmb3VuZEVudW0gPyBmb3VuZEVudW0ubmFtZSA6IGZvdW5kRW51bSk7XHJcbiAgfVxyXG5cclxuICB1cGRhdGVWYWx1ZSh2YWx1ZSkge1xyXG4gICAgaWYgKCF2YWx1ZSkge1xyXG4gICAgICB0aGlzLmVudW0gPSB0aGlzLm9wdGlvbnMuZW51bSB8fCBbXTtcclxuICAgICAgdGhpcy5qc2YudXBkYXRlVmFsdWUodGhpcywgbnVsbCk7XHJcbiAgICB9IGVsc2UgaWYgKHR5cGVvZiB2YWx1ZSA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgdGhpcy5lbnVtID0gdGhpcy5vcHRpb25zLmVudW0uZmlsdGVyKFxyXG4gICAgICAgIGUgPT4gZS5uYW1lLnRvTG93ZXJDYXNlKCkuaW5kZXhPZih2YWx1ZS50b0xvd2VyQ2FzZSgpKSAhPT0gLTFcclxuICAgICAgKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGNvbnN0IGZvdW5kRW51bSA9IHRoaXMuZW51bS5maW5kKGUgPT4gZS5pZCA9PT0gdmFsdWUuaWQpO1xyXG4gICAgICB0aGlzLmpzZi51cGRhdGVWYWx1ZSh0aGlzLCBmb3VuZEVudW0gPyBmb3VuZEVudW0uaWQgOiBmb3VuZEVudW0pO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iXX0=