import * as tslib_1 from "tslib";
import { FlexLayoutRootComponent } from './flex-layout-root.component';
import { FlexLayoutSectionComponent } from './flex-layout-section.component';
import { Framework } from '../framework';
import { Injectable } from '@angular/core';
import { MaterialAddReferenceComponent } from './material-add-reference.component';
import { MaterialAutoCompleteComponent } from './material-autocomplete.component';
import { MaterialButtonComponent } from './material-button.component';
import { MaterialButtonGroupComponent } from './material-button-group.component';
import { MaterialCheckboxComponent } from './material-checkbox.component';
import { MaterialCheckboxesComponent } from './material-checkboxes.component';
import { MaterialChiplistComponent } from './material-chiplist.component';
import { MaterialComputationComponent } from './material-computation.component';
import { MaterialDatepickerComponent } from './material-datepicker.component';
import { MaterialDesignFrameworkComponent } from './material-design-framework.component';
import { MaterialDynamicOptionsWidget } from './material-dynamic-options.component';
import { MaterialFileComponent } from './material-file.component';
import { MaterialInputComponent } from './material-input.component';
import { MaterialNumberComponent } from './material-number.component';
import { MaterialOneOfComponent } from './material-one-of.component';
import { MaterialRadiosComponent } from './material-radios.component';
import { MaterialSelectComponent } from './material-select.component';
import { MaterialSliderComponent } from './material-slider.component';
import { MaterialStepperComponent } from './material-stepper.component';
import { MaterialTabsComponent } from './material-tabs.component';
import { MaterialTextareaComponent } from './material-textarea.component';
import { MaterialMultiTextFieldComponent } from './material-multitextfield.component';
import { MaterialPasswordComponent } from './material-password.component';
// Material Design Framework
// https://github.com/angular/material2
var MaterialDesignFramework = /** @class */ (function (_super) {
    tslib_1.__extends(MaterialDesignFramework, _super);
    function MaterialDesignFramework() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.name = 'material-design';
        _this.framework = MaterialDesignFrameworkComponent;
        _this.stylesheets = [
            '//fonts.googleapis.com/icon?family=Material+Icons',
            '//fonts.googleapis.com/css?family=Roboto:300,400,500,700'
        ];
        _this.widgets = {
            root: FlexLayoutRootComponent,
            section: FlexLayoutSectionComponent,
            $ref: MaterialAddReferenceComponent,
            autocomplete: MaterialAutoCompleteComponent,
            button: MaterialButtonComponent,
            'button-group': MaterialButtonGroupComponent,
            checkbox: MaterialCheckboxComponent,
            checkboxes: MaterialCheckboxesComponent,
            chiplist: MaterialChiplistComponent,
            computation: MaterialComputationComponent,
            multitextfield: MaterialMultiTextFieldComponent,
            date: MaterialDatepickerComponent,
            dynamicoption: MaterialDynamicOptionsWidget,
            file: MaterialFileComponent,
            number: MaterialNumberComponent,
            'one-of': MaterialOneOfComponent,
            password: MaterialPasswordComponent,
            radios: MaterialRadiosComponent,
            select: MaterialSelectComponent,
            slider: MaterialSliderComponent,
            stepper: MaterialStepperComponent,
            tabs: MaterialTabsComponent,
            text: MaterialInputComponent,
            textarea: MaterialTextareaComponent,
            'alt-date': 'date',
            'any-of': 'one-of',
            card: 'section',
            color: 'text',
            'expansion-panel': 'section',
            hidden: 'none',
            image: 'none',
            integer: 'number',
            radiobuttons: 'button-group',
            range: 'slider',
            submit: 'button',
            tagsinput: 'chiplist',
            wizard: 'stepper'
        };
        return _this;
    }
    MaterialDesignFramework = tslib_1.__decorate([
        Injectable()
    ], MaterialDesignFramework);
    return MaterialDesignFramework;
}(Framework));
export { MaterialDesignFramework };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWF0ZXJpYWwtZGVzaWduLmZyYW1ld29yay5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FuZ3VsYXI2LWpzb24tc2NoZW1hLWZvcm0vIiwic291cmNlcyI6WyJsaWIvZnJhbWV3b3JrLWxpYnJhcnkvbWF0ZXJpYWwtZGVzaWduLWZyYW1ld29yay9tYXRlcmlhbC1kZXNpZ24uZnJhbWV3b3JrLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUN2RSxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUM3RSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLDZCQUE2QixFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFDbkYsT0FBTyxFQUFFLDZCQUE2QixFQUFFLE1BQU0sbUNBQW1DLENBQUM7QUFDbEYsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDdEUsT0FBTyxFQUFFLDRCQUE0QixFQUFFLE1BQU0sbUNBQW1DLENBQUM7QUFDakYsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFDMUUsT0FBTyxFQUFFLDJCQUEyQixFQUFFLE1BQU0saUNBQWlDLENBQUM7QUFDOUUsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFDMUUsT0FBTyxFQUFFLDRCQUE0QixFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDaEYsT0FBTyxFQUFFLDJCQUEyQixFQUFFLE1BQU0saUNBQWlDLENBQUM7QUFDOUUsT0FBTyxFQUFFLGdDQUFnQyxFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDekYsT0FBTyxFQUFFLDRCQUE0QixFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFDcEYsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFDbEUsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDcEUsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDdEUsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDckUsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDdEUsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDdEUsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDdEUsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDeEUsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFDbEUsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFDMUUsT0FBTyxFQUFFLCtCQUErQixFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFDdEYsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFFMUUsNEJBQTRCO0FBQzVCLHVDQUF1QztBQUd2QztJQUE2QyxtREFBUztJQUR0RDtRQUFBLHFFQWtEQztRQWhEQyxVQUFJLEdBQUcsaUJBQWlCLENBQUM7UUFFekIsZUFBUyxHQUFHLGdDQUFnQyxDQUFDO1FBRTdDLGlCQUFXLEdBQUc7WUFDWixtREFBbUQ7WUFDbkQsMERBQTBEO1NBQzNELENBQUM7UUFFRixhQUFPLEdBQUc7WUFDUixJQUFJLEVBQUUsdUJBQXVCO1lBQzdCLE9BQU8sRUFBRSwwQkFBMEI7WUFDbkMsSUFBSSxFQUFFLDZCQUE2QjtZQUNuQyxZQUFZLEVBQUUsNkJBQTZCO1lBQzNDLE1BQU0sRUFBRSx1QkFBdUI7WUFDL0IsY0FBYyxFQUFFLDRCQUE0QjtZQUM1QyxRQUFRLEVBQUUseUJBQXlCO1lBQ25DLFVBQVUsRUFBRSwyQkFBMkI7WUFDdkMsUUFBUSxFQUFFLHlCQUF5QjtZQUNuQyxXQUFXLEVBQUUsNEJBQTRCO1lBQ3pDLGNBQWMsRUFBRSwrQkFBK0I7WUFDL0MsSUFBSSxFQUFFLDJCQUEyQjtZQUNqQyxhQUFhLEVBQUUsNEJBQTRCO1lBQzNDLElBQUksRUFBRSxxQkFBcUI7WUFDM0IsTUFBTSxFQUFFLHVCQUF1QjtZQUMvQixRQUFRLEVBQUUsc0JBQXNCO1lBQ2hDLFFBQVEsRUFBRSx5QkFBeUI7WUFDbkMsTUFBTSxFQUFFLHVCQUF1QjtZQUMvQixNQUFNLEVBQUUsdUJBQXVCO1lBQy9CLE1BQU0sRUFBRSx1QkFBdUI7WUFDL0IsT0FBTyxFQUFFLHdCQUF3QjtZQUNqQyxJQUFJLEVBQUUscUJBQXFCO1lBQzNCLElBQUksRUFBRSxzQkFBc0I7WUFDNUIsUUFBUSxFQUFFLHlCQUF5QjtZQUNuQyxVQUFVLEVBQUUsTUFBTTtZQUNsQixRQUFRLEVBQUUsUUFBUTtZQUNsQixJQUFJLEVBQUUsU0FBUztZQUNmLEtBQUssRUFBRSxNQUFNO1lBQ2IsaUJBQWlCLEVBQUUsU0FBUztZQUM1QixNQUFNLEVBQUUsTUFBTTtZQUNkLEtBQUssRUFBRSxNQUFNO1lBQ2IsT0FBTyxFQUFFLFFBQVE7WUFDakIsWUFBWSxFQUFFLGNBQWM7WUFDNUIsS0FBSyxFQUFFLFFBQVE7WUFDZixNQUFNLEVBQUUsUUFBUTtZQUNoQixTQUFTLEVBQUUsVUFBVTtZQUNyQixNQUFNLEVBQUUsU0FBUztTQUNsQixDQUFDOztJQUNKLENBQUM7SUFqRFksdUJBQXVCO1FBRG5DLFVBQVUsRUFBRTtPQUNBLHVCQUF1QixDQWlEbkM7SUFBRCw4QkFBQztDQUFBLEFBakRELENBQTZDLFNBQVMsR0FpRHJEO1NBakRZLHVCQUF1QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEZsZXhMYXlvdXRSb290Q29tcG9uZW50IH0gZnJvbSAnLi9mbGV4LWxheW91dC1yb290LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEZsZXhMYXlvdXRTZWN0aW9uQ29tcG9uZW50IH0gZnJvbSAnLi9mbGV4LWxheW91dC1zZWN0aW9uLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEZyYW1ld29yayB9IGZyb20gJy4uL2ZyYW1ld29yayc7XHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTWF0ZXJpYWxBZGRSZWZlcmVuY2VDb21wb25lbnQgfSBmcm9tICcuL21hdGVyaWFsLWFkZC1yZWZlcmVuY2UuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTWF0ZXJpYWxBdXRvQ29tcGxldGVDb21wb25lbnQgfSBmcm9tICcuL21hdGVyaWFsLWF1dG9jb21wbGV0ZS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNYXRlcmlhbEJ1dHRvbkNvbXBvbmVudCB9IGZyb20gJy4vbWF0ZXJpYWwtYnV0dG9uLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hdGVyaWFsQnV0dG9uR3JvdXBDb21wb25lbnQgfSBmcm9tICcuL21hdGVyaWFsLWJ1dHRvbi1ncm91cC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNYXRlcmlhbENoZWNrYm94Q29tcG9uZW50IH0gZnJvbSAnLi9tYXRlcmlhbC1jaGVja2JveC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNYXRlcmlhbENoZWNrYm94ZXNDb21wb25lbnQgfSBmcm9tICcuL21hdGVyaWFsLWNoZWNrYm94ZXMuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTWF0ZXJpYWxDaGlwbGlzdENvbXBvbmVudCB9IGZyb20gJy4vbWF0ZXJpYWwtY2hpcGxpc3QuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTWF0ZXJpYWxDb21wdXRhdGlvbkNvbXBvbmVudCB9IGZyb20gJy4vbWF0ZXJpYWwtY29tcHV0YXRpb24uY29tcG9uZW50JztcclxuaW1wb3J0IHsgTWF0ZXJpYWxEYXRlcGlja2VyQ29tcG9uZW50IH0gZnJvbSAnLi9tYXRlcmlhbC1kYXRlcGlja2VyLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hdGVyaWFsRGVzaWduRnJhbWV3b3JrQ29tcG9uZW50IH0gZnJvbSAnLi9tYXRlcmlhbC1kZXNpZ24tZnJhbWV3b3JrLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hdGVyaWFsRHluYW1pY09wdGlvbnNXaWRnZXQgfSBmcm9tICcuL21hdGVyaWFsLWR5bmFtaWMtb3B0aW9ucy5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNYXRlcmlhbEZpbGVDb21wb25lbnQgfSBmcm9tICcuL21hdGVyaWFsLWZpbGUuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTWF0ZXJpYWxJbnB1dENvbXBvbmVudCB9IGZyb20gJy4vbWF0ZXJpYWwtaW5wdXQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTWF0ZXJpYWxOdW1iZXJDb21wb25lbnQgfSBmcm9tICcuL21hdGVyaWFsLW51bWJlci5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNYXRlcmlhbE9uZU9mQ29tcG9uZW50IH0gZnJvbSAnLi9tYXRlcmlhbC1vbmUtb2YuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTWF0ZXJpYWxSYWRpb3NDb21wb25lbnQgfSBmcm9tICcuL21hdGVyaWFsLXJhZGlvcy5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNYXRlcmlhbFNlbGVjdENvbXBvbmVudCB9IGZyb20gJy4vbWF0ZXJpYWwtc2VsZWN0LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hdGVyaWFsU2xpZGVyQ29tcG9uZW50IH0gZnJvbSAnLi9tYXRlcmlhbC1zbGlkZXIuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTWF0ZXJpYWxTdGVwcGVyQ29tcG9uZW50IH0gZnJvbSAnLi9tYXRlcmlhbC1zdGVwcGVyLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hdGVyaWFsVGFic0NvbXBvbmVudCB9IGZyb20gJy4vbWF0ZXJpYWwtdGFicy5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNYXRlcmlhbFRleHRhcmVhQ29tcG9uZW50IH0gZnJvbSAnLi9tYXRlcmlhbC10ZXh0YXJlYS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNYXRlcmlhbE11bHRpVGV4dEZpZWxkQ29tcG9uZW50IH0gZnJvbSAnLi9tYXRlcmlhbC1tdWx0aXRleHRmaWVsZC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNYXRlcmlhbFBhc3N3b3JkQ29tcG9uZW50IH0gZnJvbSAnLi9tYXRlcmlhbC1wYXNzd29yZC5jb21wb25lbnQnO1xyXG5cclxuLy8gTWF0ZXJpYWwgRGVzaWduIEZyYW1ld29ya1xyXG4vLyBodHRwczovL2dpdGh1Yi5jb20vYW5ndWxhci9tYXRlcmlhbDJcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIE1hdGVyaWFsRGVzaWduRnJhbWV3b3JrIGV4dGVuZHMgRnJhbWV3b3JrIHtcclxuICBuYW1lID0gJ21hdGVyaWFsLWRlc2lnbic7XHJcblxyXG4gIGZyYW1ld29yayA9IE1hdGVyaWFsRGVzaWduRnJhbWV3b3JrQ29tcG9uZW50O1xyXG5cclxuICBzdHlsZXNoZWV0cyA9IFtcclxuICAgICcvL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2ljb24/ZmFtaWx5PU1hdGVyaWFsK0ljb25zJyxcclxuICAgICcvL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2Nzcz9mYW1pbHk9Um9ib3RvOjMwMCw0MDAsNTAwLDcwMCdcclxuICBdO1xyXG5cclxuICB3aWRnZXRzID0ge1xyXG4gICAgcm9vdDogRmxleExheW91dFJvb3RDb21wb25lbnQsXHJcbiAgICBzZWN0aW9uOiBGbGV4TGF5b3V0U2VjdGlvbkNvbXBvbmVudCxcclxuICAgICRyZWY6IE1hdGVyaWFsQWRkUmVmZXJlbmNlQ29tcG9uZW50LFxyXG4gICAgYXV0b2NvbXBsZXRlOiBNYXRlcmlhbEF1dG9Db21wbGV0ZUNvbXBvbmVudCxcclxuICAgIGJ1dHRvbjogTWF0ZXJpYWxCdXR0b25Db21wb25lbnQsXHJcbiAgICAnYnV0dG9uLWdyb3VwJzogTWF0ZXJpYWxCdXR0b25Hcm91cENvbXBvbmVudCxcclxuICAgIGNoZWNrYm94OiBNYXRlcmlhbENoZWNrYm94Q29tcG9uZW50LFxyXG4gICAgY2hlY2tib3hlczogTWF0ZXJpYWxDaGVja2JveGVzQ29tcG9uZW50LFxyXG4gICAgY2hpcGxpc3Q6IE1hdGVyaWFsQ2hpcGxpc3RDb21wb25lbnQsXHJcbiAgICBjb21wdXRhdGlvbjogTWF0ZXJpYWxDb21wdXRhdGlvbkNvbXBvbmVudCxcclxuICAgIG11bHRpdGV4dGZpZWxkOiBNYXRlcmlhbE11bHRpVGV4dEZpZWxkQ29tcG9uZW50LFxyXG4gICAgZGF0ZTogTWF0ZXJpYWxEYXRlcGlja2VyQ29tcG9uZW50LFxyXG4gICAgZHluYW1pY29wdGlvbjogTWF0ZXJpYWxEeW5hbWljT3B0aW9uc1dpZGdldCxcclxuICAgIGZpbGU6IE1hdGVyaWFsRmlsZUNvbXBvbmVudCxcclxuICAgIG51bWJlcjogTWF0ZXJpYWxOdW1iZXJDb21wb25lbnQsXHJcbiAgICAnb25lLW9mJzogTWF0ZXJpYWxPbmVPZkNvbXBvbmVudCxcclxuICAgIHBhc3N3b3JkOiBNYXRlcmlhbFBhc3N3b3JkQ29tcG9uZW50LFxyXG4gICAgcmFkaW9zOiBNYXRlcmlhbFJhZGlvc0NvbXBvbmVudCxcclxuICAgIHNlbGVjdDogTWF0ZXJpYWxTZWxlY3RDb21wb25lbnQsXHJcbiAgICBzbGlkZXI6IE1hdGVyaWFsU2xpZGVyQ29tcG9uZW50LFxyXG4gICAgc3RlcHBlcjogTWF0ZXJpYWxTdGVwcGVyQ29tcG9uZW50LFxyXG4gICAgdGFiczogTWF0ZXJpYWxUYWJzQ29tcG9uZW50LFxyXG4gICAgdGV4dDogTWF0ZXJpYWxJbnB1dENvbXBvbmVudCxcclxuICAgIHRleHRhcmVhOiBNYXRlcmlhbFRleHRhcmVhQ29tcG9uZW50LFxyXG4gICAgJ2FsdC1kYXRlJzogJ2RhdGUnLFxyXG4gICAgJ2FueS1vZic6ICdvbmUtb2YnLFxyXG4gICAgY2FyZDogJ3NlY3Rpb24nLFxyXG4gICAgY29sb3I6ICd0ZXh0JyxcclxuICAgICdleHBhbnNpb24tcGFuZWwnOiAnc2VjdGlvbicsXHJcbiAgICBoaWRkZW46ICdub25lJyxcclxuICAgIGltYWdlOiAnbm9uZScsXHJcbiAgICBpbnRlZ2VyOiAnbnVtYmVyJyxcclxuICAgIHJhZGlvYnV0dG9uczogJ2J1dHRvbi1ncm91cCcsXHJcbiAgICByYW5nZTogJ3NsaWRlcicsXHJcbiAgICBzdWJtaXQ6ICdidXR0b24nLFxyXG4gICAgdGFnc2lucHV0OiAnY2hpcGxpc3QnLFxyXG4gICAgd2l6YXJkOiAnc3RlcHBlcidcclxuICB9O1xyXG59XHJcbiJdfQ==