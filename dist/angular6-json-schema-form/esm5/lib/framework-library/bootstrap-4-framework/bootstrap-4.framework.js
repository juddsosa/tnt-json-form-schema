import * as tslib_1 from "tslib";
import { Bootstrap4FrameworkComponent } from './bootstrap-4-framework.component';
import { Framework } from '../framework';
import { Injectable } from '@angular/core';
// Bootstrap 4 Framework
// https://github.com/ng-bootstrap/ng-bootstrap
var Bootstrap4Framework = /** @class */ (function (_super) {
    tslib_1.__extends(Bootstrap4Framework, _super);
    function Bootstrap4Framework() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.name = 'bootstrap-4';
        _this.framework = Bootstrap4FrameworkComponent;
        _this.stylesheets = [
            '//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css'
        ];
        _this.scripts = [
            '//code.jquery.com/jquery-3.2.1.slim.min.js',
            '//cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js',
            '//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js',
        ];
        return _this;
    }
    Bootstrap4Framework = tslib_1.__decorate([
        Injectable()
    ], Bootstrap4Framework);
    return Bootstrap4Framework;
}(Framework));
export { Bootstrap4Framework };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0cmFwLTQuZnJhbWV3b3JrLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYW5ndWxhcjYtanNvbi1zY2hlbWEtZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9mcmFtZXdvcmstbGlicmFyeS9ib290c3RyYXAtNC1mcmFtZXdvcmsvYm9vdHN0cmFwLTQuZnJhbWV3b3JrLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsNEJBQTRCLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUNqRixPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFM0Msd0JBQXdCO0FBQ3hCLCtDQUErQztBQUcvQztJQUF5QywrQ0FBUztJQURsRDtRQUFBLHFFQWVDO1FBYkMsVUFBSSxHQUFHLGFBQWEsQ0FBQztRQUVyQixlQUFTLEdBQUcsNEJBQTRCLENBQUM7UUFFekMsaUJBQVcsR0FBRztZQUNaLHdFQUF3RTtTQUN6RSxDQUFDO1FBRUYsYUFBTyxHQUFHO1lBQ1IsNENBQTRDO1lBQzVDLHFFQUFxRTtZQUNyRSxzRUFBc0U7U0FDdkUsQ0FBQzs7SUFDSixDQUFDO0lBZFksbUJBQW1CO1FBRC9CLFVBQVUsRUFBRTtPQUNBLG1CQUFtQixDQWMvQjtJQUFELDBCQUFDO0NBQUEsQUFkRCxDQUF5QyxTQUFTLEdBY2pEO1NBZFksbUJBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQm9vdHN0cmFwNEZyYW1ld29ya0NvbXBvbmVudCB9IGZyb20gJy4vYm9vdHN0cmFwLTQtZnJhbWV3b3JrLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEZyYW1ld29yayB9IGZyb20gJy4uL2ZyYW1ld29yayc7XHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbi8vIEJvb3RzdHJhcCA0IEZyYW1ld29ya1xyXG4vLyBodHRwczovL2dpdGh1Yi5jb20vbmctYm9vdHN0cmFwL25nLWJvb3RzdHJhcFxyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgQm9vdHN0cmFwNEZyYW1ld29yayBleHRlbmRzIEZyYW1ld29yayB7XHJcbiAgbmFtZSA9ICdib290c3RyYXAtNCc7XHJcblxyXG4gIGZyYW1ld29yayA9IEJvb3RzdHJhcDRGcmFtZXdvcmtDb21wb25lbnQ7XHJcblxyXG4gIHN0eWxlc2hlZXRzID0gW1xyXG4gICAgJy8vbWF4Y2RuLmJvb3RzdHJhcGNkbi5jb20vYm9vdHN0cmFwLzQuMC4wLWJldGEuMi9jc3MvYm9vdHN0cmFwLm1pbi5jc3MnXHJcbiAgXTtcclxuXHJcbiAgc2NyaXB0cyA9IFtcclxuICAgICcvL2NvZGUuanF1ZXJ5LmNvbS9qcXVlcnktMy4yLjEuc2xpbS5taW4uanMnLFxyXG4gICAgJy8vY2RuanMuY2xvdWRmbGFyZS5jb20vYWpheC9saWJzL3BvcHBlci5qcy8xLjEyLjMvdW1kL3BvcHBlci5taW4uanMnLFxyXG4gICAgJy8vbWF4Y2RuLmJvb3RzdHJhcGNkbi5jb20vYm9vdHN0cmFwLzQuMC4wLWJldGEuMi9qcy9ib290c3RyYXAubWluLmpzJyxcclxuICBdO1xyXG59XHJcbiJdfQ==