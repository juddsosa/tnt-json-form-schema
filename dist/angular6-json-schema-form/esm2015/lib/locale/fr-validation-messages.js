export const frValidationMessages = {
    required: 'Est obligatoire.',
    minLength: 'Doit avoir minimum {{minimumLength}} caractères (actuellement: {{currentLength}})',
    maxLength: 'Doit avoir maximum {{maximumLength}} caractères (actuellement: {{currentLength}})',
    pattern: 'Doit respecter: {{requiredPattern}}',
    format: function (error) {
        switch (error.requiredFormat) {
            case 'date':
                return 'Doit être une date, tel que "2000-12-31"';
            case 'time':
                return 'Doit être une heure, tel que "16:20" ou "03:14:15.9265"';
            case 'date-time':
                return 'Doit être une date et une heure, tel que "2000-03-14T01:59" ou "2000-03-14T01:59:26.535Z"';
            case 'email':
                return 'Doit être une adresse e-mail, tel que "name@example.com"';
            case 'hostname':
                return 'Doit être un nom de domaine, tel que "example.com"';
            case 'ipv4':
                return 'Doit être une adresse IPv4, tel que "127.0.0.1"';
            case 'ipv6':
                return 'Doit être une adresse IPv6, tel que "1234:5678:9ABC:DEF0:1234:5678:9ABC:DEF0"';
            // TODO: add examples for 'uri', 'uri-reference', and 'uri-template'
            // case 'uri': case 'uri-reference': case 'uri-template':
            case 'url':
                return 'Doit être une URL, tel que "http://www.example.com/page.html"';
            case 'uuid':
                return 'Doit être un UUID, tel que "12345678-9ABC-DEF0-1234-56789ABCDEF0"';
            case 'color':
                return 'Doit être une couleur, tel que "#FFFFFF" or "rgb(255, 255, 255)"';
            case 'json-pointer':
                return 'Doit être un JSON Pointer, tel que "/pointer/to/something"';
            case 'relative-json-pointer':
                return 'Doit être un relative JSON Pointer, tel que "2/pointer/to/something"';
            case 'regex':
                return 'Doit être une expression régulière, tel que "(1-)?\\d{3}-\\d{3}-\\d{4}"';
            default:
                return 'Doit être avoir le format correct: ' + error.requiredFormat;
        }
    },
    minimum: 'Doit être supérieur à {{minimumValue}}',
    exclusiveMinimum: 'Doit avoir minimum {{exclusiveMinimumValue}} charactères',
    maximum: 'Doit être inférieur à {{maximumValue}}',
    exclusiveMaximum: 'Doit avoir maximum {{exclusiveMaximumValue}} charactères',
    multipleOf: function (error) {
        if ((1 / error.multipleOfValue) % 10 === 0) {
            const decimals = Math.log10(1 / error.multipleOfValue);
            return `Doit comporter ${decimals} ou moins de decimales.`;
        }
        else {
            return `Doit être un multiple de ${error.multipleOfValue}.`;
        }
    },
    minProperties: 'Doit comporter au minimum {{minimumProperties}} éléments',
    maxProperties: 'Doit comporter au maximum {{maximumProperties}} éléments',
    minItems: 'Doit comporter au minimum {{minimumItems}} éléments',
    maxItems: 'Doit comporter au maximum {{minimumItems}} éléments',
    uniqueItems: 'Tous les éléments doivent être uniques',
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZnItdmFsaWRhdGlvbi1tZXNzYWdlcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FuZ3VsYXI2LWpzb24tc2NoZW1hLWZvcm0vIiwic291cmNlcyI6WyJsaWIvbG9jYWxlL2ZyLXZhbGlkYXRpb24tbWVzc2FnZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsTUFBTSxDQUFDLE1BQU0sb0JBQW9CLEdBQVE7SUFDdkMsUUFBUSxFQUFFLGtCQUFrQjtJQUM1QixTQUFTLEVBQUUsbUZBQW1GO0lBQzlGLFNBQVMsRUFBRSxtRkFBbUY7SUFDOUYsT0FBTyxFQUFFLHFDQUFxQztJQUM5QyxNQUFNLEVBQUUsVUFBVSxLQUFLO1FBQ3JCLFFBQVEsS0FBSyxDQUFDLGNBQWMsRUFBRTtZQUM1QixLQUFLLE1BQU07Z0JBQ1QsT0FBTywwQ0FBMEMsQ0FBQztZQUNwRCxLQUFLLE1BQU07Z0JBQ1QsT0FBTyx5REFBeUQsQ0FBQztZQUNuRSxLQUFLLFdBQVc7Z0JBQ2QsT0FBTywyRkFBMkYsQ0FBQztZQUNyRyxLQUFLLE9BQU87Z0JBQ1YsT0FBTywwREFBMEQsQ0FBQztZQUNwRSxLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxvREFBb0QsQ0FBQztZQUM5RCxLQUFLLE1BQU07Z0JBQ1QsT0FBTyxpREFBaUQsQ0FBQztZQUMzRCxLQUFLLE1BQU07Z0JBQ1QsT0FBTywrRUFBK0UsQ0FBQztZQUN6RixvRUFBb0U7WUFDcEUseURBQXlEO1lBQ3pELEtBQUssS0FBSztnQkFDUixPQUFPLCtEQUErRCxDQUFDO1lBQ3pFLEtBQUssTUFBTTtnQkFDVCxPQUFPLG1FQUFtRSxDQUFDO1lBQzdFLEtBQUssT0FBTztnQkFDVixPQUFPLGtFQUFrRSxDQUFDO1lBQzVFLEtBQUssY0FBYztnQkFDakIsT0FBTyw0REFBNEQsQ0FBQztZQUN0RSxLQUFLLHVCQUF1QjtnQkFDMUIsT0FBTyxzRUFBc0UsQ0FBQztZQUNoRixLQUFLLE9BQU87Z0JBQ1YsT0FBTyx5RUFBeUUsQ0FBQztZQUNuRjtnQkFDRSxPQUFPLHFDQUFxQyxHQUFHLEtBQUssQ0FBQyxjQUFjLENBQUM7U0FDdkU7SUFDSCxDQUFDO0lBQ0QsT0FBTyxFQUFFLHdDQUF3QztJQUNqRCxnQkFBZ0IsRUFBRSwwREFBMEQ7SUFDNUUsT0FBTyxFQUFFLHdDQUF3QztJQUNqRCxnQkFBZ0IsRUFBRSwwREFBMEQ7SUFDNUUsVUFBVSxFQUFFLFVBQVUsS0FBSztRQUN6QixJQUFJLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQyxlQUFlLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxFQUFFO1lBQzFDLE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUN2RCxPQUFPLGtCQUFrQixRQUFRLHlCQUF5QixDQUFDO1NBQzVEO2FBQU07WUFDTCxPQUFPLDRCQUE0QixLQUFLLENBQUMsZUFBZSxHQUFHLENBQUM7U0FDN0Q7SUFDSCxDQUFDO0lBQ0QsYUFBYSxFQUFFLDBEQUEwRDtJQUN6RSxhQUFhLEVBQUUsMERBQTBEO0lBQ3pFLFFBQVEsRUFBRSxxREFBcUQ7SUFDL0QsUUFBUSxFQUFFLHFEQUFxRDtJQUMvRCxXQUFXLEVBQUUsd0NBQXdDO0NBRXRELENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY29uc3QgZnJWYWxpZGF0aW9uTWVzc2FnZXM6IGFueSA9IHsgLy8gRnJlbmNoIGVycm9yIG1lc3NhZ2VzXHJcbiAgcmVxdWlyZWQ6ICdFc3Qgb2JsaWdhdG9pcmUuJyxcclxuICBtaW5MZW5ndGg6ICdEb2l0IGF2b2lyIG1pbmltdW0ge3ttaW5pbXVtTGVuZ3RofX0gY2FyYWN0w6hyZXMgKGFjdHVlbGxlbWVudDoge3tjdXJyZW50TGVuZ3RofX0pJyxcclxuICBtYXhMZW5ndGg6ICdEb2l0IGF2b2lyIG1heGltdW0ge3ttYXhpbXVtTGVuZ3RofX0gY2FyYWN0w6hyZXMgKGFjdHVlbGxlbWVudDoge3tjdXJyZW50TGVuZ3RofX0pJyxcclxuICBwYXR0ZXJuOiAnRG9pdCByZXNwZWN0ZXI6IHt7cmVxdWlyZWRQYXR0ZXJufX0nLFxyXG4gIGZvcm1hdDogZnVuY3Rpb24gKGVycm9yKSB7XHJcbiAgICBzd2l0Y2ggKGVycm9yLnJlcXVpcmVkRm9ybWF0KSB7XHJcbiAgICAgIGNhc2UgJ2RhdGUnOlxyXG4gICAgICAgIHJldHVybiAnRG9pdCDDqnRyZSB1bmUgZGF0ZSwgdGVsIHF1ZSBcIjIwMDAtMTItMzFcIic7XHJcbiAgICAgIGNhc2UgJ3RpbWUnOlxyXG4gICAgICAgIHJldHVybiAnRG9pdCDDqnRyZSB1bmUgaGV1cmUsIHRlbCBxdWUgXCIxNjoyMFwiIG91IFwiMDM6MTQ6MTUuOTI2NVwiJztcclxuICAgICAgY2FzZSAnZGF0ZS10aW1lJzpcclxuICAgICAgICByZXR1cm4gJ0RvaXQgw6p0cmUgdW5lIGRhdGUgZXQgdW5lIGhldXJlLCB0ZWwgcXVlIFwiMjAwMC0wMy0xNFQwMTo1OVwiIG91IFwiMjAwMC0wMy0xNFQwMTo1OToyNi41MzVaXCInO1xyXG4gICAgICBjYXNlICdlbWFpbCc6XHJcbiAgICAgICAgcmV0dXJuICdEb2l0IMOqdHJlIHVuZSBhZHJlc3NlIGUtbWFpbCwgdGVsIHF1ZSBcIm5hbWVAZXhhbXBsZS5jb21cIic7XHJcbiAgICAgIGNhc2UgJ2hvc3RuYW1lJzpcclxuICAgICAgICByZXR1cm4gJ0RvaXQgw6p0cmUgdW4gbm9tIGRlIGRvbWFpbmUsIHRlbCBxdWUgXCJleGFtcGxlLmNvbVwiJztcclxuICAgICAgY2FzZSAnaXB2NCc6XHJcbiAgICAgICAgcmV0dXJuICdEb2l0IMOqdHJlIHVuZSBhZHJlc3NlIElQdjQsIHRlbCBxdWUgXCIxMjcuMC4wLjFcIic7XHJcbiAgICAgIGNhc2UgJ2lwdjYnOlxyXG4gICAgICAgIHJldHVybiAnRG9pdCDDqnRyZSB1bmUgYWRyZXNzZSBJUHY2LCB0ZWwgcXVlIFwiMTIzNDo1Njc4OjlBQkM6REVGMDoxMjM0OjU2Nzg6OUFCQzpERUYwXCInO1xyXG4gICAgICAvLyBUT0RPOiBhZGQgZXhhbXBsZXMgZm9yICd1cmknLCAndXJpLXJlZmVyZW5jZScsIGFuZCAndXJpLXRlbXBsYXRlJ1xyXG4gICAgICAvLyBjYXNlICd1cmknOiBjYXNlICd1cmktcmVmZXJlbmNlJzogY2FzZSAndXJpLXRlbXBsYXRlJzpcclxuICAgICAgY2FzZSAndXJsJzpcclxuICAgICAgICByZXR1cm4gJ0RvaXQgw6p0cmUgdW5lIFVSTCwgdGVsIHF1ZSBcImh0dHA6Ly93d3cuZXhhbXBsZS5jb20vcGFnZS5odG1sXCInO1xyXG4gICAgICBjYXNlICd1dWlkJzpcclxuICAgICAgICByZXR1cm4gJ0RvaXQgw6p0cmUgdW4gVVVJRCwgdGVsIHF1ZSBcIjEyMzQ1Njc4LTlBQkMtREVGMC0xMjM0LTU2Nzg5QUJDREVGMFwiJztcclxuICAgICAgY2FzZSAnY29sb3InOlxyXG4gICAgICAgIHJldHVybiAnRG9pdCDDqnRyZSB1bmUgY291bGV1ciwgdGVsIHF1ZSBcIiNGRkZGRkZcIiBvciBcInJnYigyNTUsIDI1NSwgMjU1KVwiJztcclxuICAgICAgY2FzZSAnanNvbi1wb2ludGVyJzpcclxuICAgICAgICByZXR1cm4gJ0RvaXQgw6p0cmUgdW4gSlNPTiBQb2ludGVyLCB0ZWwgcXVlIFwiL3BvaW50ZXIvdG8vc29tZXRoaW5nXCInO1xyXG4gICAgICBjYXNlICdyZWxhdGl2ZS1qc29uLXBvaW50ZXInOlxyXG4gICAgICAgIHJldHVybiAnRG9pdCDDqnRyZSB1biByZWxhdGl2ZSBKU09OIFBvaW50ZXIsIHRlbCBxdWUgXCIyL3BvaW50ZXIvdG8vc29tZXRoaW5nXCInO1xyXG4gICAgICBjYXNlICdyZWdleCc6XHJcbiAgICAgICAgcmV0dXJuICdEb2l0IMOqdHJlIHVuZSBleHByZXNzaW9uIHLDqWd1bGnDqHJlLCB0ZWwgcXVlIFwiKDEtKT9cXFxcZHszfS1cXFxcZHszfS1cXFxcZHs0fVwiJztcclxuICAgICAgZGVmYXVsdDpcclxuICAgICAgICByZXR1cm4gJ0RvaXQgw6p0cmUgYXZvaXIgbGUgZm9ybWF0IGNvcnJlY3Q6ICcgKyBlcnJvci5yZXF1aXJlZEZvcm1hdDtcclxuICAgIH1cclxuICB9LFxyXG4gIG1pbmltdW06ICdEb2l0IMOqdHJlIHN1cMOpcmlldXIgw6Age3ttaW5pbXVtVmFsdWV9fScsXHJcbiAgZXhjbHVzaXZlTWluaW11bTogJ0RvaXQgYXZvaXIgbWluaW11bSB7e2V4Y2x1c2l2ZU1pbmltdW1WYWx1ZX19IGNoYXJhY3TDqHJlcycsXHJcbiAgbWF4aW11bTogJ0RvaXQgw6p0cmUgaW5mw6lyaWV1ciDDoCB7e21heGltdW1WYWx1ZX19JyxcclxuICBleGNsdXNpdmVNYXhpbXVtOiAnRG9pdCBhdm9pciBtYXhpbXVtIHt7ZXhjbHVzaXZlTWF4aW11bVZhbHVlfX0gY2hhcmFjdMOocmVzJyxcclxuICBtdWx0aXBsZU9mOiBmdW5jdGlvbiAoZXJyb3IpIHtcclxuICAgIGlmICgoMSAvIGVycm9yLm11bHRpcGxlT2ZWYWx1ZSkgJSAxMCA9PT0gMCkge1xyXG4gICAgICBjb25zdCBkZWNpbWFscyA9IE1hdGgubG9nMTAoMSAvIGVycm9yLm11bHRpcGxlT2ZWYWx1ZSk7XHJcbiAgICAgIHJldHVybiBgRG9pdCBjb21wb3J0ZXIgJHtkZWNpbWFsc30gb3UgbW9pbnMgZGUgZGVjaW1hbGVzLmA7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gYERvaXQgw6p0cmUgdW4gbXVsdGlwbGUgZGUgJHtlcnJvci5tdWx0aXBsZU9mVmFsdWV9LmA7XHJcbiAgICB9XHJcbiAgfSxcclxuICBtaW5Qcm9wZXJ0aWVzOiAnRG9pdCBjb21wb3J0ZXIgYXUgbWluaW11bSB7e21pbmltdW1Qcm9wZXJ0aWVzfX0gw6lsw6ltZW50cycsXHJcbiAgbWF4UHJvcGVydGllczogJ0RvaXQgY29tcG9ydGVyIGF1IG1heGltdW0ge3ttYXhpbXVtUHJvcGVydGllc319IMOpbMOpbWVudHMnLFxyXG4gIG1pbkl0ZW1zOiAnRG9pdCBjb21wb3J0ZXIgYXUgbWluaW11bSB7e21pbmltdW1JdGVtc319IMOpbMOpbWVudHMnLFxyXG4gIG1heEl0ZW1zOiAnRG9pdCBjb21wb3J0ZXIgYXUgbWF4aW11bSB7e21pbmltdW1JdGVtc319IMOpbMOpbWVudHMnLFxyXG4gIHVuaXF1ZUl0ZW1zOiAnVG91cyBsZXMgw6lsw6ltZW50cyBkb2l2ZW50IMOqdHJlIHVuaXF1ZXMnLFxyXG4gIC8vIE5vdGU6IE5vIGRlZmF1bHQgZXJyb3IgbWVzc2FnZXMgZm9yICd0eXBlJywgJ2NvbnN0JywgJ2VudW0nLCBvciAnZGVwZW5kZW5jaWVzJ1xyXG59O1xyXG4iXX0=