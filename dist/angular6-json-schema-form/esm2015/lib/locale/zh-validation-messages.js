export const zhValidationMessages = {
    required: '必填字段.',
    minLength: '字符长度必须大于或者等于 {{minimumLength}} (当前长度: {{currentLength}})',
    maxLength: '字符长度必须小于或者等于 {{maximumLength}} (当前长度: {{currentLength}})',
    pattern: '必须匹配正则表达式: {{requiredPattern}}',
    format: function (error) {
        switch (error.requiredFormat) {
            case 'date':
                return '必须为日期格式, 比如 "2000-12-31"';
            case 'time':
                return '必须为时间格式, 比如 "16:20" 或者 "03:14:15.9265"';
            case 'date-time':
                return '必须为日期时间格式, 比如 "2000-03-14T01:59" 或者 "2000-03-14T01:59:26.535Z"';
            case 'email':
                return '必须为邮箱地址, 比如 "name@example.com"';
            case 'hostname':
                return '必须为主机名, 比如 "example.com"';
            case 'ipv4':
                return '必须为 IPv4 地址, 比如 "127.0.0.1"';
            case 'ipv6':
                return '必须为 IPv6 地址, 比如 "1234:5678:9ABC:DEF0:1234:5678:9ABC:DEF0"';
            // TODO: add examples for 'uri', 'uri-reference', and 'uri-template'
            // case 'uri': case 'uri-reference': case 'uri-template':
            case 'url':
                return '必须为 url, 比如 "http://www.example.com/page.html"';
            case 'uuid':
                return '必须为 uuid, 比如 "12345678-9ABC-DEF0-1234-56789ABCDEF0"';
            case 'color':
                return '必须为颜色值, 比如 "#FFFFFF" 或者 "rgb(255, 255, 255)"';
            case 'json-pointer':
                return '必须为 JSON Pointer, 比如 "/pointer/to/something"';
            case 'relative-json-pointer':
                return '必须为相对的 JSON Pointer, 比如 "2/pointer/to/something"';
            case 'regex':
                return '必须为正则表达式, 比如 "(1-)?\\d{3}-\\d{3}-\\d{4}"';
            default:
                return '必须为格式正确的 ' + error.requiredFormat;
        }
    },
    minimum: '必须大于或者等于最小值: {{minimumValue}}',
    exclusiveMinimum: '必须大于最小值: {{exclusiveMinimumValue}}',
    maximum: '必须小于或者等于最大值: {{maximumValue}}',
    exclusiveMaximum: '必须小于最大值: {{exclusiveMaximumValue}}',
    multipleOf: function (error) {
        if ((1 / error.multipleOfValue) % 10 === 0) {
            const decimals = Math.log10(1 / error.multipleOfValue);
            return `必须有 ${decimals} 位或更少的小数位`;
        }
        else {
            return `必须为 ${error.multipleOfValue} 的倍数`;
        }
    },
    minProperties: '项目数必须大于或者等于 {{minimumProperties}} (当前项目数: {{currentProperties}})',
    maxProperties: '项目数必须小于或者等于 {{maximumProperties}} (当前项目数: {{currentProperties}})',
    minItems: '项目数必须大于或者等于 {{minimumItems}} (当前项目数: {{currentItems}})',
    maxItems: '项目数必须小于或者等于 {{maximumItems}} (当前项目数: {{currentItems}})',
    uniqueItems: '所有项目必须是唯一的',
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiemgtdmFsaWRhdGlvbi1tZXNzYWdlcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FuZ3VsYXI2LWpzb24tc2NoZW1hLWZvcm0vIiwic291cmNlcyI6WyJsaWIvbG9jYWxlL3poLXZhbGlkYXRpb24tbWVzc2FnZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsTUFBTSxDQUFDLE1BQU0sb0JBQW9CLEdBQVE7SUFDdkMsUUFBUSxFQUFFLE9BQU87SUFDakIsU0FBUyxFQUFFLDBEQUEwRDtJQUNyRSxTQUFTLEVBQUUsMERBQTBEO0lBQ3JFLE9BQU8sRUFBRSxnQ0FBZ0M7SUFDekMsTUFBTSxFQUFFLFVBQVUsS0FBSztRQUNyQixRQUFRLEtBQUssQ0FBQyxjQUFjLEVBQUU7WUFDNUIsS0FBSyxNQUFNO2dCQUNULE9BQU8sMEJBQTBCLENBQUM7WUFDcEMsS0FBSyxNQUFNO2dCQUNULE9BQU8sd0NBQXdDLENBQUM7WUFDbEQsS0FBSyxXQUFXO2dCQUNkLE9BQU8sZ0VBQWdFLENBQUM7WUFDMUUsS0FBSyxPQUFPO2dCQUNWLE9BQU8sZ0NBQWdDLENBQUM7WUFDMUMsS0FBSyxVQUFVO2dCQUNiLE9BQU8sMEJBQTBCLENBQUM7WUFDcEMsS0FBSyxNQUFNO2dCQUNULE9BQU8sNkJBQTZCLENBQUM7WUFDdkMsS0FBSyxNQUFNO2dCQUNULE9BQU8sMkRBQTJELENBQUM7WUFDckUsb0VBQW9FO1lBQ3BFLHlEQUF5RDtZQUN6RCxLQUFLLEtBQUs7Z0JBQ1IsT0FBTyxnREFBZ0QsQ0FBQztZQUMxRCxLQUFLLE1BQU07Z0JBQ1QsT0FBTyxxREFBcUQsQ0FBQztZQUMvRCxLQUFLLE9BQU87Z0JBQ1YsT0FBTyw4Q0FBOEMsQ0FBQztZQUN4RCxLQUFLLGNBQWM7Z0JBQ2pCLE9BQU8sOENBQThDLENBQUM7WUFDeEQsS0FBSyx1QkFBdUI7Z0JBQzFCLE9BQU8sa0RBQWtELENBQUM7WUFDNUQsS0FBSyxPQUFPO2dCQUNWLE9BQU8sMENBQTBDLENBQUM7WUFDcEQ7Z0JBQ0UsT0FBTyxXQUFXLEdBQUcsS0FBSyxDQUFDLGNBQWMsQ0FBQztTQUM3QztJQUNILENBQUM7SUFDRCxPQUFPLEVBQUUsK0JBQStCO0lBQ3hDLGdCQUFnQixFQUFFLG9DQUFvQztJQUN0RCxPQUFPLEVBQUUsK0JBQStCO0lBQ3hDLGdCQUFnQixFQUFFLG9DQUFvQztJQUN0RCxVQUFVLEVBQUUsVUFBVSxLQUFLO1FBQ3pCLElBQUksQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDLGVBQWUsQ0FBQyxHQUFHLEVBQUUsS0FBSyxDQUFDLEVBQUU7WUFDMUMsTUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQ3ZELE9BQU8sT0FBTyxRQUFRLFdBQVcsQ0FBQztTQUNuQzthQUFNO1lBQ0wsT0FBTyxPQUFPLEtBQUssQ0FBQyxlQUFlLE1BQU0sQ0FBQztTQUMzQztJQUNILENBQUM7SUFDRCxhQUFhLEVBQUUsa0VBQWtFO0lBQ2pGLGFBQWEsRUFBRSxrRUFBa0U7SUFDakYsUUFBUSxFQUFFLHdEQUF3RDtJQUNsRSxRQUFRLEVBQUUsd0RBQXdEO0lBQ2xFLFdBQVcsRUFBRSxZQUFZO0NBRTFCLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY29uc3QgemhWYWxpZGF0aW9uTWVzc2FnZXM6IGFueSA9IHsgLy8gQ2hpbmVzZSBlcnJvciBtZXNzYWdlc1xyXG4gIHJlcXVpcmVkOiAn5b+F5aGr5a2X5q61LicsXHJcbiAgbWluTGVuZ3RoOiAn5a2X56ym6ZW/5bqm5b+F6aG75aSn5LqO5oiW6ICF562J5LqOIHt7bWluaW11bUxlbmd0aH19ICjlvZPliY3plb/luqY6IHt7Y3VycmVudExlbmd0aH19KScsXHJcbiAgbWF4TGVuZ3RoOiAn5a2X56ym6ZW/5bqm5b+F6aG75bCP5LqO5oiW6ICF562J5LqOIHt7bWF4aW11bUxlbmd0aH19ICjlvZPliY3plb/luqY6IHt7Y3VycmVudExlbmd0aH19KScsXHJcbiAgcGF0dGVybjogJ+W/hemhu+WMuemFjeato+WImeihqOi+vuW8jzoge3tyZXF1aXJlZFBhdHRlcm59fScsXHJcbiAgZm9ybWF0OiBmdW5jdGlvbiAoZXJyb3IpIHtcclxuICAgIHN3aXRjaCAoZXJyb3IucmVxdWlyZWRGb3JtYXQpIHtcclxuICAgICAgY2FzZSAnZGF0ZSc6XHJcbiAgICAgICAgcmV0dXJuICflv4XpobvkuLrml6XmnJ/moLzlvI8sIOavlOWmgiBcIjIwMDAtMTItMzFcIic7XHJcbiAgICAgIGNhc2UgJ3RpbWUnOlxyXG4gICAgICAgIHJldHVybiAn5b+F6aG75Li65pe26Ze05qC85byPLCDmr5TlpoIgXCIxNjoyMFwiIOaIluiAhSBcIjAzOjE0OjE1LjkyNjVcIic7XHJcbiAgICAgIGNhc2UgJ2RhdGUtdGltZSc6XHJcbiAgICAgICAgcmV0dXJuICflv4XpobvkuLrml6XmnJ/ml7bpl7TmoLzlvI8sIOavlOWmgiBcIjIwMDAtMDMtMTRUMDE6NTlcIiDmiJbogIUgXCIyMDAwLTAzLTE0VDAxOjU5OjI2LjUzNVpcIic7XHJcbiAgICAgIGNhc2UgJ2VtYWlsJzpcclxuICAgICAgICByZXR1cm4gJ+W/hemhu+S4uumCrueuseWcsOWdgCwg5q+U5aaCIFwibmFtZUBleGFtcGxlLmNvbVwiJztcclxuICAgICAgY2FzZSAnaG9zdG5hbWUnOlxyXG4gICAgICAgIHJldHVybiAn5b+F6aG75Li65Li75py65ZCNLCDmr5TlpoIgXCJleGFtcGxlLmNvbVwiJztcclxuICAgICAgY2FzZSAnaXB2NCc6XHJcbiAgICAgICAgcmV0dXJuICflv4XpobvkuLogSVB2NCDlnLDlnYAsIOavlOWmgiBcIjEyNy4wLjAuMVwiJztcclxuICAgICAgY2FzZSAnaXB2Nic6XHJcbiAgICAgICAgcmV0dXJuICflv4XpobvkuLogSVB2NiDlnLDlnYAsIOavlOWmgiBcIjEyMzQ6NTY3ODo5QUJDOkRFRjA6MTIzNDo1Njc4OjlBQkM6REVGMFwiJztcclxuICAgICAgLy8gVE9ETzogYWRkIGV4YW1wbGVzIGZvciAndXJpJywgJ3VyaS1yZWZlcmVuY2UnLCBhbmQgJ3VyaS10ZW1wbGF0ZSdcclxuICAgICAgLy8gY2FzZSAndXJpJzogY2FzZSAndXJpLXJlZmVyZW5jZSc6IGNhc2UgJ3VyaS10ZW1wbGF0ZSc6XHJcbiAgICAgIGNhc2UgJ3VybCc6XHJcbiAgICAgICAgcmV0dXJuICflv4XpobvkuLogdXJsLCDmr5TlpoIgXCJodHRwOi8vd3d3LmV4YW1wbGUuY29tL3BhZ2UuaHRtbFwiJztcclxuICAgICAgY2FzZSAndXVpZCc6XHJcbiAgICAgICAgcmV0dXJuICflv4XpobvkuLogdXVpZCwg5q+U5aaCIFwiMTIzNDU2NzgtOUFCQy1ERUYwLTEyMzQtNTY3ODlBQkNERUYwXCInO1xyXG4gICAgICBjYXNlICdjb2xvcic6XHJcbiAgICAgICAgcmV0dXJuICflv4XpobvkuLrpopzoibLlgLwsIOavlOWmgiBcIiNGRkZGRkZcIiDmiJbogIUgXCJyZ2IoMjU1LCAyNTUsIDI1NSlcIic7XHJcbiAgICAgIGNhc2UgJ2pzb24tcG9pbnRlcic6XHJcbiAgICAgICAgcmV0dXJuICflv4XpobvkuLogSlNPTiBQb2ludGVyLCDmr5TlpoIgXCIvcG9pbnRlci90by9zb21ldGhpbmdcIic7XHJcbiAgICAgIGNhc2UgJ3JlbGF0aXZlLWpzb24tcG9pbnRlcic6XHJcbiAgICAgICAgcmV0dXJuICflv4XpobvkuLrnm7jlr7nnmoQgSlNPTiBQb2ludGVyLCDmr5TlpoIgXCIyL3BvaW50ZXIvdG8vc29tZXRoaW5nXCInO1xyXG4gICAgICBjYXNlICdyZWdleCc6XHJcbiAgICAgICAgcmV0dXJuICflv4XpobvkuLrmraPliJnooajovr7lvI8sIOavlOWmgiBcIigxLSk/XFxcXGR7M30tXFxcXGR7M30tXFxcXGR7NH1cIic7XHJcbiAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgcmV0dXJuICflv4XpobvkuLrmoLzlvI/mraPnoa7nmoQgJyArIGVycm9yLnJlcXVpcmVkRm9ybWF0O1xyXG4gICAgfVxyXG4gIH0sXHJcbiAgbWluaW11bTogJ+W/hemhu+Wkp+S6juaIluiAheetieS6juacgOWwj+WAvDoge3ttaW5pbXVtVmFsdWV9fScsXHJcbiAgZXhjbHVzaXZlTWluaW11bTogJ+W/hemhu+Wkp+S6juacgOWwj+WAvDoge3tleGNsdXNpdmVNaW5pbXVtVmFsdWV9fScsXHJcbiAgbWF4aW11bTogJ+W/hemhu+Wwj+S6juaIluiAheetieS6juacgOWkp+WAvDoge3ttYXhpbXVtVmFsdWV9fScsXHJcbiAgZXhjbHVzaXZlTWF4aW11bTogJ+W/hemhu+Wwj+S6juacgOWkp+WAvDoge3tleGNsdXNpdmVNYXhpbXVtVmFsdWV9fScsXHJcbiAgbXVsdGlwbGVPZjogZnVuY3Rpb24gKGVycm9yKSB7XHJcbiAgICBpZiAoKDEgLyBlcnJvci5tdWx0aXBsZU9mVmFsdWUpICUgMTAgPT09IDApIHtcclxuICAgICAgY29uc3QgZGVjaW1hbHMgPSBNYXRoLmxvZzEwKDEgLyBlcnJvci5tdWx0aXBsZU9mVmFsdWUpO1xyXG4gICAgICByZXR1cm4gYOW/hemhu+aciSAke2RlY2ltYWxzfSDkvY3miJbmm7TlsJHnmoTlsI/mlbDkvY1gO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIGDlv4XpobvkuLogJHtlcnJvci5tdWx0aXBsZU9mVmFsdWV9IOeahOWAjeaVsGA7XHJcbiAgICB9XHJcbiAgfSxcclxuICBtaW5Qcm9wZXJ0aWVzOiAn6aG555uu5pWw5b+F6aG75aSn5LqO5oiW6ICF562J5LqOIHt7bWluaW11bVByb3BlcnRpZXN9fSAo5b2T5YmN6aG555uu5pWwOiB7e2N1cnJlbnRQcm9wZXJ0aWVzfX0pJyxcclxuICBtYXhQcm9wZXJ0aWVzOiAn6aG555uu5pWw5b+F6aG75bCP5LqO5oiW6ICF562J5LqOIHt7bWF4aW11bVByb3BlcnRpZXN9fSAo5b2T5YmN6aG555uu5pWwOiB7e2N1cnJlbnRQcm9wZXJ0aWVzfX0pJyxcclxuICBtaW5JdGVtczogJ+mhueebruaVsOW/hemhu+Wkp+S6juaIluiAheetieS6jiB7e21pbmltdW1JdGVtc319ICjlvZPliY3pobnnm67mlbA6IHt7Y3VycmVudEl0ZW1zfX0pJyxcclxuICBtYXhJdGVtczogJ+mhueebruaVsOW/hemhu+Wwj+S6juaIluiAheetieS6jiB7e21heGltdW1JdGVtc319ICjlvZPliY3pobnnm67mlbA6IHt7Y3VycmVudEl0ZW1zfX0pJyxcclxuICB1bmlxdWVJdGVtczogJ+aJgOaciemhueebruW/hemhu+aYr+WUr+S4gOeahCcsXHJcbiAgLy8gTm90ZTogTm8gZGVmYXVsdCBlcnJvciBtZXNzYWdlcyBmb3IgJ3R5cGUnLCAnY29uc3QnLCAnZW51bScsIG9yICdkZXBlbmRlbmNpZXMnXHJcbn07XHJcbiJdfQ==