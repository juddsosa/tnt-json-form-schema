import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
let Framework = class Framework {
    constructor() {
        this.widgets = {};
        this.stylesheets = [];
        this.scripts = [];
    }
};
Framework = tslib_1.__decorate([
    Injectable()
], Framework);
export { Framework };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZnJhbWV3b3JrLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYW5ndWxhcjYtanNvbi1zY2hlbWEtZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9mcmFtZXdvcmstbGlicmFyeS9mcmFtZXdvcmsudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFHM0MsSUFBYSxTQUFTLEdBQXRCLE1BQWEsU0FBUztJQUR0QjtRQUlFLFlBQU8sR0FBNEIsRUFBRSxDQUFDO1FBQ3RDLGdCQUFXLEdBQWMsRUFBRSxDQUFDO1FBQzVCLFlBQU8sR0FBYyxFQUFFLENBQUM7SUFDMUIsQ0FBQztDQUFBLENBQUE7QUFOWSxTQUFTO0lBRHJCLFVBQVUsRUFBRTtHQUNBLFNBQVMsQ0FNckI7U0FOWSxTQUFTIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgRnJhbWV3b3JrIHtcclxuICBuYW1lOiBzdHJpbmc7XHJcbiAgZnJhbWV3b3JrOiBhbnk7XHJcbiAgd2lkZ2V0cz86IHsgW2tleTogc3RyaW5nXTogYW55IH0gPSB7fTtcclxuICBzdHlsZXNoZWV0cz86IHN0cmluZ1tdID0gW107XHJcbiAgc2NyaXB0cz86IHN0cmluZ1tdID0gW107XHJcbn1cclxuIl19