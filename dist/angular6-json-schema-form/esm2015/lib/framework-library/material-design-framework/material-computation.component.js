import * as tslib_1 from "tslib";
import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatAutocomplete } from '@angular/material';
import { map, startWith, skipWhile } from 'rxjs/operators';
import { JsonSchemaFormService } from '../../json-schema-form.service';
let MaterialComputationComponent = class MaterialComputationComponent {
    constructor(jsf) {
        this.jsf = jsf;
        this.outputTypeControl = new FormControl('Alphanumeric');
        this.functionTypeControl = new FormControl();
        this.functionValueControl = new FormControl();
        this.copyToControl = new FormControl();
        this.joinTheseControl = new FormControl();
        this.controlDisabled = true;
        this.boundControl = false;
        this.isArray = true;
        this.selectable = true;
        this.addOnBlur = true;
        this.removable = true;
        this.separatorKeysCodes = [ENTER, COMMA];
        this.selectedLeftOpt = '';
        this.selectedRightOpt = [];
        this.outputType = ['Alphanumeric', 'Numeric'];
        this.functionType = ['First', 'Last'];
    }
    ngOnInit() {
        this.options = this.layoutNode.options || {};
        this.leftEnum = this.options.leftEnum || [];
        this.rightEnum = this.options.rightEnum || [];
        this.selectedLeftOpt = this.options.selectedLeftOpt || '';
        this.selectedRightOpt = this.options.selectedRightOpt || [];
        this.jsf.initializeControl(this);
        if (this.formControl.value) {
            const _fcValue = JSON.parse(this.formControl.value);
            Object.keys(_fcValue).forEach(key => {
                if (key === 'formula') {
                    this.outputTypeControl.setValue(_fcValue[key].outputType || 'Alphanumeric');
                    this.functionTypeControl.setValue(_fcValue[key].functionType);
                    this.functionValueControl.setValue(_fcValue[key].functionValue);
                }
                else {
                    const foundLeftEnum = this.leftEnum.find(le => le.field === key);
                    this.selectedLeftOpt = foundLeftEnum ? foundLeftEnum.name : '';
                    this.selectedRightOpt = _fcValue[key] || [];
                }
            });
        }
        this.copyToControl.setValue(this.selectedLeftOpt);
        this.updateValue();
        if (!this.options.notitle &&
            !this.options.description &&
            this.options.placeholder) {
            this.options.description = this.options.placeholder;
        }
        this.filteredLeftEnum = this.copyToControl.valueChanges.pipe(startWith(null), map((fre) => fre ? this.filterLeftEnum(fre) : this.rightEnum.slice()));
        this.filteredRightEnum = this.joinTheseControl.valueChanges.pipe(startWith(null), map((fre) => fre ? this.filterRightEnum(fre) : this.rightEnum.slice()));
        this.copyToControl.valueChanges
            .pipe(skipWhile(value => {
            if (this.options.enableCustomInput) {
                return false;
            }
            return this.leftEnum.findIndex(le => le.name.toLowerCase() === value.split('.')[0].toLowerCase()) !== -1
                ? false
                : true;
        }))
            .subscribe(value => this.updateValue());
        this.outputTypeControl.valueChanges.subscribe(value => this.updateValue());
        this.functionTypeControl.valueChanges.subscribe(value => {
            this.updateValue();
        });
        this.functionValueControl.valueChanges.subscribe(value => this.updateValue());
    }
    updateValue() {
        const foundLeftEnum = this.leftEnum.find(le => le.name === this.copyToControl.value);
        const outputKey = foundLeftEnum ? foundLeftEnum.field : this.options.name;
        const _output = {
            formula: {
                outputType: this.outputTypeControl.value,
                functionType: this.functionTypeControl.value,
                functionValue: this.functionValueControl.value
            },
            [outputKey]: this.selectedRightOpt
        };
        this.jsf.updateValue(this, JSON.stringify(_output));
        if (this.options.onChanges &&
            typeof this.options.onChanges === 'function') {
            this.options.onChanges(_output);
        }
    }
    filterLeftEnum(fle) {
        return this.leftEnum.filter(opt => opt.name.toLowerCase().indexOf(fle.toLowerCase()) > -1);
    }
    filterRightEnum(fre) {
        return this.rightEnum.filter(opt => opt.name.toLowerCase().indexOf(fre.toLowerCase()) > -1);
    }
    add(event) {
        const input = event.input;
        const value = event.value;
        if (value) {
            this.addSelected(value);
        }
        // Reset the input value
        if (input) {
            input.value = '';
        }
        this.joinTheseControl.setValue(null);
        this.updateValue();
    }
    addSelected(value) {
        const foundRightEnum = this.rightEnum.find(re => re.name === value);
        this.selectedRightOpt.push(foundRightEnum
            ? foundRightEnum
            : { id: null, name: '', field: '', value: value });
    }
    selected(event) {
        this.addSelected(event.option.viewValue);
        this.chipInput.nativeElement.value = '';
        this.joinTheseControl.setValue(null);
        this.updateValue();
    }
    remove(opt) {
        const index = this.selectedRightOpt.indexOf(opt);
        if (index >= 0) {
            this.selectedRightOpt.splice(index, 1);
        }
        this.updateValue();
    }
};
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], MaterialComputationComponent.prototype, "layoutNode", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Array)
], MaterialComputationComponent.prototype, "layoutIndex", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Array)
], MaterialComputationComponent.prototype, "dataIndex", void 0);
tslib_1.__decorate([
    ViewChild('chipInput'),
    tslib_1.__metadata("design:type", ElementRef)
], MaterialComputationComponent.prototype, "chipInput", void 0);
tslib_1.__decorate([
    ViewChild('rightAuto'),
    tslib_1.__metadata("design:type", MatAutocomplete)
], MaterialComputationComponent.prototype, "matAutocomplete", void 0);
MaterialComputationComponent = tslib_1.__decorate([
    Component({
        selector: 'material-computation-widget',
        template: `
    <div fxLayout="column" fxLayoutAlign="start center">
      <div
        fxLayout="row"
        fxLayoutAlign="space-between center"
        class="function-container"
      >
        <mat-form-field
          [class]="options?.htmlClass || ''"
          [floatLabel]="
            options?.floatLabel || (options?.notitle ? 'never' : 'auto')
          "
          [style.width]="'30%'"
        >
          <mat-select
            [formControl]="outputTypeControl"
            placeholder="Output Type"
          >
            <mat-option
              *ngFor="let ot of outputType"
              [value]="ot"
              [attr.selected]="ot === outputTypeControl.value"
            >
              {{ ot }}
            </mat-option>
          </mat-select>
        </mat-form-field>

        <mat-form-field
          [class]="options?.htmlClass || ''"
          [floatLabel]="
            options?.floatLabel || (options?.notitle ? 'never' : 'auto')
          "
          [style.width]="'30%'"
        >
          <mat-select
            [formControl]="functionTypeControl"
            placeholder="Function Type"
          >
            <mat-option (click)="functionTypeControl.reset()"></mat-option>
            <mat-option
              *ngFor="let ft of functionType"
              [value]="ft"
              [attr.selected]="ft === functionTypeControl.value"
            >
              {{ ft }}
            </mat-option>
          </mat-select>
        </mat-form-field>

        <mat-form-field
          [class]="options?.htmlClass || ''"
          [floatLabel]="
            options?.floatLabel || (options?.notitle ? 'never' : 'auto')
          "
          [style.width]="'30%'"
        >
          <input
            matInput
            [formControl]="functionValueControl"
            placeholder="Function Value"
          />
        </mat-form-field>
      </div>
      <div
        fxLayout="row"
        fxLayoutAlign="start start"
        class="computations-container"
      >
        <mat-form-field
          [class]="options?.htmlClass || ''"
          [floatLabel]="
            options?.floatLabel || (options?.notitle ? 'never' : 'auto')
          "
          [style.width]="'30%'"
        >
          <input
            matInput
            [formControl]="copyToControl"
            [matAutocomplete]="leftAuto"
            placeholder="Copy To"
            [style.width]="'100%'"
          />
          <mat-autocomplete #leftAuto="matAutocomplete">
            <mat-option
              *ngFor="let option of filteredLeftEnum | async"
              [value]="option.name"
            >
              {{ option.name }}
            </mat-option>
          </mat-autocomplete>
        </mat-form-field>

        <p
          [ngStyle]="{
            'text-align': 'center',
            width: '5%',
            margin: '28px 0 0 0'
          }"
        >
          =
        </p>

        <mat-form-field
          [class]="options?.htmlClass || ''"
          [floatLabel]="
            options?.floatLabel || (options?.notitle ? 'never' : 'auto')
          "
          [style.width]="'65%'"
        >
          <mat-chip-list #chipList [style.width]="'100%'">
            <div class="right-chiplist-content">
              <input
                matInput
                #chipInput
                [formControl]="joinTheseControl"
                [matAutocomplete]="rightAuto"
                [matChipInputFor]="chipList"
                [matChipInputAddOnBlur]="addOnBlur"
                [matChipInputSeparatorKeyCodes]="separatorKeysCodes"
                (matChipInputTokenEnd)="add($event)"
                placeholder="Join these"
                [style.width]="'100%'"
              />
              <mat-chip
                *ngFor="let opt of selectedRightOpt"
                [selectable]="selectable"
                [removable]="removable"
                (removed)="remove(opt)"
              >
                <span>{{ opt.name || opt.value }}</span>
                <mat-icon matChipRemove>cancel</mat-icon>
              </mat-chip>
            </div>
          </mat-chip-list>
          <mat-autocomplete
            #rightAuto="matAutocomplete"
            (optionSelected)="selected($event)"
            [style.width]="'100%'"
          >
            <mat-option
              *ngFor="let option of filteredRightEnum | async"
              [value]="option.name"
            >
              {{ option.name }}
            </mat-option>
          </mat-autocomplete>
        </mat-form-field>
      </div>
    </div>
  `,
        styles: [`
      .function-container {
        width: 100%;
      }

      .computations-container {
        width: 100%;
      }

      .right-chiplist-content {
        width: 100%;
      }

      .mat-chip-remove {
        background: grey;
        border-radius: 50%;
        color: white !important;
        font-size: 18px;
        font-weight: bold;
      }
    `]
    }),
    tslib_1.__metadata("design:paramtypes", [JsonSchemaFormService])
], MaterialComputationComponent);
export { MaterialComputationComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWF0ZXJpYWwtY29tcHV0YXRpb24uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYW5ndWxhcjYtanNvbi1zY2hlbWEtZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9mcmFtZXdvcmstbGlicmFyeS9tYXRlcmlhbC1kZXNpZ24tZnJhbWV3b3JrL21hdGVyaWFsLWNvbXB1dGF0aW9uLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFVLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNoRixPQUFPLEVBQW1CLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzlELE9BQU8sRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDckQsT0FBTyxFQUdMLGVBQWUsRUFDaEIsTUFBTSxtQkFBbUIsQ0FBQztBQUUzQixPQUFPLEVBQUUsR0FBRyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUUzRCxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQW1MdkUsSUFBYSw0QkFBNEIsR0FBekMsTUFBYSw0QkFBNEI7SUFpQ3ZDLFlBQW9CLEdBQTBCO1FBQTFCLFFBQUcsR0FBSCxHQUFHLENBQXVCO1FBL0I5QyxzQkFBaUIsR0FBRyxJQUFJLFdBQVcsQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUNwRCx3QkFBbUIsR0FBRyxJQUFJLFdBQVcsRUFBRSxDQUFDO1FBQ3hDLHlCQUFvQixHQUFHLElBQUksV0FBVyxFQUFFLENBQUM7UUFDekMsa0JBQWEsR0FBRyxJQUFJLFdBQVcsRUFBRSxDQUFDO1FBQ2xDLHFCQUFnQixHQUFHLElBQUksV0FBVyxFQUFFLENBQUM7UUFHckMsb0JBQWUsR0FBRyxJQUFJLENBQUM7UUFDdkIsaUJBQVksR0FBRyxLQUFLLENBQUM7UUFFckIsWUFBTyxHQUFHLElBQUksQ0FBQztRQUtmLGVBQVUsR0FBRyxJQUFJLENBQUM7UUFDbEIsY0FBUyxHQUFHLElBQUksQ0FBQztRQUNqQixjQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLHVCQUFrQixHQUFhLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBSzlDLG9CQUFlLEdBQUcsRUFBRSxDQUFDO1FBQ3JCLHFCQUFnQixHQUFVLEVBQUUsQ0FBQztRQUM3QixlQUFVLEdBQWEsQ0FBQyxjQUFjLEVBQUUsU0FBUyxDQUFDLENBQUM7UUFDbkQsaUJBQVksR0FBYSxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQztJQUtNLENBQUM7SUFFbEQsUUFBUTtRQUNOLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLElBQUksRUFBRSxDQUFDO1FBQzdDLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLElBQUksRUFBRSxDQUFDO1FBQzVDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLElBQUksRUFBRSxDQUFDO1FBQzlDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxlQUFlLElBQUksRUFBRSxDQUFDO1FBQzFELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLGdCQUFnQixJQUFJLEVBQUUsQ0FBQztRQUM1RCxJQUFJLENBQUMsR0FBRyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2pDLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUU7WUFDMUIsTUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBRXBELE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxFQUFFO2dCQUNsQyxJQUFJLEdBQUcsS0FBSyxTQUFTLEVBQUU7b0JBQ3JCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQzdCLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxVQUFVLElBQUksY0FBYyxDQUMzQyxDQUFDO29CQUNGLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDO29CQUM5RCxJQUFJLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQztpQkFDakU7cUJBQU07b0JBQ0wsTUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsS0FBSyxLQUFLLEdBQUcsQ0FBQyxDQUFDO29CQUNqRSxJQUFJLENBQUMsZUFBZSxHQUFHLGFBQWEsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO29CQUMvRCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsUUFBUSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztpQkFDN0M7WUFDSCxDQUFDLENBQUMsQ0FBQztTQUNKO1FBQ0QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQ2xELElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUNuQixJQUNFLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPO1lBQ3JCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXO1lBQ3pCLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUN4QjtZQUNBLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDO1NBQ3JEO1FBRUQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLElBQUksQ0FDMUQsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUNmLEdBQUcsQ0FBQyxDQUFDLEdBQWtCLEVBQUUsRUFBRSxDQUN6QixHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQ3hELENBQ0YsQ0FBQztRQUVGLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxDQUFDLElBQUksQ0FDOUQsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUNmLEdBQUcsQ0FBQyxDQUFDLEdBQWtCLEVBQUUsRUFBRSxDQUN6QixHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQ3pELENBQ0YsQ0FBQztRQUVGLElBQUksQ0FBQyxhQUFhLENBQUMsWUFBWTthQUM1QixJQUFJLENBQ0gsU0FBUyxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ2hCLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsRUFBRTtnQkFDbEMsT0FBTyxLQUFLLENBQUM7YUFDZDtZQUNELE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQzVCLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsS0FBSyxLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUNsRSxLQUFLLENBQUMsQ0FBQztnQkFDTixDQUFDLENBQUMsS0FBSztnQkFDUCxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQ0g7YUFDQSxTQUFTLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztRQUUxQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO1FBQzNFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ3RELElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUNyQixDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQ3ZELElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FDbkIsQ0FBQztJQUNKLENBQUM7SUFFRCxXQUFXO1FBQ1QsTUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQ3RDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FDM0MsQ0FBQztRQUNGLE1BQU0sU0FBUyxHQUFHLGFBQWEsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUM7UUFDMUUsTUFBTSxPQUFPLEdBQUc7WUFDZCxPQUFPLEVBQUU7Z0JBQ1AsVUFBVSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLO2dCQUN4QyxZQUFZLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUs7Z0JBQzVDLGFBQWEsRUFBRSxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSzthQUMvQztZQUNELENBQUMsU0FBUyxDQUFDLEVBQUUsSUFBSSxDQUFDLGdCQUFnQjtTQUNuQyxDQUFDO1FBQ0YsSUFBSSxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztRQUNwRCxJQUNFLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUztZQUN0QixPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxLQUFLLFVBQVUsRUFDNUM7WUFDQSxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztTQUNqQztJQUNILENBQUM7SUFFTyxjQUFjLENBQUMsR0FBRztRQUN4QixPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUN6QixHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUM5RCxDQUFDO0lBQ0osQ0FBQztJQUVPLGVBQWUsQ0FBQyxHQUFHO1FBQ3pCLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQzFCLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQzlELENBQUM7SUFDSixDQUFDO0lBRUQsR0FBRyxDQUFDLEtBQXdCO1FBQzFCLE1BQU0sS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUM7UUFDMUIsTUFBTSxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQztRQUUxQixJQUFJLEtBQUssRUFBRTtZQUNULElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDekI7UUFFRCx3QkFBd0I7UUFDeEIsSUFBSSxLQUFLLEVBQUU7WUFDVCxLQUFLLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztTQUNsQjtRQUNELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDckMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ3JCLENBQUM7SUFFTyxXQUFXLENBQUMsS0FBSztRQUN2QixNQUFNLGNBQWMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLEtBQUssS0FBSyxDQUFDLENBQUM7UUFDcEUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FDeEIsY0FBYztZQUNaLENBQUMsQ0FBQyxjQUFjO1lBQ2hCLENBQUMsQ0FBQyxFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsQ0FDcEQsQ0FBQztJQUNKLENBQUM7SUFFRCxRQUFRLENBQUMsS0FBbUM7UUFDMUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3pDLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7UUFDeEMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNyQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDckIsQ0FBQztJQUVELE1BQU0sQ0FBQyxHQUFXO1FBQ2hCLE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7UUFFakQsSUFBSSxLQUFLLElBQUksQ0FBQyxFQUFFO1lBQ2QsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDeEM7UUFDRCxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDckIsQ0FBQztDQUNGLENBQUE7QUF4S1U7SUFBUixLQUFLLEVBQUU7O2dFQUFpQjtBQUNoQjtJQUFSLEtBQUssRUFBRTs7aUVBQXVCO0FBQ3RCO0lBQVIsS0FBSyxFQUFFOzsrREFBcUI7QUFlTDtJQUF2QixTQUFTLENBQUMsV0FBVyxDQUFDO3NDQUFZLFVBQVU7K0RBQW1CO0FBQ3hDO0lBQXZCLFNBQVMsQ0FBQyxXQUFXLENBQUM7c0NBQWtCLGVBQWU7cUVBQUM7QUEvQjlDLDRCQUE0QjtJQWpMeEMsU0FBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLDZCQUE2QjtRQUN2QyxRQUFRLEVBQUU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQXNKVDtpQkFFQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7S0FvQkM7S0FFSixDQUFDOzZDQWtDeUIscUJBQXFCO0dBakNuQyw0QkFBNEIsQ0FxTHhDO1NBckxZLDRCQUE0QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgRWxlbWVudFJlZiwgSW5wdXQsIE9uSW5pdCwgVmlld0NoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEFic3RyYWN0Q29udHJvbCwgRm9ybUNvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IENPTU1BLCBFTlRFUiB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9rZXljb2Rlcyc7XHJcbmltcG9ydCB7XHJcbiAgTWF0QXV0b2NvbXBsZXRlU2VsZWN0ZWRFdmVudCxcclxuICBNYXRDaGlwSW5wdXRFdmVudCxcclxuICBNYXRBdXRvY29tcGxldGVcclxufSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgbWFwLCBzdGFydFdpdGgsIHNraXBXaGlsZSB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbmltcG9ydCB7IEpzb25TY2hlbWFGb3JtU2VydmljZSB9IGZyb20gJy4uLy4uL2pzb24tc2NoZW1hLWZvcm0uc2VydmljZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ21hdGVyaWFsLWNvbXB1dGF0aW9uLXdpZGdldCcsXHJcbiAgdGVtcGxhdGU6IGBcclxuICAgIDxkaXYgZnhMYXlvdXQ9XCJjb2x1bW5cIiBmeExheW91dEFsaWduPVwic3RhcnQgY2VudGVyXCI+XHJcbiAgICAgIDxkaXZcclxuICAgICAgICBmeExheW91dD1cInJvd1wiXHJcbiAgICAgICAgZnhMYXlvdXRBbGlnbj1cInNwYWNlLWJldHdlZW4gY2VudGVyXCJcclxuICAgICAgICBjbGFzcz1cImZ1bmN0aW9uLWNvbnRhaW5lclwiXHJcbiAgICAgID5cclxuICAgICAgICA8bWF0LWZvcm0tZmllbGRcclxuICAgICAgICAgIFtjbGFzc109XCJvcHRpb25zPy5odG1sQ2xhc3MgfHwgJydcIlxyXG4gICAgICAgICAgW2Zsb2F0TGFiZWxdPVwiXHJcbiAgICAgICAgICAgIG9wdGlvbnM/LmZsb2F0TGFiZWwgfHwgKG9wdGlvbnM/Lm5vdGl0bGUgPyAnbmV2ZXInIDogJ2F1dG8nKVxyXG4gICAgICAgICAgXCJcclxuICAgICAgICAgIFtzdHlsZS53aWR0aF09XCInMzAlJ1wiXHJcbiAgICAgICAgPlxyXG4gICAgICAgICAgPG1hdC1zZWxlY3RcclxuICAgICAgICAgICAgW2Zvcm1Db250cm9sXT1cIm91dHB1dFR5cGVDb250cm9sXCJcclxuICAgICAgICAgICAgcGxhY2Vob2xkZXI9XCJPdXRwdXQgVHlwZVwiXHJcbiAgICAgICAgICA+XHJcbiAgICAgICAgICAgIDxtYXQtb3B0aW9uXHJcbiAgICAgICAgICAgICAgKm5nRm9yPVwibGV0IG90IG9mIG91dHB1dFR5cGVcIlxyXG4gICAgICAgICAgICAgIFt2YWx1ZV09XCJvdFwiXHJcbiAgICAgICAgICAgICAgW2F0dHIuc2VsZWN0ZWRdPVwib3QgPT09IG91dHB1dFR5cGVDb250cm9sLnZhbHVlXCJcclxuICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgIHt7IG90IH19XHJcbiAgICAgICAgICAgIDwvbWF0LW9wdGlvbj5cclxuICAgICAgICAgIDwvbWF0LXNlbGVjdD5cclxuICAgICAgICA8L21hdC1mb3JtLWZpZWxkPlxyXG5cclxuICAgICAgICA8bWF0LWZvcm0tZmllbGRcclxuICAgICAgICAgIFtjbGFzc109XCJvcHRpb25zPy5odG1sQ2xhc3MgfHwgJydcIlxyXG4gICAgICAgICAgW2Zsb2F0TGFiZWxdPVwiXHJcbiAgICAgICAgICAgIG9wdGlvbnM/LmZsb2F0TGFiZWwgfHwgKG9wdGlvbnM/Lm5vdGl0bGUgPyAnbmV2ZXInIDogJ2F1dG8nKVxyXG4gICAgICAgICAgXCJcclxuICAgICAgICAgIFtzdHlsZS53aWR0aF09XCInMzAlJ1wiXHJcbiAgICAgICAgPlxyXG4gICAgICAgICAgPG1hdC1zZWxlY3RcclxuICAgICAgICAgICAgW2Zvcm1Db250cm9sXT1cImZ1bmN0aW9uVHlwZUNvbnRyb2xcIlxyXG4gICAgICAgICAgICBwbGFjZWhvbGRlcj1cIkZ1bmN0aW9uIFR5cGVcIlxyXG4gICAgICAgICAgPlxyXG4gICAgICAgICAgICA8bWF0LW9wdGlvbiAoY2xpY2spPVwiZnVuY3Rpb25UeXBlQ29udHJvbC5yZXNldCgpXCI+PC9tYXQtb3B0aW9uPlxyXG4gICAgICAgICAgICA8bWF0LW9wdGlvblxyXG4gICAgICAgICAgICAgICpuZ0Zvcj1cImxldCBmdCBvZiBmdW5jdGlvblR5cGVcIlxyXG4gICAgICAgICAgICAgIFt2YWx1ZV09XCJmdFwiXHJcbiAgICAgICAgICAgICAgW2F0dHIuc2VsZWN0ZWRdPVwiZnQgPT09IGZ1bmN0aW9uVHlwZUNvbnRyb2wudmFsdWVcIlxyXG4gICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAge3sgZnQgfX1cclxuICAgICAgICAgICAgPC9tYXQtb3B0aW9uPlxyXG4gICAgICAgICAgPC9tYXQtc2VsZWN0PlxyXG4gICAgICAgIDwvbWF0LWZvcm0tZmllbGQ+XHJcblxyXG4gICAgICAgIDxtYXQtZm9ybS1maWVsZFxyXG4gICAgICAgICAgW2NsYXNzXT1cIm9wdGlvbnM/Lmh0bWxDbGFzcyB8fCAnJ1wiXHJcbiAgICAgICAgICBbZmxvYXRMYWJlbF09XCJcclxuICAgICAgICAgICAgb3B0aW9ucz8uZmxvYXRMYWJlbCB8fCAob3B0aW9ucz8ubm90aXRsZSA/ICduZXZlcicgOiAnYXV0bycpXHJcbiAgICAgICAgICBcIlxyXG4gICAgICAgICAgW3N0eWxlLndpZHRoXT1cIiczMCUnXCJcclxuICAgICAgICA+XHJcbiAgICAgICAgICA8aW5wdXRcclxuICAgICAgICAgICAgbWF0SW5wdXRcclxuICAgICAgICAgICAgW2Zvcm1Db250cm9sXT1cImZ1bmN0aW9uVmFsdWVDb250cm9sXCJcclxuICAgICAgICAgICAgcGxhY2Vob2xkZXI9XCJGdW5jdGlvbiBWYWx1ZVwiXHJcbiAgICAgICAgICAvPlxyXG4gICAgICAgIDwvbWF0LWZvcm0tZmllbGQ+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgICA8ZGl2XHJcbiAgICAgICAgZnhMYXlvdXQ9XCJyb3dcIlxyXG4gICAgICAgIGZ4TGF5b3V0QWxpZ249XCJzdGFydCBzdGFydFwiXHJcbiAgICAgICAgY2xhc3M9XCJjb21wdXRhdGlvbnMtY29udGFpbmVyXCJcclxuICAgICAgPlxyXG4gICAgICAgIDxtYXQtZm9ybS1maWVsZFxyXG4gICAgICAgICAgW2NsYXNzXT1cIm9wdGlvbnM/Lmh0bWxDbGFzcyB8fCAnJ1wiXHJcbiAgICAgICAgICBbZmxvYXRMYWJlbF09XCJcclxuICAgICAgICAgICAgb3B0aW9ucz8uZmxvYXRMYWJlbCB8fCAob3B0aW9ucz8ubm90aXRsZSA/ICduZXZlcicgOiAnYXV0bycpXHJcbiAgICAgICAgICBcIlxyXG4gICAgICAgICAgW3N0eWxlLndpZHRoXT1cIiczMCUnXCJcclxuICAgICAgICA+XHJcbiAgICAgICAgICA8aW5wdXRcclxuICAgICAgICAgICAgbWF0SW5wdXRcclxuICAgICAgICAgICAgW2Zvcm1Db250cm9sXT1cImNvcHlUb0NvbnRyb2xcIlxyXG4gICAgICAgICAgICBbbWF0QXV0b2NvbXBsZXRlXT1cImxlZnRBdXRvXCJcclxuICAgICAgICAgICAgcGxhY2Vob2xkZXI9XCJDb3B5IFRvXCJcclxuICAgICAgICAgICAgW3N0eWxlLndpZHRoXT1cIicxMDAlJ1wiXHJcbiAgICAgICAgICAvPlxyXG4gICAgICAgICAgPG1hdC1hdXRvY29tcGxldGUgI2xlZnRBdXRvPVwibWF0QXV0b2NvbXBsZXRlXCI+XHJcbiAgICAgICAgICAgIDxtYXQtb3B0aW9uXHJcbiAgICAgICAgICAgICAgKm5nRm9yPVwibGV0IG9wdGlvbiBvZiBmaWx0ZXJlZExlZnRFbnVtIHwgYXN5bmNcIlxyXG4gICAgICAgICAgICAgIFt2YWx1ZV09XCJvcHRpb24ubmFtZVwiXHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICB7eyBvcHRpb24ubmFtZSB9fVxyXG4gICAgICAgICAgICA8L21hdC1vcHRpb24+XHJcbiAgICAgICAgICA8L21hdC1hdXRvY29tcGxldGU+XHJcbiAgICAgICAgPC9tYXQtZm9ybS1maWVsZD5cclxuXHJcbiAgICAgICAgPHBcclxuICAgICAgICAgIFtuZ1N0eWxlXT1cIntcclxuICAgICAgICAgICAgJ3RleHQtYWxpZ24nOiAnY2VudGVyJyxcclxuICAgICAgICAgICAgd2lkdGg6ICc1JScsXHJcbiAgICAgICAgICAgIG1hcmdpbjogJzI4cHggMCAwIDAnXHJcbiAgICAgICAgICB9XCJcclxuICAgICAgICA+XHJcbiAgICAgICAgICA9XHJcbiAgICAgICAgPC9wPlxyXG5cclxuICAgICAgICA8bWF0LWZvcm0tZmllbGRcclxuICAgICAgICAgIFtjbGFzc109XCJvcHRpb25zPy5odG1sQ2xhc3MgfHwgJydcIlxyXG4gICAgICAgICAgW2Zsb2F0TGFiZWxdPVwiXHJcbiAgICAgICAgICAgIG9wdGlvbnM/LmZsb2F0TGFiZWwgfHwgKG9wdGlvbnM/Lm5vdGl0bGUgPyAnbmV2ZXInIDogJ2F1dG8nKVxyXG4gICAgICAgICAgXCJcclxuICAgICAgICAgIFtzdHlsZS53aWR0aF09XCInNjUlJ1wiXHJcbiAgICAgICAgPlxyXG4gICAgICAgICAgPG1hdC1jaGlwLWxpc3QgI2NoaXBMaXN0IFtzdHlsZS53aWR0aF09XCInMTAwJSdcIj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cInJpZ2h0LWNoaXBsaXN0LWNvbnRlbnRcIj5cclxuICAgICAgICAgICAgICA8aW5wdXRcclxuICAgICAgICAgICAgICAgIG1hdElucHV0XHJcbiAgICAgICAgICAgICAgICAjY2hpcElucHV0XHJcbiAgICAgICAgICAgICAgICBbZm9ybUNvbnRyb2xdPVwiam9pblRoZXNlQ29udHJvbFwiXHJcbiAgICAgICAgICAgICAgICBbbWF0QXV0b2NvbXBsZXRlXT1cInJpZ2h0QXV0b1wiXHJcbiAgICAgICAgICAgICAgICBbbWF0Q2hpcElucHV0Rm9yXT1cImNoaXBMaXN0XCJcclxuICAgICAgICAgICAgICAgIFttYXRDaGlwSW5wdXRBZGRPbkJsdXJdPVwiYWRkT25CbHVyXCJcclxuICAgICAgICAgICAgICAgIFttYXRDaGlwSW5wdXRTZXBhcmF0b3JLZXlDb2Rlc109XCJzZXBhcmF0b3JLZXlzQ29kZXNcIlxyXG4gICAgICAgICAgICAgICAgKG1hdENoaXBJbnB1dFRva2VuRW5kKT1cImFkZCgkZXZlbnQpXCJcclxuICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwiSm9pbiB0aGVzZVwiXHJcbiAgICAgICAgICAgICAgICBbc3R5bGUud2lkdGhdPVwiJzEwMCUnXCJcclxuICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgIDxtYXQtY2hpcFxyXG4gICAgICAgICAgICAgICAgKm5nRm9yPVwibGV0IG9wdCBvZiBzZWxlY3RlZFJpZ2h0T3B0XCJcclxuICAgICAgICAgICAgICAgIFtzZWxlY3RhYmxlXT1cInNlbGVjdGFibGVcIlxyXG4gICAgICAgICAgICAgICAgW3JlbW92YWJsZV09XCJyZW1vdmFibGVcIlxyXG4gICAgICAgICAgICAgICAgKHJlbW92ZWQpPVwicmVtb3ZlKG9wdClcIlxyXG4gICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIDxzcGFuPnt7IG9wdC5uYW1lIHx8IG9wdC52YWx1ZSB9fTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIDxtYXQtaWNvbiBtYXRDaGlwUmVtb3ZlPmNhbmNlbDwvbWF0LWljb24+XHJcbiAgICAgICAgICAgICAgPC9tYXQtY2hpcD5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICA8L21hdC1jaGlwLWxpc3Q+XHJcbiAgICAgICAgICA8bWF0LWF1dG9jb21wbGV0ZVxyXG4gICAgICAgICAgICAjcmlnaHRBdXRvPVwibWF0QXV0b2NvbXBsZXRlXCJcclxuICAgICAgICAgICAgKG9wdGlvblNlbGVjdGVkKT1cInNlbGVjdGVkKCRldmVudClcIlxyXG4gICAgICAgICAgICBbc3R5bGUud2lkdGhdPVwiJzEwMCUnXCJcclxuICAgICAgICAgID5cclxuICAgICAgICAgICAgPG1hdC1vcHRpb25cclxuICAgICAgICAgICAgICAqbmdGb3I9XCJsZXQgb3B0aW9uIG9mIGZpbHRlcmVkUmlnaHRFbnVtIHwgYXN5bmNcIlxyXG4gICAgICAgICAgICAgIFt2YWx1ZV09XCJvcHRpb24ubmFtZVwiXHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICB7eyBvcHRpb24ubmFtZSB9fVxyXG4gICAgICAgICAgICA8L21hdC1vcHRpb24+XHJcbiAgICAgICAgICA8L21hdC1hdXRvY29tcGxldGU+XHJcbiAgICAgICAgPC9tYXQtZm9ybS1maWVsZD5cclxuICAgICAgPC9kaXY+XHJcbiAgICA8L2Rpdj5cclxuICBgLFxyXG4gIHN0eWxlczogW1xyXG4gICAgYFxyXG4gICAgICAuZnVuY3Rpb24tY29udGFpbmVyIHtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgfVxyXG5cclxuICAgICAgLmNvbXB1dGF0aW9ucy1jb250YWluZXIge1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAucmlnaHQtY2hpcGxpc3QtY29udGVudCB7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC5tYXQtY2hpcC1yZW1vdmUge1xyXG4gICAgICAgIGJhY2tncm91bmQ6IGdyZXk7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgfVxyXG4gICAgYFxyXG4gIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIE1hdGVyaWFsQ29tcHV0YXRpb25Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIGZvcm1Db250cm9sOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgb3V0cHV0VHlwZUNvbnRyb2wgPSBuZXcgRm9ybUNvbnRyb2woJ0FscGhhbnVtZXJpYycpO1xyXG4gIGZ1bmN0aW9uVHlwZUNvbnRyb2wgPSBuZXcgRm9ybUNvbnRyb2woKTtcclxuICBmdW5jdGlvblZhbHVlQ29udHJvbCA9IG5ldyBGb3JtQ29udHJvbCgpO1xyXG4gIGNvcHlUb0NvbnRyb2wgPSBuZXcgRm9ybUNvbnRyb2woKTtcclxuICBqb2luVGhlc2VDb250cm9sID0gbmV3IEZvcm1Db250cm9sKCk7XHJcbiAgY29udHJvbE5hbWU6IHN0cmluZztcclxuICBjb250cm9sVmFsdWU6IGFueTtcclxuICBjb250cm9sRGlzYWJsZWQgPSB0cnVlO1xyXG4gIGJvdW5kQ29udHJvbCA9IGZhbHNlO1xyXG4gIG9wdGlvbnM6IGFueTtcclxuICBpc0FycmF5ID0gdHJ1ZTtcclxuICBASW5wdXQoKSBsYXlvdXROb2RlOiBhbnk7XHJcbiAgQElucHV0KCkgbGF5b3V0SW5kZXg6IG51bWJlcltdO1xyXG4gIEBJbnB1dCgpIGRhdGFJbmRleDogbnVtYmVyW107XHJcblxyXG4gIHNlbGVjdGFibGUgPSB0cnVlO1xyXG4gIGFkZE9uQmx1ciA9IHRydWU7XHJcbiAgcmVtb3ZhYmxlID0gdHJ1ZTtcclxuICBzZXBhcmF0b3JLZXlzQ29kZXM6IG51bWJlcltdID0gW0VOVEVSLCBDT01NQV07XHJcbiAgbGVmdEVudW06IGFueVtdO1xyXG4gIHJpZ2h0RW51bTogYW55W107XHJcbiAgZmlsdGVyZWRMZWZ0RW51bTogT2JzZXJ2YWJsZTxhbnlbXT47XHJcbiAgZmlsdGVyZWRSaWdodEVudW06IE9ic2VydmFibGU8YW55W10+O1xyXG4gIHNlbGVjdGVkTGVmdE9wdCA9ICcnO1xyXG4gIHNlbGVjdGVkUmlnaHRPcHQ6IGFueVtdID0gW107XHJcbiAgb3V0cHV0VHlwZTogc3RyaW5nW10gPSBbJ0FscGhhbnVtZXJpYycsICdOdW1lcmljJ107XHJcbiAgZnVuY3Rpb25UeXBlOiBzdHJpbmdbXSA9IFsnRmlyc3QnLCAnTGFzdCddO1xyXG5cclxuICBAVmlld0NoaWxkKCdjaGlwSW5wdXQnKSBjaGlwSW5wdXQ6IEVsZW1lbnRSZWY8SFRNTElucHV0RWxlbWVudD47XHJcbiAgQFZpZXdDaGlsZCgncmlnaHRBdXRvJykgbWF0QXV0b2NvbXBsZXRlOiBNYXRBdXRvY29tcGxldGU7XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUganNmOiBKc29uU2NoZW1hRm9ybVNlcnZpY2UpIHt9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgdGhpcy5vcHRpb25zID0gdGhpcy5sYXlvdXROb2RlLm9wdGlvbnMgfHwge307XHJcbiAgICB0aGlzLmxlZnRFbnVtID0gdGhpcy5vcHRpb25zLmxlZnRFbnVtIHx8IFtdO1xyXG4gICAgdGhpcy5yaWdodEVudW0gPSB0aGlzLm9wdGlvbnMucmlnaHRFbnVtIHx8IFtdO1xyXG4gICAgdGhpcy5zZWxlY3RlZExlZnRPcHQgPSB0aGlzLm9wdGlvbnMuc2VsZWN0ZWRMZWZ0T3B0IHx8ICcnO1xyXG4gICAgdGhpcy5zZWxlY3RlZFJpZ2h0T3B0ID0gdGhpcy5vcHRpb25zLnNlbGVjdGVkUmlnaHRPcHQgfHwgW107XHJcbiAgICB0aGlzLmpzZi5pbml0aWFsaXplQ29udHJvbCh0aGlzKTtcclxuICAgIGlmICh0aGlzLmZvcm1Db250cm9sLnZhbHVlKSB7XHJcbiAgICAgIGNvbnN0IF9mY1ZhbHVlID0gSlNPTi5wYXJzZSh0aGlzLmZvcm1Db250cm9sLnZhbHVlKTtcclxuXHJcbiAgICAgIE9iamVjdC5rZXlzKF9mY1ZhbHVlKS5mb3JFYWNoKGtleSA9PiB7XHJcbiAgICAgICAgaWYgKGtleSA9PT0gJ2Zvcm11bGEnKSB7XHJcbiAgICAgICAgICB0aGlzLm91dHB1dFR5cGVDb250cm9sLnNldFZhbHVlKFxyXG4gICAgICAgICAgICBfZmNWYWx1ZVtrZXldLm91dHB1dFR5cGUgfHwgJ0FscGhhbnVtZXJpYydcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgICB0aGlzLmZ1bmN0aW9uVHlwZUNvbnRyb2wuc2V0VmFsdWUoX2ZjVmFsdWVba2V5XS5mdW5jdGlvblR5cGUpO1xyXG4gICAgICAgICAgdGhpcy5mdW5jdGlvblZhbHVlQ29udHJvbC5zZXRWYWx1ZShfZmNWYWx1ZVtrZXldLmZ1bmN0aW9uVmFsdWUpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBjb25zdCBmb3VuZExlZnRFbnVtID0gdGhpcy5sZWZ0RW51bS5maW5kKGxlID0+IGxlLmZpZWxkID09PSBrZXkpO1xyXG4gICAgICAgICAgdGhpcy5zZWxlY3RlZExlZnRPcHQgPSBmb3VuZExlZnRFbnVtID8gZm91bmRMZWZ0RW51bS5uYW1lIDogJyc7XHJcbiAgICAgICAgICB0aGlzLnNlbGVjdGVkUmlnaHRPcHQgPSBfZmNWYWx1ZVtrZXldIHx8IFtdO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICB0aGlzLmNvcHlUb0NvbnRyb2wuc2V0VmFsdWUodGhpcy5zZWxlY3RlZExlZnRPcHQpO1xyXG4gICAgdGhpcy51cGRhdGVWYWx1ZSgpO1xyXG4gICAgaWYgKFxyXG4gICAgICAhdGhpcy5vcHRpb25zLm5vdGl0bGUgJiZcclxuICAgICAgIXRoaXMub3B0aW9ucy5kZXNjcmlwdGlvbiAmJlxyXG4gICAgICB0aGlzLm9wdGlvbnMucGxhY2Vob2xkZXJcclxuICAgICkge1xyXG4gICAgICB0aGlzLm9wdGlvbnMuZGVzY3JpcHRpb24gPSB0aGlzLm9wdGlvbnMucGxhY2Vob2xkZXI7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5maWx0ZXJlZExlZnRFbnVtID0gdGhpcy5jb3B5VG9Db250cm9sLnZhbHVlQ2hhbmdlcy5waXBlKFxyXG4gICAgICBzdGFydFdpdGgobnVsbCksXHJcbiAgICAgIG1hcCgoZnJlOiBzdHJpbmcgfCBudWxsKSA9PlxyXG4gICAgICAgIGZyZSA/IHRoaXMuZmlsdGVyTGVmdEVudW0oZnJlKSA6IHRoaXMucmlnaHRFbnVtLnNsaWNlKClcclxuICAgICAgKVxyXG4gICAgKTtcclxuXHJcbiAgICB0aGlzLmZpbHRlcmVkUmlnaHRFbnVtID0gdGhpcy5qb2luVGhlc2VDb250cm9sLnZhbHVlQ2hhbmdlcy5waXBlKFxyXG4gICAgICBzdGFydFdpdGgobnVsbCksXHJcbiAgICAgIG1hcCgoZnJlOiBzdHJpbmcgfCBudWxsKSA9PlxyXG4gICAgICAgIGZyZSA/IHRoaXMuZmlsdGVyUmlnaHRFbnVtKGZyZSkgOiB0aGlzLnJpZ2h0RW51bS5zbGljZSgpXHJcbiAgICAgIClcclxuICAgICk7XHJcblxyXG4gICAgdGhpcy5jb3B5VG9Db250cm9sLnZhbHVlQ2hhbmdlc1xyXG4gICAgICAucGlwZShcclxuICAgICAgICBza2lwV2hpbGUodmFsdWUgPT4ge1xyXG4gICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5lbmFibGVDdXN0b21JbnB1dCkge1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICByZXR1cm4gdGhpcy5sZWZ0RW51bS5maW5kSW5kZXgoXHJcbiAgICAgICAgICAgIGxlID0+IGxlLm5hbWUudG9Mb3dlckNhc2UoKSA9PT0gdmFsdWUuc3BsaXQoJy4nKVswXS50b0xvd2VyQ2FzZSgpXHJcbiAgICAgICAgICApICE9PSAtMVxyXG4gICAgICAgICAgICA/IGZhbHNlXHJcbiAgICAgICAgICAgIDogdHJ1ZTtcclxuICAgICAgICB9KVxyXG4gICAgICApXHJcbiAgICAgIC5zdWJzY3JpYmUodmFsdWUgPT4gdGhpcy51cGRhdGVWYWx1ZSgpKTtcclxuXHJcbiAgICB0aGlzLm91dHB1dFR5cGVDb250cm9sLnZhbHVlQ2hhbmdlcy5zdWJzY3JpYmUodmFsdWUgPT4gdGhpcy51cGRhdGVWYWx1ZSgpKTtcclxuICAgIHRoaXMuZnVuY3Rpb25UeXBlQ29udHJvbC52YWx1ZUNoYW5nZXMuc3Vic2NyaWJlKHZhbHVlID0+IHtcclxuICAgICAgdGhpcy51cGRhdGVWYWx1ZSgpO1xyXG4gICAgfSk7XHJcbiAgICB0aGlzLmZ1bmN0aW9uVmFsdWVDb250cm9sLnZhbHVlQ2hhbmdlcy5zdWJzY3JpYmUodmFsdWUgPT5cclxuICAgICAgdGhpcy51cGRhdGVWYWx1ZSgpXHJcbiAgICApO1xyXG4gIH1cclxuXHJcbiAgdXBkYXRlVmFsdWUoKSB7XHJcbiAgICBjb25zdCBmb3VuZExlZnRFbnVtID0gdGhpcy5sZWZ0RW51bS5maW5kKFxyXG4gICAgICBsZSA9PiBsZS5uYW1lID09PSB0aGlzLmNvcHlUb0NvbnRyb2wudmFsdWVcclxuICAgICk7XHJcbiAgICBjb25zdCBvdXRwdXRLZXkgPSBmb3VuZExlZnRFbnVtID8gZm91bmRMZWZ0RW51bS5maWVsZCA6IHRoaXMub3B0aW9ucy5uYW1lO1xyXG4gICAgY29uc3QgX291dHB1dCA9IHtcclxuICAgICAgZm9ybXVsYToge1xyXG4gICAgICAgIG91dHB1dFR5cGU6IHRoaXMub3V0cHV0VHlwZUNvbnRyb2wudmFsdWUsXHJcbiAgICAgICAgZnVuY3Rpb25UeXBlOiB0aGlzLmZ1bmN0aW9uVHlwZUNvbnRyb2wudmFsdWUsXHJcbiAgICAgICAgZnVuY3Rpb25WYWx1ZTogdGhpcy5mdW5jdGlvblZhbHVlQ29udHJvbC52YWx1ZVxyXG4gICAgICB9LFxyXG4gICAgICBbb3V0cHV0S2V5XTogdGhpcy5zZWxlY3RlZFJpZ2h0T3B0XHJcbiAgICB9O1xyXG4gICAgdGhpcy5qc2YudXBkYXRlVmFsdWUodGhpcywgSlNPTi5zdHJpbmdpZnkoX291dHB1dCkpO1xyXG4gICAgaWYgKFxyXG4gICAgICB0aGlzLm9wdGlvbnMub25DaGFuZ2VzICYmXHJcbiAgICAgIHR5cGVvZiB0aGlzLm9wdGlvbnMub25DaGFuZ2VzID09PSAnZnVuY3Rpb24nXHJcbiAgICApIHtcclxuICAgICAgdGhpcy5vcHRpb25zLm9uQ2hhbmdlcyhfb3V0cHV0KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHByaXZhdGUgZmlsdGVyTGVmdEVudW0oZmxlKSB7XHJcbiAgICByZXR1cm4gdGhpcy5sZWZ0RW51bS5maWx0ZXIoXHJcbiAgICAgIG9wdCA9PiBvcHQubmFtZS50b0xvd2VyQ2FzZSgpLmluZGV4T2YoZmxlLnRvTG93ZXJDYXNlKCkpID4gLTFcclxuICAgICk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGZpbHRlclJpZ2h0RW51bShmcmUpIHtcclxuICAgIHJldHVybiB0aGlzLnJpZ2h0RW51bS5maWx0ZXIoXHJcbiAgICAgIG9wdCA9PiBvcHQubmFtZS50b0xvd2VyQ2FzZSgpLmluZGV4T2YoZnJlLnRvTG93ZXJDYXNlKCkpID4gLTFcclxuICAgICk7XHJcbiAgfVxyXG5cclxuICBhZGQoZXZlbnQ6IE1hdENoaXBJbnB1dEV2ZW50KTogdm9pZCB7XHJcbiAgICBjb25zdCBpbnB1dCA9IGV2ZW50LmlucHV0O1xyXG4gICAgY29uc3QgdmFsdWUgPSBldmVudC52YWx1ZTtcclxuXHJcbiAgICBpZiAodmFsdWUpIHtcclxuICAgICAgdGhpcy5hZGRTZWxlY3RlZCh2YWx1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gUmVzZXQgdGhlIGlucHV0IHZhbHVlXHJcbiAgICBpZiAoaW5wdXQpIHtcclxuICAgICAgaW5wdXQudmFsdWUgPSAnJztcclxuICAgIH1cclxuICAgIHRoaXMuam9pblRoZXNlQ29udHJvbC5zZXRWYWx1ZShudWxsKTtcclxuICAgIHRoaXMudXBkYXRlVmFsdWUoKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgYWRkU2VsZWN0ZWQodmFsdWUpIHtcclxuICAgIGNvbnN0IGZvdW5kUmlnaHRFbnVtID0gdGhpcy5yaWdodEVudW0uZmluZChyZSA9PiByZS5uYW1lID09PSB2YWx1ZSk7XHJcbiAgICB0aGlzLnNlbGVjdGVkUmlnaHRPcHQucHVzaChcclxuICAgICAgZm91bmRSaWdodEVudW1cclxuICAgICAgICA/IGZvdW5kUmlnaHRFbnVtXHJcbiAgICAgICAgOiB7IGlkOiBudWxsLCBuYW1lOiAnJywgZmllbGQ6ICcnLCB2YWx1ZTogdmFsdWUgfVxyXG4gICAgKTtcclxuICB9XHJcblxyXG4gIHNlbGVjdGVkKGV2ZW50OiBNYXRBdXRvY29tcGxldGVTZWxlY3RlZEV2ZW50KTogdm9pZCB7XHJcbiAgICB0aGlzLmFkZFNlbGVjdGVkKGV2ZW50Lm9wdGlvbi52aWV3VmFsdWUpO1xyXG4gICAgdGhpcy5jaGlwSW5wdXQubmF0aXZlRWxlbWVudC52YWx1ZSA9ICcnO1xyXG4gICAgdGhpcy5qb2luVGhlc2VDb250cm9sLnNldFZhbHVlKG51bGwpO1xyXG4gICAgdGhpcy51cGRhdGVWYWx1ZSgpO1xyXG4gIH1cclxuXHJcbiAgcmVtb3ZlKG9wdDogc3RyaW5nKTogdm9pZCB7XHJcbiAgICBjb25zdCBpbmRleCA9IHRoaXMuc2VsZWN0ZWRSaWdodE9wdC5pbmRleE9mKG9wdCk7XHJcblxyXG4gICAgaWYgKGluZGV4ID49IDApIHtcclxuICAgICAgdGhpcy5zZWxlY3RlZFJpZ2h0T3B0LnNwbGljZShpbmRleCwgMSk7XHJcbiAgICB9XHJcbiAgICB0aGlzLnVwZGF0ZVZhbHVlKCk7XHJcbiAgfVxyXG59XHJcbiJdfQ==