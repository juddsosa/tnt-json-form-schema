import * as tslib_1 from "tslib";
import { FlexLayoutRootComponent } from './flex-layout-root.component';
import { FlexLayoutSectionComponent } from './flex-layout-section.component';
import { Framework } from '../framework';
import { Injectable } from '@angular/core';
import { MaterialAddReferenceComponent } from './material-add-reference.component';
import { MaterialAutoCompleteComponent } from './material-autocomplete.component';
import { MaterialButtonComponent } from './material-button.component';
import { MaterialButtonGroupComponent } from './material-button-group.component';
import { MaterialCheckboxComponent } from './material-checkbox.component';
import { MaterialCheckboxesComponent } from './material-checkboxes.component';
import { MaterialChiplistComponent } from './material-chiplist.component';
import { MaterialComputationComponent } from './material-computation.component';
import { MaterialDatepickerComponent } from './material-datepicker.component';
import { MaterialDesignFrameworkComponent } from './material-design-framework.component';
import { MaterialDynamicOptionsWidget } from './material-dynamic-options.component';
import { MaterialFileComponent } from './material-file.component';
import { MaterialInputComponent } from './material-input.component';
import { MaterialNumberComponent } from './material-number.component';
import { MaterialOneOfComponent } from './material-one-of.component';
import { MaterialRadiosComponent } from './material-radios.component';
import { MaterialSelectComponent } from './material-select.component';
import { MaterialSliderComponent } from './material-slider.component';
import { MaterialStepperComponent } from './material-stepper.component';
import { MaterialTabsComponent } from './material-tabs.component';
import { MaterialTextareaComponent } from './material-textarea.component';
import { MaterialMultiTextFieldComponent } from './material-multitextfield.component';
import { MaterialPasswordComponent } from './material-password.component';
// Material Design Framework
// https://github.com/angular/material2
let MaterialDesignFramework = class MaterialDesignFramework extends Framework {
    // Material Design Framework
    // https://github.com/angular/material2
    constructor() {
        super(...arguments);
        this.name = 'material-design';
        this.framework = MaterialDesignFrameworkComponent;
        this.stylesheets = [
            '//fonts.googleapis.com/icon?family=Material+Icons',
            '//fonts.googleapis.com/css?family=Roboto:300,400,500,700'
        ];
        this.widgets = {
            root: FlexLayoutRootComponent,
            section: FlexLayoutSectionComponent,
            $ref: MaterialAddReferenceComponent,
            autocomplete: MaterialAutoCompleteComponent,
            button: MaterialButtonComponent,
            'button-group': MaterialButtonGroupComponent,
            checkbox: MaterialCheckboxComponent,
            checkboxes: MaterialCheckboxesComponent,
            chiplist: MaterialChiplistComponent,
            computation: MaterialComputationComponent,
            multitextfield: MaterialMultiTextFieldComponent,
            date: MaterialDatepickerComponent,
            dynamicoption: MaterialDynamicOptionsWidget,
            file: MaterialFileComponent,
            number: MaterialNumberComponent,
            'one-of': MaterialOneOfComponent,
            password: MaterialPasswordComponent,
            radios: MaterialRadiosComponent,
            select: MaterialSelectComponent,
            slider: MaterialSliderComponent,
            stepper: MaterialStepperComponent,
            tabs: MaterialTabsComponent,
            text: MaterialInputComponent,
            textarea: MaterialTextareaComponent,
            'alt-date': 'date',
            'any-of': 'one-of',
            card: 'section',
            color: 'text',
            'expansion-panel': 'section',
            hidden: 'none',
            image: 'none',
            integer: 'number',
            radiobuttons: 'button-group',
            range: 'slider',
            submit: 'button',
            tagsinput: 'chiplist',
            wizard: 'stepper'
        };
    }
};
MaterialDesignFramework = tslib_1.__decorate([
    Injectable()
], MaterialDesignFramework);
export { MaterialDesignFramework };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWF0ZXJpYWwtZGVzaWduLmZyYW1ld29yay5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FuZ3VsYXI2LWpzb24tc2NoZW1hLWZvcm0vIiwic291cmNlcyI6WyJsaWIvZnJhbWV3b3JrLWxpYnJhcnkvbWF0ZXJpYWwtZGVzaWduLWZyYW1ld29yay9tYXRlcmlhbC1kZXNpZ24uZnJhbWV3b3JrLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUN2RSxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUM3RSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLDZCQUE2QixFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFDbkYsT0FBTyxFQUFFLDZCQUE2QixFQUFFLE1BQU0sbUNBQW1DLENBQUM7QUFDbEYsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDdEUsT0FBTyxFQUFFLDRCQUE0QixFQUFFLE1BQU0sbUNBQW1DLENBQUM7QUFDakYsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFDMUUsT0FBTyxFQUFFLDJCQUEyQixFQUFFLE1BQU0saUNBQWlDLENBQUM7QUFDOUUsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFDMUUsT0FBTyxFQUFFLDRCQUE0QixFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDaEYsT0FBTyxFQUFFLDJCQUEyQixFQUFFLE1BQU0saUNBQWlDLENBQUM7QUFDOUUsT0FBTyxFQUFFLGdDQUFnQyxFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDekYsT0FBTyxFQUFFLDRCQUE0QixFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFDcEYsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFDbEUsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDcEUsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDdEUsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDckUsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDdEUsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDdEUsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDdEUsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDeEUsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFDbEUsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFDMUUsT0FBTyxFQUFFLCtCQUErQixFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFDdEYsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFFMUUsNEJBQTRCO0FBQzVCLHVDQUF1QztBQUd2QyxJQUFhLHVCQUF1QixHQUFwQyxNQUFhLHVCQUF3QixTQUFRLFNBQVM7SUFKdEQsNEJBQTRCO0lBQzVCLHVDQUF1QztJQUV2Qzs7UUFFRSxTQUFJLEdBQUcsaUJBQWlCLENBQUM7UUFFekIsY0FBUyxHQUFHLGdDQUFnQyxDQUFDO1FBRTdDLGdCQUFXLEdBQUc7WUFDWixtREFBbUQ7WUFDbkQsMERBQTBEO1NBQzNELENBQUM7UUFFRixZQUFPLEdBQUc7WUFDUixJQUFJLEVBQUUsdUJBQXVCO1lBQzdCLE9BQU8sRUFBRSwwQkFBMEI7WUFDbkMsSUFBSSxFQUFFLDZCQUE2QjtZQUNuQyxZQUFZLEVBQUUsNkJBQTZCO1lBQzNDLE1BQU0sRUFBRSx1QkFBdUI7WUFDL0IsY0FBYyxFQUFFLDRCQUE0QjtZQUM1QyxRQUFRLEVBQUUseUJBQXlCO1lBQ25DLFVBQVUsRUFBRSwyQkFBMkI7WUFDdkMsUUFBUSxFQUFFLHlCQUF5QjtZQUNuQyxXQUFXLEVBQUUsNEJBQTRCO1lBQ3pDLGNBQWMsRUFBRSwrQkFBK0I7WUFDL0MsSUFBSSxFQUFFLDJCQUEyQjtZQUNqQyxhQUFhLEVBQUUsNEJBQTRCO1lBQzNDLElBQUksRUFBRSxxQkFBcUI7WUFDM0IsTUFBTSxFQUFFLHVCQUF1QjtZQUMvQixRQUFRLEVBQUUsc0JBQXNCO1lBQ2hDLFFBQVEsRUFBRSx5QkFBeUI7WUFDbkMsTUFBTSxFQUFFLHVCQUF1QjtZQUMvQixNQUFNLEVBQUUsdUJBQXVCO1lBQy9CLE1BQU0sRUFBRSx1QkFBdUI7WUFDL0IsT0FBTyxFQUFFLHdCQUF3QjtZQUNqQyxJQUFJLEVBQUUscUJBQXFCO1lBQzNCLElBQUksRUFBRSxzQkFBc0I7WUFDNUIsUUFBUSxFQUFFLHlCQUF5QjtZQUNuQyxVQUFVLEVBQUUsTUFBTTtZQUNsQixRQUFRLEVBQUUsUUFBUTtZQUNsQixJQUFJLEVBQUUsU0FBUztZQUNmLEtBQUssRUFBRSxNQUFNO1lBQ2IsaUJBQWlCLEVBQUUsU0FBUztZQUM1QixNQUFNLEVBQUUsTUFBTTtZQUNkLEtBQUssRUFBRSxNQUFNO1lBQ2IsT0FBTyxFQUFFLFFBQVE7WUFDakIsWUFBWSxFQUFFLGNBQWM7WUFDNUIsS0FBSyxFQUFFLFFBQVE7WUFDZixNQUFNLEVBQUUsUUFBUTtZQUNoQixTQUFTLEVBQUUsVUFBVTtZQUNyQixNQUFNLEVBQUUsU0FBUztTQUNsQixDQUFDO0lBQ0osQ0FBQztDQUFBLENBQUE7QUFqRFksdUJBQXVCO0lBRG5DLFVBQVUsRUFBRTtHQUNBLHVCQUF1QixDQWlEbkM7U0FqRFksdUJBQXVCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRmxleExheW91dFJvb3RDb21wb25lbnQgfSBmcm9tICcuL2ZsZXgtbGF5b3V0LXJvb3QuY29tcG9uZW50JztcclxuaW1wb3J0IHsgRmxleExheW91dFNlY3Rpb25Db21wb25lbnQgfSBmcm9tICcuL2ZsZXgtbGF5b3V0LXNlY3Rpb24uY29tcG9uZW50JztcclxuaW1wb3J0IHsgRnJhbWV3b3JrIH0gZnJvbSAnLi4vZnJhbWV3b3JrJztcclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNYXRlcmlhbEFkZFJlZmVyZW5jZUNvbXBvbmVudCB9IGZyb20gJy4vbWF0ZXJpYWwtYWRkLXJlZmVyZW5jZS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNYXRlcmlhbEF1dG9Db21wbGV0ZUNvbXBvbmVudCB9IGZyb20gJy4vbWF0ZXJpYWwtYXV0b2NvbXBsZXRlLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hdGVyaWFsQnV0dG9uQ29tcG9uZW50IH0gZnJvbSAnLi9tYXRlcmlhbC1idXR0b24uY29tcG9uZW50JztcclxuaW1wb3J0IHsgTWF0ZXJpYWxCdXR0b25Hcm91cENvbXBvbmVudCB9IGZyb20gJy4vbWF0ZXJpYWwtYnV0dG9uLWdyb3VwLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hdGVyaWFsQ2hlY2tib3hDb21wb25lbnQgfSBmcm9tICcuL21hdGVyaWFsLWNoZWNrYm94LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hdGVyaWFsQ2hlY2tib3hlc0NvbXBvbmVudCB9IGZyb20gJy4vbWF0ZXJpYWwtY2hlY2tib3hlcy5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNYXRlcmlhbENoaXBsaXN0Q29tcG9uZW50IH0gZnJvbSAnLi9tYXRlcmlhbC1jaGlwbGlzdC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNYXRlcmlhbENvbXB1dGF0aW9uQ29tcG9uZW50IH0gZnJvbSAnLi9tYXRlcmlhbC1jb21wdXRhdGlvbi5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNYXRlcmlhbERhdGVwaWNrZXJDb21wb25lbnQgfSBmcm9tICcuL21hdGVyaWFsLWRhdGVwaWNrZXIuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTWF0ZXJpYWxEZXNpZ25GcmFtZXdvcmtDb21wb25lbnQgfSBmcm9tICcuL21hdGVyaWFsLWRlc2lnbi1mcmFtZXdvcmsuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTWF0ZXJpYWxEeW5hbWljT3B0aW9uc1dpZGdldCB9IGZyb20gJy4vbWF0ZXJpYWwtZHluYW1pYy1vcHRpb25zLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hdGVyaWFsRmlsZUNvbXBvbmVudCB9IGZyb20gJy4vbWF0ZXJpYWwtZmlsZS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNYXRlcmlhbElucHV0Q29tcG9uZW50IH0gZnJvbSAnLi9tYXRlcmlhbC1pbnB1dC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNYXRlcmlhbE51bWJlckNvbXBvbmVudCB9IGZyb20gJy4vbWF0ZXJpYWwtbnVtYmVyLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hdGVyaWFsT25lT2ZDb21wb25lbnQgfSBmcm9tICcuL21hdGVyaWFsLW9uZS1vZi5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNYXRlcmlhbFJhZGlvc0NvbXBvbmVudCB9IGZyb20gJy4vbWF0ZXJpYWwtcmFkaW9zLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hdGVyaWFsU2VsZWN0Q29tcG9uZW50IH0gZnJvbSAnLi9tYXRlcmlhbC1zZWxlY3QuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTWF0ZXJpYWxTbGlkZXJDb21wb25lbnQgfSBmcm9tICcuL21hdGVyaWFsLXNsaWRlci5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNYXRlcmlhbFN0ZXBwZXJDb21wb25lbnQgfSBmcm9tICcuL21hdGVyaWFsLXN0ZXBwZXIuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTWF0ZXJpYWxUYWJzQ29tcG9uZW50IH0gZnJvbSAnLi9tYXRlcmlhbC10YWJzLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hdGVyaWFsVGV4dGFyZWFDb21wb25lbnQgfSBmcm9tICcuL21hdGVyaWFsLXRleHRhcmVhLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hdGVyaWFsTXVsdGlUZXh0RmllbGRDb21wb25lbnQgfSBmcm9tICcuL21hdGVyaWFsLW11bHRpdGV4dGZpZWxkLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hdGVyaWFsUGFzc3dvcmRDb21wb25lbnQgfSBmcm9tICcuL21hdGVyaWFsLXBhc3N3b3JkLmNvbXBvbmVudCc7XHJcblxyXG4vLyBNYXRlcmlhbCBEZXNpZ24gRnJhbWV3b3JrXHJcbi8vIGh0dHBzOi8vZ2l0aHViLmNvbS9hbmd1bGFyL21hdGVyaWFsMlxyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgTWF0ZXJpYWxEZXNpZ25GcmFtZXdvcmsgZXh0ZW5kcyBGcmFtZXdvcmsge1xyXG4gIG5hbWUgPSAnbWF0ZXJpYWwtZGVzaWduJztcclxuXHJcbiAgZnJhbWV3b3JrID0gTWF0ZXJpYWxEZXNpZ25GcmFtZXdvcmtDb21wb25lbnQ7XHJcblxyXG4gIHN0eWxlc2hlZXRzID0gW1xyXG4gICAgJy8vZm9udHMuZ29vZ2xlYXBpcy5jb20vaWNvbj9mYW1pbHk9TWF0ZXJpYWwrSWNvbnMnLFxyXG4gICAgJy8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1Sb2JvdG86MzAwLDQwMCw1MDAsNzAwJ1xyXG4gIF07XHJcblxyXG4gIHdpZGdldHMgPSB7XHJcbiAgICByb290OiBGbGV4TGF5b3V0Um9vdENvbXBvbmVudCxcclxuICAgIHNlY3Rpb246IEZsZXhMYXlvdXRTZWN0aW9uQ29tcG9uZW50LFxyXG4gICAgJHJlZjogTWF0ZXJpYWxBZGRSZWZlcmVuY2VDb21wb25lbnQsXHJcbiAgICBhdXRvY29tcGxldGU6IE1hdGVyaWFsQXV0b0NvbXBsZXRlQ29tcG9uZW50LFxyXG4gICAgYnV0dG9uOiBNYXRlcmlhbEJ1dHRvbkNvbXBvbmVudCxcclxuICAgICdidXR0b24tZ3JvdXAnOiBNYXRlcmlhbEJ1dHRvbkdyb3VwQ29tcG9uZW50LFxyXG4gICAgY2hlY2tib3g6IE1hdGVyaWFsQ2hlY2tib3hDb21wb25lbnQsXHJcbiAgICBjaGVja2JveGVzOiBNYXRlcmlhbENoZWNrYm94ZXNDb21wb25lbnQsXHJcbiAgICBjaGlwbGlzdDogTWF0ZXJpYWxDaGlwbGlzdENvbXBvbmVudCxcclxuICAgIGNvbXB1dGF0aW9uOiBNYXRlcmlhbENvbXB1dGF0aW9uQ29tcG9uZW50LFxyXG4gICAgbXVsdGl0ZXh0ZmllbGQ6IE1hdGVyaWFsTXVsdGlUZXh0RmllbGRDb21wb25lbnQsXHJcbiAgICBkYXRlOiBNYXRlcmlhbERhdGVwaWNrZXJDb21wb25lbnQsXHJcbiAgICBkeW5hbWljb3B0aW9uOiBNYXRlcmlhbER5bmFtaWNPcHRpb25zV2lkZ2V0LFxyXG4gICAgZmlsZTogTWF0ZXJpYWxGaWxlQ29tcG9uZW50LFxyXG4gICAgbnVtYmVyOiBNYXRlcmlhbE51bWJlckNvbXBvbmVudCxcclxuICAgICdvbmUtb2YnOiBNYXRlcmlhbE9uZU9mQ29tcG9uZW50LFxyXG4gICAgcGFzc3dvcmQ6IE1hdGVyaWFsUGFzc3dvcmRDb21wb25lbnQsXHJcbiAgICByYWRpb3M6IE1hdGVyaWFsUmFkaW9zQ29tcG9uZW50LFxyXG4gICAgc2VsZWN0OiBNYXRlcmlhbFNlbGVjdENvbXBvbmVudCxcclxuICAgIHNsaWRlcjogTWF0ZXJpYWxTbGlkZXJDb21wb25lbnQsXHJcbiAgICBzdGVwcGVyOiBNYXRlcmlhbFN0ZXBwZXJDb21wb25lbnQsXHJcbiAgICB0YWJzOiBNYXRlcmlhbFRhYnNDb21wb25lbnQsXHJcbiAgICB0ZXh0OiBNYXRlcmlhbElucHV0Q29tcG9uZW50LFxyXG4gICAgdGV4dGFyZWE6IE1hdGVyaWFsVGV4dGFyZWFDb21wb25lbnQsXHJcbiAgICAnYWx0LWRhdGUnOiAnZGF0ZScsXHJcbiAgICAnYW55LW9mJzogJ29uZS1vZicsXHJcbiAgICBjYXJkOiAnc2VjdGlvbicsXHJcbiAgICBjb2xvcjogJ3RleHQnLFxyXG4gICAgJ2V4cGFuc2lvbi1wYW5lbCc6ICdzZWN0aW9uJyxcclxuICAgIGhpZGRlbjogJ25vbmUnLFxyXG4gICAgaW1hZ2U6ICdub25lJyxcclxuICAgIGludGVnZXI6ICdudW1iZXInLFxyXG4gICAgcmFkaW9idXR0b25zOiAnYnV0dG9uLWdyb3VwJyxcclxuICAgIHJhbmdlOiAnc2xpZGVyJyxcclxuICAgIHN1Ym1pdDogJ2J1dHRvbicsXHJcbiAgICB0YWdzaW5wdXQ6ICdjaGlwbGlzdCcsXHJcbiAgICB3aXphcmQ6ICdzdGVwcGVyJ1xyXG4gIH07XHJcbn1cclxuIl19