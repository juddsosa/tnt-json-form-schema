import * as tslib_1 from "tslib";
import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatAutocomplete } from '@angular/material';
import { map, startWith } from 'rxjs/operators';
import { JsonSchemaFormService } from '../../json-schema-form.service';
import _ from 'lodash';
let MaterialChiplistComponent = class MaterialChiplistComponent {
    constructor(jsf) {
        this.jsf = jsf;
        this.dummyFormControl = new FormControl();
        this.controlDisabled = true;
        this.boundControl = false;
        this.isArray = true;
        this.selectable = true;
        this.addOnBlur = true;
        this.enableInput = true;
        this.enableDelete = true;
        this.enableClick = true;
        this.separatorKeysCodes = [ENTER, COMMA];
        this.selectedOpt = [];
        this.currentOpt = [];
    }
    ngOnInit() {
        this.options = this.layoutNode.options || {};
        this.options.enum = this.options.enum || [];
        this.selectedOpt = this.options.selectedOpt || [];
        this.currentOpt = [];
        this.enableInput = this.options.enableInput;
        this.enableDelete = this.options.enableDelete;
        this.enableClick = this.options.enableClick;
        this.jsf.initializeControl(this);
        const _fcValue = JSON.parse(this.formControl.value);
        if (_fcValue && _fcValue.length) {
            for (const value of _fcValue) {
                value.complete != null
                    ? this.selectedOpt.push(value)
                    : this.currentOpt.push(value);
            }
        }
        this.selectedOpt = _.uniqBy(this.selectedOpt, 'name');
        this.updateValue();
        this.filteredEnum = this.dummyFormControl.valueChanges.pipe(startWith(null), map((fe) => fe ? this.findOpts(fe, true) : this.options.enum.slice()));
    }
    updateValue() {
        this.jsf.updateValue(this, JSON.stringify([...this.currentOpt, ...this.selectedOpt]));
        if (this.options.onChanges &&
            typeof this.options.onChanges === 'function') {
            this.options.onChanges(this.currentOpt);
        }
    }
    add(event) {
        if (!this.matAutocomplete.isOpen) {
            const input = event.input;
            const value = event.value;
            if ((value || '').trim()) {
                this.addSelected(value);
            }
            // Reset the input value
            if (input) {
                input.value = '';
            }
            this.dummyFormControl.setValue(null);
            this.updateValue();
        }
    }
    removeSelected(opt) {
        const index = this.selectedOpt.indexOf(opt);
        if (index >= 0) {
            this.selectedOpt.splice(index, 1);
        }
        this.updateValue();
    }
    removeCurrent(opt) {
        const index = this.currentOpt.indexOf(opt);
        if (index >= 0) {
            this.currentOpt.splice(index, 1);
        }
        this.updateValue();
    }
    selected(event) {
        if (this.currentOpt.length < this.options.chipsLimit ||
            this.options.chipsLimit === 0) {
            this.addSelected(event.option.viewValue);
            this.chipInput.nativeElement.value = '';
            this.dummyFormControl.setValue(null);
            this.updateValue();
        }
    }
    onChipsClick(value) {
        if (this.enableClick &&
            this.options.onChipsClick &&
            typeof this.options.onChipsClick === 'function') {
            this.options.onChipsClick(value);
        }
    }
    // private addSelected(value) {
    //   const _opt = this.findOpts(value);
    //   if (_opt) {
    //     const _selectedOpt = this.selectedOpt.filter(
    //       (so: any) => so.name.toLowerCase() === _opt.name.toLowerCase()
    //     );
    //     if (!_selectedOpt.length) {
    //       this.selectedOpt.push(_opt);
    //     }
    //   }
    // }
    addSelected(value) {
        const _opt = this.findOpts(value);
        if (_opt) {
            const _currentOpt = this.currentOpt.filter((so) => so.name.toLowerCase() === _opt.name.toLowerCase());
            const _selectedOpt = this.selectedOpt.filter((so) => so.name.toLowerCase() === _opt.name.toLowerCase());
            if (!_currentOpt.length && !_selectedOpt.length) {
                this.currentOpt.push(_opt);
            }
        }
    }
    findOpts(value, like = false) {
        if (value) {
            const _value = (value.name || value).trim().split(' | ');
            const _foundOpt = like
                ? this.options.enum.filter(opt => opt.name.toLowerCase().indexOf(_value[0].toLowerCase()) === 0)
                : this.options.enum.filter(opt => opt.name.toLowerCase() === _value[0].toLowerCase());
            return _foundOpt.length ? (like ? _foundOpt : _foundOpt[0]) : null;
        }
        return null;
    }
    _filter(value) {
        const _opt = this.findOpts(value, true);
        return _opt
            ? this.options.enum.filter(opt => opt.name.toLowerCase().indexOf(_opt.name.toLowerCase()) === 0)
            : [];
    }
};
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], MaterialChiplistComponent.prototype, "layoutNode", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Array)
], MaterialChiplistComponent.prototype, "layoutIndex", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Array)
], MaterialChiplistComponent.prototype, "dataIndex", void 0);
tslib_1.__decorate([
    ViewChild('chipInput'),
    tslib_1.__metadata("design:type", ElementRef)
], MaterialChiplistComponent.prototype, "chipInput", void 0);
tslib_1.__decorate([
    ViewChild('auto'),
    tslib_1.__metadata("design:type", MatAutocomplete)
], MaterialChiplistComponent.prototype, "matAutocomplete", void 0);
MaterialChiplistComponent = tslib_1.__decorate([
    Component({
        selector: 'material-chiplist-widget',
        template: `
    <mat-form-field
      [class]="options?.htmlClass || ''"
      [floatLabel]="
        options?.floatLabel || (options?.notitle ? 'never' : 'auto')
      "
      [style.width]="'100%'"
    >
      <mat-chip-list #chipList [style.width]="'100%'">
        <div id="currentChips">
          <mat-chip
            *ngFor="let opt of currentOpt"
            [selectable]="selectable"
            [removable]="enableDelete"
            (removed)="removeCurrent(opt)"
            (click)="onChipsClick(opt)"
          >
            <div
              matChipAvatar
              class="complete-marker"
              *ngIf="opt.complete != null && opt.complete != ''"
              [ngClass]="{ complete: opt.complete }"
            ></div>

            <span class="chip-name">{{ opt.name }}</span>
            <mat-icon matChipRemove *ngIf="enableDelete">cancel</mat-icon>
          </mat-chip>
        </div>
        <input
          matInput
          #chipInput
          [placeholder]="
            options?.notitle ? options?.placeholder : options?.title
          "
          [matAutocomplete]="auto"
          [matChipInputFor]="chipList"
          [matChipInputSeparatorKeyCodes]="separatorKeysCodes"
          [matChipInputAddOnBlur]="addOnBlur"
          (matChipInputTokenEnd)="add($event)"
          [style.width]="'100%'"
          [formControl]="dummyFormControl"
          *ngIf="enableInput"
        />
      </mat-chip-list>
      <div>
        <mat-chip
          *ngFor="let opt of selectedOpt"
          [selectable]="selectable"
          [removable]="enableDelete"
          (removed)="removeSelected(opt)"
          (click)="onChipsClick(opt)"
        >
          <div
            matChipAvatar
            class="complete-marker"
            *ngIf="opt.complete != null && opt.complete != ''"
            [ngClass]="{ complete: opt.complete }"
          ></div>

          <span class="chip-name">{{ opt.name }}</span>

          <mat-icon matChipRemove *ngIf="enableDelete">cancel</mat-icon>
        </mat-chip>
      </div>
      <mat-autocomplete
        #auto="matAutocomplete"
        class="chiplist-autocomp"
        [style.width]="'100%'"
        (optionSelected)="selected($event)"
      >
        <mat-option *ngFor="let fe of filteredEnum | async" [value]="fe">
          {{ fe.name }}
        </mat-option>
      </mat-autocomplete>
    </mat-form-field>
  `,
        styles: [`
      .mat-standard-chip {
        margin: 4px 8px 4px 0;
      }

      .chip-name {
        margin-top: 2px;
      }

      .mat-chip-remove {
        font-size: 32px;
        opacity: 1 !important;
      }

      .complete-marker {
        background-color: red;
      }

      .complete {
        background-color: green;
      }
    `]
    }),
    tslib_1.__metadata("design:paramtypes", [JsonSchemaFormService])
], MaterialChiplistComponent);
export { MaterialChiplistComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWF0ZXJpYWwtY2hpcGxpc3QuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYW5ndWxhcjYtanNvbi1zY2hlbWEtZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9mcmFtZXdvcmstbGlicmFyeS9tYXRlcmlhbC1kZXNpZ24tZnJhbWV3b3JrL21hdGVyaWFsLWNoaXBsaXN0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFVLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNoRixPQUFPLEVBQW1CLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzlELE9BQU8sRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDckQsT0FBTyxFQUdMLGVBQWUsRUFDaEIsTUFBTSxtQkFBbUIsQ0FBQztBQUUzQixPQUFPLEVBQUUsR0FBRyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRWhELE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQ3ZFLE9BQU8sQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQXlHdkIsSUFBYSx5QkFBeUIsR0FBdEMsTUFBYSx5QkFBeUI7SUEwQnBDLFlBQW9CLEdBQTBCO1FBQTFCLFFBQUcsR0FBSCxHQUFHLENBQXVCO1FBeEI5QyxxQkFBZ0IsR0FBRyxJQUFJLFdBQVcsRUFBRSxDQUFDO1FBR3JDLG9CQUFlLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLGlCQUFZLEdBQUcsS0FBSyxDQUFDO1FBRXJCLFlBQU8sR0FBRyxJQUFJLENBQUM7UUFLZixlQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLGNBQVMsR0FBRyxJQUFJLENBQUM7UUFDakIsZ0JBQVcsR0FBRyxJQUFJLENBQUM7UUFDbkIsaUJBQVksR0FBRyxJQUFJLENBQUM7UUFDcEIsZ0JBQVcsR0FBRyxJQUFJLENBQUM7UUFDbkIsdUJBQWtCLEdBQWEsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFFOUMsZ0JBQVcsR0FBYSxFQUFFLENBQUM7UUFDM0IsZUFBVSxHQUFhLEVBQUUsQ0FBQztJQUt1QixDQUFDO0lBRWxELFFBQVE7UUFDTixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxJQUFJLEVBQUUsQ0FBQztRQUM3QyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksSUFBSSxFQUFFLENBQUM7UUFDNUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsSUFBSSxFQUFFLENBQUM7UUFDbEQsSUFBSSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQztRQUM1QyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDO1FBQzlDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUM7UUFDNUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNqQyxNQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDcEQsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLE1BQU0sRUFBRTtZQUMvQixLQUFLLE1BQU0sS0FBSyxJQUFJLFFBQVEsRUFBRTtnQkFDNUIsS0FBSyxDQUFDLFFBQVEsSUFBSSxJQUFJO29CQUNwQixDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO29CQUM5QixDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDakM7U0FDRjtRQUNELElBQUksQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQ3RELElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUNuQixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUN6RCxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQ2YsR0FBRyxDQUFDLENBQUMsRUFBaUIsRUFBRSxFQUFFLENBQ3hCLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUN6RCxDQUNGLENBQUM7SUFDSixDQUFDO0lBRUQsV0FBVztRQUNULElBQUksQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUNsQixJQUFJLEVBQ0osSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLFVBQVUsRUFBRSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUMxRCxDQUFDO1FBQ0YsSUFDRSxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVM7WUFDdEIsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsS0FBSyxVQUFVLEVBQzVDO1lBQ0EsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1NBQ3pDO0lBQ0gsQ0FBQztJQUVELEdBQUcsQ0FBQyxLQUF3QjtRQUMxQixJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEVBQUU7WUFDaEMsTUFBTSxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQztZQUMxQixNQUFNLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO1lBRTFCLElBQUksQ0FBQyxLQUFLLElBQUksRUFBRSxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQUU7Z0JBQ3hCLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDekI7WUFFRCx3QkFBd0I7WUFDeEIsSUFBSSxLQUFLLEVBQUU7Z0JBQ1QsS0FBSyxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7YUFDbEI7WUFDRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3JDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUNwQjtJQUNILENBQUM7SUFFRCxjQUFjLENBQUMsR0FBVztRQUN4QixNQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUU1QyxJQUFJLEtBQUssSUFBSSxDQUFDLEVBQUU7WUFDZCxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDbkM7UUFDRCxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDckIsQ0FBQztJQUVELGFBQWEsQ0FBQyxHQUFXO1FBQ3ZCLE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBRTNDLElBQUksS0FBSyxJQUFJLENBQUMsRUFBRTtZQUNkLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztTQUNsQztRQUNELElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUNyQixDQUFDO0lBRUQsUUFBUSxDQUFDLEtBQW1DO1FBQzFDLElBQ0UsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVO1lBQ2hELElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxLQUFLLENBQUMsRUFDN0I7WUFDQSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDekMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztZQUN4QyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3JDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUNwQjtJQUNILENBQUM7SUFFRCxZQUFZLENBQUMsS0FBSztRQUNoQixJQUNFLElBQUksQ0FBQyxXQUFXO1lBQ2hCLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWTtZQUN6QixPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxLQUFLLFVBQVUsRUFDL0M7WUFDQSxJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNsQztJQUNILENBQUM7SUFFRCwrQkFBK0I7SUFDL0IsdUNBQXVDO0lBQ3ZDLGdCQUFnQjtJQUNoQixvREFBb0Q7SUFDcEQsdUVBQXVFO0lBQ3ZFLFNBQVM7SUFDVCxrQ0FBa0M7SUFDbEMscUNBQXFDO0lBQ3JDLFFBQVE7SUFDUixNQUFNO0lBQ04sSUFBSTtJQUVJLFdBQVcsQ0FBQyxLQUFLO1FBQ3ZCLE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDbEMsSUFBSSxJQUFJLEVBQUU7WUFDUixNQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FDeEMsQ0FBQyxFQUFPLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLEtBQUssSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FDL0QsQ0FBQztZQUNGLE1BQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUMxQyxDQUFDLEVBQU8sRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsS0FBSyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUMvRCxDQUFDO1lBQ0YsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFO2dCQUMvQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUM1QjtTQUNGO0lBQ0gsQ0FBQztJQUVPLFFBQVEsQ0FBQyxLQUFLLEVBQUUsT0FBZ0IsS0FBSztRQUMzQyxJQUFJLEtBQUssRUFBRTtZQUNULE1BQU0sTUFBTSxHQUFHLENBQUMsS0FBSyxDQUFDLElBQUksSUFBSSxLQUFLLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDekQsTUFBTSxTQUFTLEdBQUcsSUFBSTtnQkFDcEIsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FDdEIsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQ3JFO2dCQUNILENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQ3RCLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsS0FBSyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLENBQzFELENBQUM7WUFDTixPQUFPLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7U0FDcEU7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFFTyxPQUFPLENBQUMsS0FBVTtRQUN4QixNQUFNLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztRQUN4QyxPQUFPLElBQUk7WUFDVCxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUN0QixHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQ3JFO1lBQ0gsQ0FBQyxDQUFDLEVBQUUsQ0FBQztJQUNULENBQUM7Q0FDRixDQUFBO0FBdktVO0lBQVIsS0FBSyxFQUFFOzs2REFBaUI7QUFDaEI7SUFBUixLQUFLLEVBQUU7OzhEQUF1QjtBQUN0QjtJQUFSLEtBQUssRUFBRTs7NERBQXFCO0FBWUw7SUFBdkIsU0FBUyxDQUFDLFdBQVcsQ0FBQztzQ0FBWSxVQUFVOzREQUFtQjtBQUM3QztJQUFsQixTQUFTLENBQUMsTUFBTSxDQUFDO3NDQUFrQixlQUFlO2tFQUFDO0FBeEJ6Qyx5QkFBeUI7SUF2R3JDLFNBQVMsQ0FBQztRQUNULFFBQVEsRUFBRSwwQkFBMEI7UUFDcEMsUUFBUSxFQUFFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7R0EyRVQ7aUJBRUM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztLQXFCQztLQUVKLENBQUM7NkNBMkJ5QixxQkFBcUI7R0ExQm5DLHlCQUF5QixDQWdMckM7U0FoTFkseUJBQXlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBFbGVtZW50UmVmLCBJbnB1dCwgT25Jbml0LCBWaWV3Q2hpbGQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQWJzdHJhY3RDb250cm9sLCBGb3JtQ29udHJvbCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgQ09NTUEsIEVOVEVSIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2tleWNvZGVzJztcclxuaW1wb3J0IHtcclxuICBNYXRBdXRvY29tcGxldGVTZWxlY3RlZEV2ZW50LFxyXG4gIE1hdENoaXBJbnB1dEV2ZW50LFxyXG4gIE1hdEF1dG9jb21wbGV0ZVxyXG59IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBtYXAsIHN0YXJ0V2l0aCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbmltcG9ydCB7IEpzb25TY2hlbWFGb3JtU2VydmljZSB9IGZyb20gJy4uLy4uL2pzb24tc2NoZW1hLWZvcm0uc2VydmljZSc7XHJcbmltcG9ydCBfIGZyb20gJ2xvZGFzaCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ21hdGVyaWFsLWNoaXBsaXN0LXdpZGdldCcsXHJcbiAgdGVtcGxhdGU6IGBcclxuICAgIDxtYXQtZm9ybS1maWVsZFxyXG4gICAgICBbY2xhc3NdPVwib3B0aW9ucz8uaHRtbENsYXNzIHx8ICcnXCJcclxuICAgICAgW2Zsb2F0TGFiZWxdPVwiXHJcbiAgICAgICAgb3B0aW9ucz8uZmxvYXRMYWJlbCB8fCAob3B0aW9ucz8ubm90aXRsZSA/ICduZXZlcicgOiAnYXV0bycpXHJcbiAgICAgIFwiXHJcbiAgICAgIFtzdHlsZS53aWR0aF09XCInMTAwJSdcIlxyXG4gICAgPlxyXG4gICAgICA8bWF0LWNoaXAtbGlzdCAjY2hpcExpc3QgW3N0eWxlLndpZHRoXT1cIicxMDAlJ1wiPlxyXG4gICAgICAgIDxkaXYgaWQ9XCJjdXJyZW50Q2hpcHNcIj5cclxuICAgICAgICAgIDxtYXQtY2hpcFxyXG4gICAgICAgICAgICAqbmdGb3I9XCJsZXQgb3B0IG9mIGN1cnJlbnRPcHRcIlxyXG4gICAgICAgICAgICBbc2VsZWN0YWJsZV09XCJzZWxlY3RhYmxlXCJcclxuICAgICAgICAgICAgW3JlbW92YWJsZV09XCJlbmFibGVEZWxldGVcIlxyXG4gICAgICAgICAgICAocmVtb3ZlZCk9XCJyZW1vdmVDdXJyZW50KG9wdClcIlxyXG4gICAgICAgICAgICAoY2xpY2spPVwib25DaGlwc0NsaWNrKG9wdClcIlxyXG4gICAgICAgICAgPlxyXG4gICAgICAgICAgICA8ZGl2XHJcbiAgICAgICAgICAgICAgbWF0Q2hpcEF2YXRhclxyXG4gICAgICAgICAgICAgIGNsYXNzPVwiY29tcGxldGUtbWFya2VyXCJcclxuICAgICAgICAgICAgICAqbmdJZj1cIm9wdC5jb21wbGV0ZSAhPSBudWxsICYmIG9wdC5jb21wbGV0ZSAhPSAnJ1wiXHJcbiAgICAgICAgICAgICAgW25nQ2xhc3NdPVwieyBjb21wbGV0ZTogb3B0LmNvbXBsZXRlIH1cIlxyXG4gICAgICAgICAgICA+PC9kaXY+XHJcblxyXG4gICAgICAgICAgICA8c3BhbiBjbGFzcz1cImNoaXAtbmFtZVwiPnt7IG9wdC5uYW1lIH19PC9zcGFuPlxyXG4gICAgICAgICAgICA8bWF0LWljb24gbWF0Q2hpcFJlbW92ZSAqbmdJZj1cImVuYWJsZURlbGV0ZVwiPmNhbmNlbDwvbWF0LWljb24+XHJcbiAgICAgICAgICA8L21hdC1jaGlwPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDxpbnB1dFxyXG4gICAgICAgICAgbWF0SW5wdXRcclxuICAgICAgICAgICNjaGlwSW5wdXRcclxuICAgICAgICAgIFtwbGFjZWhvbGRlcl09XCJcclxuICAgICAgICAgICAgb3B0aW9ucz8ubm90aXRsZSA/IG9wdGlvbnM/LnBsYWNlaG9sZGVyIDogb3B0aW9ucz8udGl0bGVcclxuICAgICAgICAgIFwiXHJcbiAgICAgICAgICBbbWF0QXV0b2NvbXBsZXRlXT1cImF1dG9cIlxyXG4gICAgICAgICAgW21hdENoaXBJbnB1dEZvcl09XCJjaGlwTGlzdFwiXHJcbiAgICAgICAgICBbbWF0Q2hpcElucHV0U2VwYXJhdG9yS2V5Q29kZXNdPVwic2VwYXJhdG9yS2V5c0NvZGVzXCJcclxuICAgICAgICAgIFttYXRDaGlwSW5wdXRBZGRPbkJsdXJdPVwiYWRkT25CbHVyXCJcclxuICAgICAgICAgIChtYXRDaGlwSW5wdXRUb2tlbkVuZCk9XCJhZGQoJGV2ZW50KVwiXHJcbiAgICAgICAgICBbc3R5bGUud2lkdGhdPVwiJzEwMCUnXCJcclxuICAgICAgICAgIFtmb3JtQ29udHJvbF09XCJkdW1teUZvcm1Db250cm9sXCJcclxuICAgICAgICAgICpuZ0lmPVwiZW5hYmxlSW5wdXRcIlxyXG4gICAgICAgIC8+XHJcbiAgICAgIDwvbWF0LWNoaXAtbGlzdD5cclxuICAgICAgPGRpdj5cclxuICAgICAgICA8bWF0LWNoaXBcclxuICAgICAgICAgICpuZ0Zvcj1cImxldCBvcHQgb2Ygc2VsZWN0ZWRPcHRcIlxyXG4gICAgICAgICAgW3NlbGVjdGFibGVdPVwic2VsZWN0YWJsZVwiXHJcbiAgICAgICAgICBbcmVtb3ZhYmxlXT1cImVuYWJsZURlbGV0ZVwiXHJcbiAgICAgICAgICAocmVtb3ZlZCk9XCJyZW1vdmVTZWxlY3RlZChvcHQpXCJcclxuICAgICAgICAgIChjbGljayk9XCJvbkNoaXBzQ2xpY2sob3B0KVwiXHJcbiAgICAgICAgPlxyXG4gICAgICAgICAgPGRpdlxyXG4gICAgICAgICAgICBtYXRDaGlwQXZhdGFyXHJcbiAgICAgICAgICAgIGNsYXNzPVwiY29tcGxldGUtbWFya2VyXCJcclxuICAgICAgICAgICAgKm5nSWY9XCJvcHQuY29tcGxldGUgIT0gbnVsbCAmJiBvcHQuY29tcGxldGUgIT0gJydcIlxyXG4gICAgICAgICAgICBbbmdDbGFzc109XCJ7IGNvbXBsZXRlOiBvcHQuY29tcGxldGUgfVwiXHJcbiAgICAgICAgICA+PC9kaXY+XHJcblxyXG4gICAgICAgICAgPHNwYW4gY2xhc3M9XCJjaGlwLW5hbWVcIj57eyBvcHQubmFtZSB9fTwvc3Bhbj5cclxuXHJcbiAgICAgICAgICA8bWF0LWljb24gbWF0Q2hpcFJlbW92ZSAqbmdJZj1cImVuYWJsZURlbGV0ZVwiPmNhbmNlbDwvbWF0LWljb24+XHJcbiAgICAgICAgPC9tYXQtY2hpcD5cclxuICAgICAgPC9kaXY+XHJcbiAgICAgIDxtYXQtYXV0b2NvbXBsZXRlXHJcbiAgICAgICAgI2F1dG89XCJtYXRBdXRvY29tcGxldGVcIlxyXG4gICAgICAgIGNsYXNzPVwiY2hpcGxpc3QtYXV0b2NvbXBcIlxyXG4gICAgICAgIFtzdHlsZS53aWR0aF09XCInMTAwJSdcIlxyXG4gICAgICAgIChvcHRpb25TZWxlY3RlZCk9XCJzZWxlY3RlZCgkZXZlbnQpXCJcclxuICAgICAgPlxyXG4gICAgICAgIDxtYXQtb3B0aW9uICpuZ0Zvcj1cImxldCBmZSBvZiBmaWx0ZXJlZEVudW0gfCBhc3luY1wiIFt2YWx1ZV09XCJmZVwiPlxyXG4gICAgICAgICAge3sgZmUubmFtZSB9fVxyXG4gICAgICAgIDwvbWF0LW9wdGlvbj5cclxuICAgICAgPC9tYXQtYXV0b2NvbXBsZXRlPlxyXG4gICAgPC9tYXQtZm9ybS1maWVsZD5cclxuICBgLFxyXG4gIHN0eWxlczogW1xyXG4gICAgYFxyXG4gICAgICAubWF0LXN0YW5kYXJkLWNoaXAge1xyXG4gICAgICAgIG1hcmdpbjogNHB4IDhweCA0cHggMDtcclxuICAgICAgfVxyXG5cclxuICAgICAgLmNoaXAtbmFtZSB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMnB4O1xyXG4gICAgICB9XHJcblxyXG4gICAgICAubWF0LWNoaXAtcmVtb3ZlIHtcclxuICAgICAgICBmb250LXNpemU6IDMycHg7XHJcbiAgICAgICAgb3BhY2l0eTogMSAhaW1wb3J0YW50O1xyXG4gICAgICB9XHJcblxyXG4gICAgICAuY29tcGxldGUtbWFya2VyIHtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZWQ7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC5jb21wbGV0ZSB7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogZ3JlZW47XHJcbiAgICAgIH1cclxuICAgIGBcclxuICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBNYXRlcmlhbENoaXBsaXN0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBmb3JtQ29udHJvbDogQWJzdHJhY3RDb250cm9sO1xyXG4gIGR1bW15Rm9ybUNvbnRyb2wgPSBuZXcgRm9ybUNvbnRyb2woKTtcclxuICBjb250cm9sTmFtZTogc3RyaW5nO1xyXG4gIGNvbnRyb2xWYWx1ZTogYW55O1xyXG4gIGNvbnRyb2xEaXNhYmxlZCA9IHRydWU7XHJcbiAgYm91bmRDb250cm9sID0gZmFsc2U7XHJcbiAgb3B0aW9uczogYW55O1xyXG4gIGlzQXJyYXkgPSB0cnVlO1xyXG4gIEBJbnB1dCgpIGxheW91dE5vZGU6IGFueTtcclxuICBASW5wdXQoKSBsYXlvdXRJbmRleDogbnVtYmVyW107XHJcbiAgQElucHV0KCkgZGF0YUluZGV4OiBudW1iZXJbXTtcclxuXHJcbiAgc2VsZWN0YWJsZSA9IHRydWU7XHJcbiAgYWRkT25CbHVyID0gdHJ1ZTtcclxuICBlbmFibGVJbnB1dCA9IHRydWU7XHJcbiAgZW5hYmxlRGVsZXRlID0gdHJ1ZTtcclxuICBlbmFibGVDbGljayA9IHRydWU7XHJcbiAgc2VwYXJhdG9yS2V5c0NvZGVzOiBudW1iZXJbXSA9IFtFTlRFUiwgQ09NTUFdO1xyXG4gIGZpbHRlcmVkRW51bTogT2JzZXJ2YWJsZTxzdHJpbmdbXT47XHJcbiAgc2VsZWN0ZWRPcHQ6IHN0cmluZ1tdID0gW107XHJcbiAgY3VycmVudE9wdDogc3RyaW5nW10gPSBbXTtcclxuXHJcbiAgQFZpZXdDaGlsZCgnY2hpcElucHV0JykgY2hpcElucHV0OiBFbGVtZW50UmVmPEhUTUxJbnB1dEVsZW1lbnQ+O1xyXG4gIEBWaWV3Q2hpbGQoJ2F1dG8nKSBtYXRBdXRvY29tcGxldGU6IE1hdEF1dG9jb21wbGV0ZTtcclxuXHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBqc2Y6IEpzb25TY2hlbWFGb3JtU2VydmljZSkge31cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICB0aGlzLm9wdGlvbnMgPSB0aGlzLmxheW91dE5vZGUub3B0aW9ucyB8fCB7fTtcclxuICAgIHRoaXMub3B0aW9ucy5lbnVtID0gdGhpcy5vcHRpb25zLmVudW0gfHwgW107XHJcbiAgICB0aGlzLnNlbGVjdGVkT3B0ID0gdGhpcy5vcHRpb25zLnNlbGVjdGVkT3B0IHx8IFtdO1xyXG4gICAgdGhpcy5jdXJyZW50T3B0ID0gW107XHJcbiAgICB0aGlzLmVuYWJsZUlucHV0ID0gdGhpcy5vcHRpb25zLmVuYWJsZUlucHV0O1xyXG4gICAgdGhpcy5lbmFibGVEZWxldGUgPSB0aGlzLm9wdGlvbnMuZW5hYmxlRGVsZXRlO1xyXG4gICAgdGhpcy5lbmFibGVDbGljayA9IHRoaXMub3B0aW9ucy5lbmFibGVDbGljaztcclxuICAgIHRoaXMuanNmLmluaXRpYWxpemVDb250cm9sKHRoaXMpO1xyXG4gICAgY29uc3QgX2ZjVmFsdWUgPSBKU09OLnBhcnNlKHRoaXMuZm9ybUNvbnRyb2wudmFsdWUpO1xyXG4gICAgaWYgKF9mY1ZhbHVlICYmIF9mY1ZhbHVlLmxlbmd0aCkge1xyXG4gICAgICBmb3IgKGNvbnN0IHZhbHVlIG9mIF9mY1ZhbHVlKSB7XHJcbiAgICAgICAgdmFsdWUuY29tcGxldGUgIT0gbnVsbFxyXG4gICAgICAgICAgPyB0aGlzLnNlbGVjdGVkT3B0LnB1c2godmFsdWUpXHJcbiAgICAgICAgICA6IHRoaXMuY3VycmVudE9wdC5wdXNoKHZhbHVlKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgdGhpcy5zZWxlY3RlZE9wdCA9IF8udW5pcUJ5KHRoaXMuc2VsZWN0ZWRPcHQsICduYW1lJyk7XHJcbiAgICB0aGlzLnVwZGF0ZVZhbHVlKCk7XHJcbiAgICB0aGlzLmZpbHRlcmVkRW51bSA9IHRoaXMuZHVtbXlGb3JtQ29udHJvbC52YWx1ZUNoYW5nZXMucGlwZShcclxuICAgICAgc3RhcnRXaXRoKG51bGwpLFxyXG4gICAgICBtYXAoKGZlOiBzdHJpbmcgfCBudWxsKSA9PlxyXG4gICAgICAgIGZlID8gdGhpcy5maW5kT3B0cyhmZSwgdHJ1ZSkgOiB0aGlzLm9wdGlvbnMuZW51bS5zbGljZSgpXHJcbiAgICAgIClcclxuICAgICk7XHJcbiAgfVxyXG5cclxuICB1cGRhdGVWYWx1ZSgpIHtcclxuICAgIHRoaXMuanNmLnVwZGF0ZVZhbHVlKFxyXG4gICAgICB0aGlzLFxyXG4gICAgICBKU09OLnN0cmluZ2lmeShbLi4udGhpcy5jdXJyZW50T3B0LCAuLi50aGlzLnNlbGVjdGVkT3B0XSlcclxuICAgICk7XHJcbiAgICBpZiAoXHJcbiAgICAgIHRoaXMub3B0aW9ucy5vbkNoYW5nZXMgJiZcclxuICAgICAgdHlwZW9mIHRoaXMub3B0aW9ucy5vbkNoYW5nZXMgPT09ICdmdW5jdGlvbidcclxuICAgICkge1xyXG4gICAgICB0aGlzLm9wdGlvbnMub25DaGFuZ2VzKHRoaXMuY3VycmVudE9wdCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBhZGQoZXZlbnQ6IE1hdENoaXBJbnB1dEV2ZW50KTogdm9pZCB7XHJcbiAgICBpZiAoIXRoaXMubWF0QXV0b2NvbXBsZXRlLmlzT3Blbikge1xyXG4gICAgICBjb25zdCBpbnB1dCA9IGV2ZW50LmlucHV0O1xyXG4gICAgICBjb25zdCB2YWx1ZSA9IGV2ZW50LnZhbHVlO1xyXG5cclxuICAgICAgaWYgKCh2YWx1ZSB8fCAnJykudHJpbSgpKSB7XHJcbiAgICAgICAgdGhpcy5hZGRTZWxlY3RlZCh2YWx1ZSk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vIFJlc2V0IHRoZSBpbnB1dCB2YWx1ZVxyXG4gICAgICBpZiAoaW5wdXQpIHtcclxuICAgICAgICBpbnB1dC52YWx1ZSA9ICcnO1xyXG4gICAgICB9XHJcbiAgICAgIHRoaXMuZHVtbXlGb3JtQ29udHJvbC5zZXRWYWx1ZShudWxsKTtcclxuICAgICAgdGhpcy51cGRhdGVWYWx1ZSgpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcmVtb3ZlU2VsZWN0ZWQob3B0OiBzdHJpbmcpOiB2b2lkIHtcclxuICAgIGNvbnN0IGluZGV4ID0gdGhpcy5zZWxlY3RlZE9wdC5pbmRleE9mKG9wdCk7XHJcblxyXG4gICAgaWYgKGluZGV4ID49IDApIHtcclxuICAgICAgdGhpcy5zZWxlY3RlZE9wdC5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgfVxyXG4gICAgdGhpcy51cGRhdGVWYWx1ZSgpO1xyXG4gIH1cclxuXHJcbiAgcmVtb3ZlQ3VycmVudChvcHQ6IHN0cmluZyk6IHZvaWQge1xyXG4gICAgY29uc3QgaW5kZXggPSB0aGlzLmN1cnJlbnRPcHQuaW5kZXhPZihvcHQpO1xyXG5cclxuICAgIGlmIChpbmRleCA+PSAwKSB7XHJcbiAgICAgIHRoaXMuY3VycmVudE9wdC5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgfVxyXG4gICAgdGhpcy51cGRhdGVWYWx1ZSgpO1xyXG4gIH1cclxuXHJcbiAgc2VsZWN0ZWQoZXZlbnQ6IE1hdEF1dG9jb21wbGV0ZVNlbGVjdGVkRXZlbnQpOiB2b2lkIHtcclxuICAgIGlmIChcclxuICAgICAgdGhpcy5jdXJyZW50T3B0Lmxlbmd0aCA8IHRoaXMub3B0aW9ucy5jaGlwc0xpbWl0IHx8XHJcbiAgICAgIHRoaXMub3B0aW9ucy5jaGlwc0xpbWl0ID09PSAwXHJcbiAgICApIHtcclxuICAgICAgdGhpcy5hZGRTZWxlY3RlZChldmVudC5vcHRpb24udmlld1ZhbHVlKTtcclxuICAgICAgdGhpcy5jaGlwSW5wdXQubmF0aXZlRWxlbWVudC52YWx1ZSA9ICcnO1xyXG4gICAgICB0aGlzLmR1bW15Rm9ybUNvbnRyb2wuc2V0VmFsdWUobnVsbCk7XHJcbiAgICAgIHRoaXMudXBkYXRlVmFsdWUoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIG9uQ2hpcHNDbGljayh2YWx1ZSkge1xyXG4gICAgaWYgKFxyXG4gICAgICB0aGlzLmVuYWJsZUNsaWNrICYmXHJcbiAgICAgIHRoaXMub3B0aW9ucy5vbkNoaXBzQ2xpY2sgJiZcclxuICAgICAgdHlwZW9mIHRoaXMub3B0aW9ucy5vbkNoaXBzQ2xpY2sgPT09ICdmdW5jdGlvbidcclxuICAgICkge1xyXG4gICAgICB0aGlzLm9wdGlvbnMub25DaGlwc0NsaWNrKHZhbHVlKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8vIHByaXZhdGUgYWRkU2VsZWN0ZWQodmFsdWUpIHtcclxuICAvLyAgIGNvbnN0IF9vcHQgPSB0aGlzLmZpbmRPcHRzKHZhbHVlKTtcclxuICAvLyAgIGlmIChfb3B0KSB7XHJcbiAgLy8gICAgIGNvbnN0IF9zZWxlY3RlZE9wdCA9IHRoaXMuc2VsZWN0ZWRPcHQuZmlsdGVyKFxyXG4gIC8vICAgICAgIChzbzogYW55KSA9PiBzby5uYW1lLnRvTG93ZXJDYXNlKCkgPT09IF9vcHQubmFtZS50b0xvd2VyQ2FzZSgpXHJcbiAgLy8gICAgICk7XHJcbiAgLy8gICAgIGlmICghX3NlbGVjdGVkT3B0Lmxlbmd0aCkge1xyXG4gIC8vICAgICAgIHRoaXMuc2VsZWN0ZWRPcHQucHVzaChfb3B0KTtcclxuICAvLyAgICAgfVxyXG4gIC8vICAgfVxyXG4gIC8vIH1cclxuXHJcbiAgcHJpdmF0ZSBhZGRTZWxlY3RlZCh2YWx1ZSkge1xyXG4gICAgY29uc3QgX29wdCA9IHRoaXMuZmluZE9wdHModmFsdWUpO1xyXG4gICAgaWYgKF9vcHQpIHtcclxuICAgICAgY29uc3QgX2N1cnJlbnRPcHQgPSB0aGlzLmN1cnJlbnRPcHQuZmlsdGVyKFxyXG4gICAgICAgIChzbzogYW55KSA9PiBzby5uYW1lLnRvTG93ZXJDYXNlKCkgPT09IF9vcHQubmFtZS50b0xvd2VyQ2FzZSgpXHJcbiAgICAgICk7XHJcbiAgICAgIGNvbnN0IF9zZWxlY3RlZE9wdCA9IHRoaXMuc2VsZWN0ZWRPcHQuZmlsdGVyKFxyXG4gICAgICAgIChzbzogYW55KSA9PiBzby5uYW1lLnRvTG93ZXJDYXNlKCkgPT09IF9vcHQubmFtZS50b0xvd2VyQ2FzZSgpXHJcbiAgICAgICk7XHJcbiAgICAgIGlmICghX2N1cnJlbnRPcHQubGVuZ3RoICYmICFfc2VsZWN0ZWRPcHQubGVuZ3RoKSB7XHJcbiAgICAgICAgdGhpcy5jdXJyZW50T3B0LnB1c2goX29wdCk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIHByaXZhdGUgZmluZE9wdHModmFsdWUsIGxpa2U6IGJvb2xlYW4gPSBmYWxzZSkge1xyXG4gICAgaWYgKHZhbHVlKSB7XHJcbiAgICAgIGNvbnN0IF92YWx1ZSA9ICh2YWx1ZS5uYW1lIHx8IHZhbHVlKS50cmltKCkuc3BsaXQoJyB8ICcpO1xyXG4gICAgICBjb25zdCBfZm91bmRPcHQgPSBsaWtlXHJcbiAgICAgICAgPyB0aGlzLm9wdGlvbnMuZW51bS5maWx0ZXIoXHJcbiAgICAgICAgICAgIG9wdCA9PiBvcHQubmFtZS50b0xvd2VyQ2FzZSgpLmluZGV4T2YoX3ZhbHVlWzBdLnRvTG93ZXJDYXNlKCkpID09PSAwXHJcbiAgICAgICAgICApXHJcbiAgICAgICAgOiB0aGlzLm9wdGlvbnMuZW51bS5maWx0ZXIoXHJcbiAgICAgICAgICAgIG9wdCA9PiBvcHQubmFtZS50b0xvd2VyQ2FzZSgpID09PSBfdmFsdWVbMF0udG9Mb3dlckNhc2UoKVxyXG4gICAgICAgICAgKTtcclxuICAgICAgcmV0dXJuIF9mb3VuZE9wdC5sZW5ndGggPyAobGlrZSA/IF9mb3VuZE9wdCA6IF9mb3VuZE9wdFswXSkgOiBudWxsO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIG51bGw7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF9maWx0ZXIodmFsdWU6IGFueSk6IHN0cmluZ1tdIHtcclxuICAgIGNvbnN0IF9vcHQgPSB0aGlzLmZpbmRPcHRzKHZhbHVlLCB0cnVlKTtcclxuICAgIHJldHVybiBfb3B0XHJcbiAgICAgID8gdGhpcy5vcHRpb25zLmVudW0uZmlsdGVyKFxyXG4gICAgICAgICAgb3B0ID0+IG9wdC5uYW1lLnRvTG93ZXJDYXNlKCkuaW5kZXhPZihfb3B0Lm5hbWUudG9Mb3dlckNhc2UoKSkgPT09IDBcclxuICAgICAgICApXHJcbiAgICAgIDogW107XHJcbiAgfVxyXG59XHJcbiJdfQ==