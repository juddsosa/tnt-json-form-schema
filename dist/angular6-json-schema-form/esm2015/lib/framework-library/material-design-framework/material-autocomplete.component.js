import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { JsonSchemaFormService } from '../../json-schema-form.service';
let MaterialAutoCompleteComponent = class MaterialAutoCompleteComponent {
    constructor(jsf) {
        this.jsf = jsf;
        this.dummyFormControl = new FormControl();
        this.controlDisabled = true;
        this.boundControl = false;
        this.isArray = true;
    }
    ngOnInit() {
        this.options = this.layoutNode.options || {};
        this.enum = this.options.enum || [];
        this.jsf.initializeControl(this);
        this.setValue(this.formControl.value);
        this.dummyFormControl.valueChanges.subscribe(value => this.updateValue(value));
    }
    selectOption(option) {
        const foundEnum = this.enum.find(e => e.id === option.id);
        this.dummyFormControl.setValue(foundEnum ? foundEnum.name : foundEnum);
    }
    displayWithName(opt) {
        return !opt ? '' : typeof opt === 'string' ? opt : opt.name;
    }
    setValue(value) {
        const foundEnum = this.enum.find(e => e.id === value);
        this.dummyFormControl.setValue(foundEnum ? foundEnum.name : foundEnum);
    }
    updateValue(value) {
        if (!value) {
            this.enum = this.options.enum || [];
            this.jsf.updateValue(this, null);
        }
        else if (typeof value === 'string') {
            this.enum = this.options.enum.filter(e => e.name.toLowerCase().indexOf(value.toLowerCase()) !== -1);
        }
        else {
            const foundEnum = this.enum.find(e => e.id === value.id);
            this.jsf.updateValue(this, foundEnum ? foundEnum.id : foundEnum);
        }
    }
};
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], MaterialAutoCompleteComponent.prototype, "layoutNode", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Array)
], MaterialAutoCompleteComponent.prototype, "layoutIndex", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Array)
], MaterialAutoCompleteComponent.prototype, "dataIndex", void 0);
MaterialAutoCompleteComponent = tslib_1.__decorate([
    Component({
        selector: 'material-autocomplete-widget',
        template: `
    <mat-form-field
      [class]="options?.htmlClass || ''"
      [floatLabel]="
        options?.floatLabel || (options?.notitle ? 'never' : 'auto')
      "
      [style.width]="'30%'"
    >
      <input
        matInput
        [formControl]="dummyFormControl"
        [matAutocomplete]="leftAuto"
        [placeholder]="options?.notitle ? options?.placeholder : options?.title"
        [style.width]="'100%'"
      />
      <mat-autocomplete
        #leftAuto="matAutocomplete"
        (optionSelected)="selectOption($event.option.value)"
      >
        <mat-option *ngFor="let option of enum" [value]="option">
          {{ option.name }}
        </mat-option>
      </mat-autocomplete>
    </mat-form-field>
  `
    }),
    tslib_1.__metadata("design:paramtypes", [JsonSchemaFormService])
], MaterialAutoCompleteComponent);
export { MaterialAutoCompleteComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWF0ZXJpYWwtYXV0b2NvbXBsZXRlLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FuZ3VsYXI2LWpzb24tc2NoZW1hLWZvcm0vIiwic291cmNlcyI6WyJsaWIvZnJhbWV3b3JrLWxpYnJhcnkvbWF0ZXJpYWwtZGVzaWduLWZyYW1ld29yay9tYXRlcmlhbC1hdXRvY29tcGxldGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBVSxNQUFNLGVBQWUsQ0FBQztBQUN6RCxPQUFPLEVBQW1CLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRTlELE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBOEJ2RSxJQUFhLDZCQUE2QixHQUExQyxNQUFhLDZCQUE2QjtJQWV4QyxZQUFvQixHQUEwQjtRQUExQixRQUFHLEdBQUgsR0FBRyxDQUF1QjtRQWI5QyxxQkFBZ0IsR0FBRyxJQUFJLFdBQVcsRUFBRSxDQUFDO1FBR3JDLG9CQUFlLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLGlCQUFZLEdBQUcsS0FBSyxDQUFDO1FBRXJCLFlBQU8sR0FBRyxJQUFJLENBQUM7SUFPa0MsQ0FBQztJQUVsRCxRQUFRO1FBQ04sSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sSUFBSSxFQUFFLENBQUM7UUFDN0MsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksSUFBSSxFQUFFLENBQUM7UUFDcEMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNqQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDdEMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FDbkQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FDeEIsQ0FBQztJQUNKLENBQUM7SUFFRCxZQUFZLENBQUMsTUFBTTtRQUNqQixNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQzFELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUN6RSxDQUFDO0lBRUQsZUFBZSxDQUFDLEdBQUc7UUFDakIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLEdBQUcsS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQztJQUM5RCxDQUFDO0lBRUQsUUFBUSxDQUFDLEtBQUs7UUFDWixNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssS0FBSyxDQUFDLENBQUM7UUFDdEQsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ3pFLENBQUM7SUFFRCxXQUFXLENBQUMsS0FBSztRQUNmLElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDVixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQztZQUNwQyxJQUFJLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7U0FDbEM7YUFBTSxJQUFJLE9BQU8sS0FBSyxLQUFLLFFBQVEsRUFBRTtZQUNwQyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FDbEMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FDOUQsQ0FBQztTQUNIO2FBQU07WUFDTCxNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ3pELElBQUksQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxTQUFTLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQ2xFO0lBQ0gsQ0FBQztDQUNGLENBQUE7QUE3Q1U7SUFBUixLQUFLLEVBQUU7O2lFQUFpQjtBQUNoQjtJQUFSLEtBQUssRUFBRTs7a0VBQXVCO0FBQ3RCO0lBQVIsS0FBSyxFQUFFOztnRUFBcUI7QUFYbEIsNkJBQTZCO0lBNUJ6QyxTQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsOEJBQThCO1FBQ3hDLFFBQVEsRUFBRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0dBd0JUO0tBQ0YsQ0FBQzs2Q0FnQnlCLHFCQUFxQjtHQWZuQyw2QkFBNkIsQ0FzRHpDO1NBdERZLDZCQUE2QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBBYnN0cmFjdENvbnRyb2wsIEZvcm1Db250cm9sIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5cclxuaW1wb3J0IHsgSnNvblNjaGVtYUZvcm1TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vanNvbi1zY2hlbWEtZm9ybS5zZXJ2aWNlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbWF0ZXJpYWwtYXV0b2NvbXBsZXRlLXdpZGdldCcsXHJcbiAgdGVtcGxhdGU6IGBcclxuICAgIDxtYXQtZm9ybS1maWVsZFxyXG4gICAgICBbY2xhc3NdPVwib3B0aW9ucz8uaHRtbENsYXNzIHx8ICcnXCJcclxuICAgICAgW2Zsb2F0TGFiZWxdPVwiXHJcbiAgICAgICAgb3B0aW9ucz8uZmxvYXRMYWJlbCB8fCAob3B0aW9ucz8ubm90aXRsZSA/ICduZXZlcicgOiAnYXV0bycpXHJcbiAgICAgIFwiXHJcbiAgICAgIFtzdHlsZS53aWR0aF09XCInMzAlJ1wiXHJcbiAgICA+XHJcbiAgICAgIDxpbnB1dFxyXG4gICAgICAgIG1hdElucHV0XHJcbiAgICAgICAgW2Zvcm1Db250cm9sXT1cImR1bW15Rm9ybUNvbnRyb2xcIlxyXG4gICAgICAgIFttYXRBdXRvY29tcGxldGVdPVwibGVmdEF1dG9cIlxyXG4gICAgICAgIFtwbGFjZWhvbGRlcl09XCJvcHRpb25zPy5ub3RpdGxlID8gb3B0aW9ucz8ucGxhY2Vob2xkZXIgOiBvcHRpb25zPy50aXRsZVwiXHJcbiAgICAgICAgW3N0eWxlLndpZHRoXT1cIicxMDAlJ1wiXHJcbiAgICAgIC8+XHJcbiAgICAgIDxtYXQtYXV0b2NvbXBsZXRlXHJcbiAgICAgICAgI2xlZnRBdXRvPVwibWF0QXV0b2NvbXBsZXRlXCJcclxuICAgICAgICAob3B0aW9uU2VsZWN0ZWQpPVwic2VsZWN0T3B0aW9uKCRldmVudC5vcHRpb24udmFsdWUpXCJcclxuICAgICAgPlxyXG4gICAgICAgIDxtYXQtb3B0aW9uICpuZ0Zvcj1cImxldCBvcHRpb24gb2YgZW51bVwiIFt2YWx1ZV09XCJvcHRpb25cIj5cclxuICAgICAgICAgIHt7IG9wdGlvbi5uYW1lIH19XHJcbiAgICAgICAgPC9tYXQtb3B0aW9uPlxyXG4gICAgICA8L21hdC1hdXRvY29tcGxldGU+XHJcbiAgICA8L21hdC1mb3JtLWZpZWxkPlxyXG4gIGBcclxufSlcclxuZXhwb3J0IGNsYXNzIE1hdGVyaWFsQXV0b0NvbXBsZXRlQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBmb3JtQ29udHJvbDogQWJzdHJhY3RDb250cm9sO1xyXG4gIGR1bW15Rm9ybUNvbnRyb2wgPSBuZXcgRm9ybUNvbnRyb2woKTtcclxuICBjb250cm9sTmFtZTogc3RyaW5nO1xyXG4gIGNvbnRyb2xWYWx1ZTogYW55O1xyXG4gIGNvbnRyb2xEaXNhYmxlZCA9IHRydWU7XHJcbiAgYm91bmRDb250cm9sID0gZmFsc2U7XHJcbiAgb3B0aW9uczogYW55O1xyXG4gIGlzQXJyYXkgPSB0cnVlO1xyXG4gIEBJbnB1dCgpIGxheW91dE5vZGU6IGFueTtcclxuICBASW5wdXQoKSBsYXlvdXRJbmRleDogbnVtYmVyW107XHJcbiAgQElucHV0KCkgZGF0YUluZGV4OiBudW1iZXJbXTtcclxuXHJcbiAgZW51bTogYW55W107XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUganNmOiBKc29uU2NoZW1hRm9ybVNlcnZpY2UpIHt9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgdGhpcy5vcHRpb25zID0gdGhpcy5sYXlvdXROb2RlLm9wdGlvbnMgfHwge307XHJcbiAgICB0aGlzLmVudW0gPSB0aGlzLm9wdGlvbnMuZW51bSB8fCBbXTtcclxuICAgIHRoaXMuanNmLmluaXRpYWxpemVDb250cm9sKHRoaXMpO1xyXG4gICAgdGhpcy5zZXRWYWx1ZSh0aGlzLmZvcm1Db250cm9sLnZhbHVlKTtcclxuICAgIHRoaXMuZHVtbXlGb3JtQ29udHJvbC52YWx1ZUNoYW5nZXMuc3Vic2NyaWJlKHZhbHVlID0+XHJcbiAgICAgIHRoaXMudXBkYXRlVmFsdWUodmFsdWUpXHJcbiAgICApO1xyXG4gIH1cclxuXHJcbiAgc2VsZWN0T3B0aW9uKG9wdGlvbikge1xyXG4gICAgY29uc3QgZm91bmRFbnVtID0gdGhpcy5lbnVtLmZpbmQoZSA9PiBlLmlkID09PSBvcHRpb24uaWQpO1xyXG4gICAgdGhpcy5kdW1teUZvcm1Db250cm9sLnNldFZhbHVlKGZvdW5kRW51bSA/IGZvdW5kRW51bS5uYW1lIDogZm91bmRFbnVtKTtcclxuICB9XHJcblxyXG4gIGRpc3BsYXlXaXRoTmFtZShvcHQpIHtcclxuICAgIHJldHVybiAhb3B0ID8gJycgOiB0eXBlb2Ygb3B0ID09PSAnc3RyaW5nJyA/IG9wdCA6IG9wdC5uYW1lO1xyXG4gIH1cclxuXHJcbiAgc2V0VmFsdWUodmFsdWUpIHtcclxuICAgIGNvbnN0IGZvdW5kRW51bSA9IHRoaXMuZW51bS5maW5kKGUgPT4gZS5pZCA9PT0gdmFsdWUpO1xyXG4gICAgdGhpcy5kdW1teUZvcm1Db250cm9sLnNldFZhbHVlKGZvdW5kRW51bSA/IGZvdW5kRW51bS5uYW1lIDogZm91bmRFbnVtKTtcclxuICB9XHJcblxyXG4gIHVwZGF0ZVZhbHVlKHZhbHVlKSB7XHJcbiAgICBpZiAoIXZhbHVlKSB7XHJcbiAgICAgIHRoaXMuZW51bSA9IHRoaXMub3B0aW9ucy5lbnVtIHx8IFtdO1xyXG4gICAgICB0aGlzLmpzZi51cGRhdGVWYWx1ZSh0aGlzLCBudWxsKTtcclxuICAgIH0gZWxzZSBpZiAodHlwZW9mIHZhbHVlID09PSAnc3RyaW5nJykge1xyXG4gICAgICB0aGlzLmVudW0gPSB0aGlzLm9wdGlvbnMuZW51bS5maWx0ZXIoXHJcbiAgICAgICAgZSA9PiBlLm5hbWUudG9Mb3dlckNhc2UoKS5pbmRleE9mKHZhbHVlLnRvTG93ZXJDYXNlKCkpICE9PSAtMVxyXG4gICAgICApO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgY29uc3QgZm91bmRFbnVtID0gdGhpcy5lbnVtLmZpbmQoZSA9PiBlLmlkID09PSB2YWx1ZS5pZCk7XHJcbiAgICAgIHRoaXMuanNmLnVwZGF0ZVZhbHVlKHRoaXMsIGZvdW5kRW51bSA/IGZvdW5kRW51bS5pZCA6IGZvdW5kRW51bSk7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ==