import { OnInit } from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';
import { JsonSchemaFormService } from '../../json-schema-form.service';
export declare class MaterialAutoCompleteComponent implements OnInit {
    private jsf;
    formControl: AbstractControl;
    dummyFormControl: FormControl;
    controlName: string;
    controlValue: any;
    controlDisabled: boolean;
    boundControl: boolean;
    options: any;
    isArray: boolean;
    layoutNode: any;
    layoutIndex: number[];
    dataIndex: number[];
    enum: any[];
    constructor(jsf: JsonSchemaFormService);
    ngOnInit(): void;
    selectOption(option: any): void;
    displayWithName(opt: any): any;
    setValue(value: any): void;
    updateValue(value: any): void;
}
